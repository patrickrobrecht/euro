<?php

namespace App\Console\Commands;

use App\Generators\IssueOverview\IssueOverviewGenerator;
use Illuminate\Console\Command;

class GenerateIssueOverviewCommand extends Command
{
    protected $signature = 'generate:issue-overview';

    protected $description = 'Generate issue overview';

    public function handle(): void
    {
        foreach (config('app.locales') as $locale) {
            $generator = new IssueOverviewGenerator($locale);
            $generator->generate();
        }
    }
}
