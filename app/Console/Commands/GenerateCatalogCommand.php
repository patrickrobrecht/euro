<?php

namespace App\Console\Commands;

use App\Generators\Catalog\CatalogGenerator;
use Illuminate\Console\Command;

class GenerateCatalogCommand extends Command
{
    protected $signature = 'generate:catalog';

    protected $description = 'Generate catalog';

    public function handle(): void
    {
        foreach (config('app.locales') as $locale) {
            $generator = new CatalogGenerator($locale);
            $generator->generate();
        }
    }
}
