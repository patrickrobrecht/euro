<?php

namespace App\Options;

use App\Models\Coin;
use App\Models\CoinSeries;
use App\Options\Traits\NamedOption;

/**
 * Class CoinType
 * @package App\Options
 *
 * Type for {@see Coin} and {@see CoinSeries}
 */
class CoinType
{
    use NamedOption;

    public const CIRCULATION = 'CIRCULATION';
    public const COMMEMORATIVE = 'COMMEMORATIVE';

    public static function getAll(): array
    {
        return [
            self::CIRCULATION => __('Circulation coins'),
            self::COMMEMORATIVE => __('2€ commemorative coins'),
        ];
    }
}
