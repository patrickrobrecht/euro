<?php

namespace App\Options;

use App\Models\User;
use App\Options\Traits\NamedOption;

/**
 * Class UserRole
 * @package App\Options
 *
 * Roles for {@see User}
 */
class UserRole
{
    use NamedOption;

    public const ADMIN = 'admin';

    public static function getAll(): array
    {
        return [
            '' => __('no user role'),
            self::ADMIN => __('Administrator'),
        ];
    }
}
