<?php

namespace App\Options;

use App\Options\Traits\NamedOption;

class SearchScope
{
    use NamedOption;

    public const COINS = 'coins';
    public const COIN_SERIES  = 'coin_series';
    public const COUNTRIES = 'countries';
    public const MINTS = 'mints';
    public const DESIGNERS = 'designers';

    public static function getAll(): array
    {
        return [
            self::COINS => __('Coins'),
            self::COIN_SERIES => __('Coin series'),
            self::COUNTRIES => __('Countries'),
            self::DESIGNERS => __('Coin designers'),
            self::MINTS => __('Mints'),
        ];
    }
}
