<?php

namespace App\Options;

use App\Options\Traits\NamedOption;

class Sex
{
    use NamedOption;

    public const UNKNOWN = 'unknown';
    public const FEMALE = 'female';
    public const MALE = 'male';

    public static function getAll(): array
    {
        return [
            self::UNKNOWN => __('unknown'),
            self::FEMALE => __('female'),
            self::MALE => __('male'),
        ];
    }
}
