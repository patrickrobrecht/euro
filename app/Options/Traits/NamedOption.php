<?php

namespace App\Options\Traits;

use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;

trait NamedOption
{
    abstract public static function getAll(): array;

    public static function exists($key): bool
    {
        return in_array($key, self::getKeys(), true);
    }

    public static function getKeys(): array
    {
        return array_keys(static::getAll());
    }

    public static function getName($key): string
    {
        return static::getAll()[$key] ?? $key;
    }

    public static function getRule(): In
    {
        return Rule::in(static::getKeys());
    }
}
