<?php

namespace App\Options;

use App\Models\Coin;
use App\Services\Formatter;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\In;

/**
 * Class Denomination
 * @package App\Options
 *
 * Denomination for {@see Coin}
 */
class Denomination
{
    private const DENOMINATIONS = [
        0.01,
        0.02,
        0.05,
        0.10,
        0.20,
        0.50,
        1.00,
        2.00,
    ];

    public static function getAll(): array
    {
        return self::DENOMINATIONS;
    }

    public static function getRule(): In
    {
        return Rule::in(self::DENOMINATIONS);
    }

    public static function exists($key): bool
    {
        return in_array($key, self::DENOMINATIONS, true);
    }

    public static function getName($denomination): string
    {
        return Formatter::number($denomination) . ' €';
    }
}
