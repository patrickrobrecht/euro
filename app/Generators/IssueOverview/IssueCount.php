<?php

namespace App\Generators\IssueOverview;

class IssueCount
{
    /**
     * @var array year -> issue count
     */
    private $issueCountPerYear;

    private $totalCount;

    public function __construct()
    {
        $this->issueCountPerYear = [];
        $this->totalCount = 0;
    }

    public function addIssue(int $year, int $count = 1): void
    {
        $this->totalCount += $count;
        if (array_key_exists($year, $this->issueCountPerYear)) {
            $this->issueCountPerYear[$year] += $count;
        } else {
            $this->issueCountPerYear[$year] = $count;
        }
    }

    public function getFirstYear(): ?int
    {
        return $this->hasIssues() ? min($this->getYears()) : null;
    }

    public function getLastYear(): ?int
    {
        return $this->hasIssues() ? max($this->getYears()) : null;
    }

    public function getYears(): array
    {
        return array_keys($this->issueCountPerYear);
    }

    public function hasIssues(): bool
    {
        return $this->totalCount > 0;
    }

    public function getTotalCount(): int
    {
        return $this->totalCount;
    }

    public function getCount(int $year): int
    {
        return array_key_exists($year, $this->issueCountPerYear) ? $this->issueCountPerYear[$year] : 0;
    }
}
