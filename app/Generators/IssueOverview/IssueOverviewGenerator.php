<?php

namespace App\Generators\IssueOverview;

use App\Generators\IssueOverview\Sheets\CirculationCoinsSheet;
use App\Generators\IssueOverview\Sheets\CommemorativeCoinsSheet;
use App\Generators\IssueOverview\Sheets\OverviewSheet;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class IssueOverviewGenerator
{
    private $locale;

    /** @var $spreadsheet Spreadsheet */
    private $spreadsheet;

    public function __construct(array $locale)
    {
        $this->locale = $locale;
    }

    public function generate(): void
    {
        App::setLocale($this->locale['slug']);

        $this->spreadsheet = new Spreadsheet();

        try {
            new OverviewSheet($this->spreadsheet->getActiveSheet());
            new CirculationCoinsSheet($this->spreadsheet->createSheet());
            new CommemorativeCoinsSheet($this->spreadsheet->createSheet());

            $directoryName = 'downloads/' . strtolower($this->locale['slug']);
            Storage::disk('public')->makeDirectory($directoryName);
            $directory = Storage::disk('public')->path($directoryName);

            $writer = new Xlsx($this->spreadsheet);
            $writer->save($directory . '/' . $this->getFileName('xlsx'));
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        };
    }

    private function getFileName(string $extension): string
    {
        return sprintf('%s.%s', __('issue-overview'), $extension);
    }
}
