<?php

namespace App\Generators\IssueOverview\Sheets;

use App\Models\CoinSeries;
use App\Models\Country;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CirculationCoinsSheet extends AbstractCoinsSheet
{
    private const SERIES_LEGEND_ROW = 3;
    private const HEADER_ROW = 5;
    private const COLORS = [
        ['ffff00', '000000'],
        ['ffcc00', '000000'],
        ['ff9999', '000000'],
        ['000099', 'ffffff'],
        ['33ff99', '000000'],
        ['009933', 'ffffff']
    ];

    public function __construct(Worksheet $sheet)
    {
        parent::__construct($sheet, __('Circulation coins'), self::HEADER_ROW);
    }

    protected function getCoins(Country $country)
    {
        return $country->circulationCoins;
    }

    protected function init(): void
    {
        parent::init();

        $maxSeriesIndex = 0;
        $rowIndex = self::HEADER_ROW;
        /* @var $country Country */
        foreach ($this->countries as $country) {
            $rowIndex++;

            $seriesIndex = 1;
            /* @var $series CoinSeries */
            foreach ($country->circulationCoinSeries as $series) {
                $startColumnIndex = $this->getYearColumn($series->first_year);
                $endColumnIndex = $this->getYearColumn($series->last_year ?: max($this->years));

                $this->colorSeries($startColumnIndex, $endColumnIndex, $rowIndex, $seriesIndex);

                $maxSeriesIndex = max($maxSeriesIndex, $seriesIndex);
                $seriesIndex++;
            }
        }

        $this->addSeriesLegend($maxSeriesIndex);
    }

    private function getYearColumn(int $year)
    {
        return $year - min($this->years) + self::FIRST_YEAR_COLUMN;
    }

    private function colorSeries(int $startColumnIndex, int $endColumnIndex, int $rowIndex, int $seriesIndex): void
    {
        if ($seriesIndex <= 0 || $seriesIndex > count(self::COLORS)) {
            return;
        }

        $backgroundColor = new Color(self::COLORS[$seriesIndex - 1][0]);
        $fontColor = new Color(self::COLORS[$seriesIndex - 1][1]);

        $style = $this->sheet->getStyleByColumnAndRow(
            $startColumnIndex,
            $rowIndex,
            $endColumnIndex,
            $rowIndex
        );
        $style->getFont()->setColor($fontColor);
        $style->getFill()->setFillType(Fill::FILL_SOLID)->setStartColor($backgroundColor);
    }

    private function addSeriesLegend(int $maxSeriesIndex): void
    {
        $this->setCellValue(2, self::SERIES_LEGEND_ROW, __('Series'));

        foreach (range(1, $maxSeriesIndex) as $seriesIndex) {
            $this->setCellValue(2 + $seriesIndex, self::SERIES_LEGEND_ROW, $seriesIndex);
            $this->colorSeries(
                2 + $seriesIndex,
                2 + $seriesIndex,
                self::SERIES_LEGEND_ROW,
                $seriesIndex
            );
        }
    }
}
