<?php

namespace App\Generators\IssueOverview\Sheets;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OverviewSheet extends AbstractSheet
{
    private const HEADER_ROW = 3;

    public function __construct(Worksheet $sheet)
    {
        parent::__construct($sheet, __('Issue overview'));
    }

    protected function init(): void
    {
        $this->addCountries(self::HEADER_ROW, true);

        $header = [
            __('Country'),
            __('Code'),
            __('Circulation coins'),
            __('2€ commemorative coins')
        ];
        $this->addHeaderColumns($header, self::HEADER_ROW);

        $this->setWidth(4, 5, 15);

        // TODO Add formulas getting numbers from other sheets
    }
}
