<?php

namespace App\Generators\IssueOverview\Sheets;

use App\Models\Country;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class AbstractSheet
{
    protected $sheet;

    public function __construct(Worksheet $sheet, string $title)
    {
        $this->sheet = $sheet;

        $this->sheet->setTitle($title);
        $this->setHeadline($title);
        $this->init();
    }

    private function setHeadline(string $headline): void
    {
        $this->sheet->setCellValue('A1', $headline);
        $this->sheet->getStyle('A1')->getFont()->setSize(16);
        $this->sheet->getRowDimension(1)->setRowHeight(20);
        $this->setBlue(1, 1);
    }

    abstract protected function init();

    protected function addHeaderColumns(array $headerColumns, int $rowIndex): void
    {
        $this->sheet->fromArray($headerColumns, null, 'B' . $rowIndex);

        $style = $this->sheet->getStyleByColumnAndRow(
            2,
            $rowIndex,
            2 + count($headerColumns),
            $rowIndex
        );
        $style->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $style->getFont()->setColor(new Color(Color::COLOR_BLUE));
    }

    protected function setCellValue(int $column, int $row, $value): void
    {
        $this->sheet->setCellValueByColumnAndRow($column, $row, $value);
    }

    protected function setBlue(int $column, int $row, $column2 = null, $row2 = null): void
    {
        $this->sheet->getStyleByColumnAndRow($column, $row, $column2, $row2)
            ->getFont()->setColor(new Color(Color::COLOR_BLUE));
    }

    protected function setBold(int $column, int $row, $column2 = null, $row2 = null): void
    {
        $this->sheet->getStyleByColumnAndRow($column, $row, $column2, $row2)
            ->getFont()->setBold(true);
    }

    protected function addCountries(int $rowOffset, bool $withCode = false): void
    {
        $index = 0;
        $rowIndex = $index + $rowOffset;
        foreach (Country::withEuro() as $country) {
            $index++;
            $rowIndex++;
            $this->setCellValue(1, $rowIndex, $index);
            $this->setCellValue(2, $rowIndex, $country->name);
            if ($withCode) {
                $this->setCellValue(3, $rowIndex, $country->code);
            }
        }
        $this->setCellValue(2, ++$rowIndex, 'Σ');
        $this->setBlue(2, $rowIndex);

        $this->sheet->getColumnDimension('A')->setWidth(3);
        $this->sheet->getColumnDimension('B')->setWidth(15);
        $this->sheet->getColumnDimension('C')->setWidth(5);
    }

    protected function setWidth(int $firstColumnIndex, int $lastColumnIndex, $width = 5): void
    {
        foreach (range($firstColumnIndex, $lastColumnIndex) as $columnIndex) {
            $this->sheet->getColumnDimensionByColumn($columnIndex)->setWidth($width);
        }
    }
}
