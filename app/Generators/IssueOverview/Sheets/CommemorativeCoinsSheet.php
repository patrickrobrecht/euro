<?php

namespace App\Generators\IssueOverview\Sheets;

use App\Models\Country;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CommemorativeCoinsSheet extends AbstractCoinsSheet
{
    private const HEADER_ROW = 3;

    public function __construct(Worksheet $sheet)
    {
        parent::__construct($sheet, __('2€ commemorative coins'), self::HEADER_ROW);
    }

    protected function getCoins(Country $country)
    {
        return $country->commemorativeCoins;
    }
}
