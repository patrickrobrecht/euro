<?php

namespace App\Generators\IssueOverview\Sheets;

use App\Generators\IssueOverview\IssueCount;
use App\Models\Coin;
use App\Models\Country;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class AbstractCoinsSheet extends AbstractSheet
{
    protected const FIRST_YEAR_COLUMN = 3;

    private $headerRow;
    protected $countries;
    protected $years;

    public function __construct(Worksheet $sheet, string $title, int $headerRow)
    {
        $this->headerRow = $headerRow;
        $this->loadData();
        parent::__construct($sheet, $title);
    }

    private function loadData(): void
    {
        $countries = [];
        $years = [];

        /* @var $country Country */
        foreach (Country::withEuro() as $country) {
            $country->issueCount = new IssueCount();

            /** @var $coin Coin */
            foreach ($this->getCoins($country) as $coin) {
                foreach ($coin->details as $detail) {
                    $country->issueCount->addIssue($detail->year);
                }
            }

            $years = array_unique(array_merge($years, $country->issueCount->getYears()), SORT_NUMERIC);
            $countries[] = $country;
        }

        $this->countries = $countries;
        $this->years = range(min($years), max($years));
    }

    abstract protected function getCoins(Country $country);

    protected function init(): void
    {
        $header = array_merge([__('Country')], $this->years, [__('Σ country')]);
        $this->addHeaderColumns($header, $this->headerRow);

        $this->addCountries($this->headerRow);

        // Fill issue count data.
        $total = 0;
        $yearColumnIndex = self::FIRST_YEAR_COLUMN;
        foreach ($this->years as $year) {
            $countryRowIndex = $this->headerRow;

            $yearTotal = 0;
            foreach ($this->countries as $country) {
                $countryRowIndex++;
                $count = $country->issueCount->getCount($year);
                if ($count > 0) {
                    // country/year cell.
                    $this->sheet->setCellValueByColumnAndRow($yearColumnIndex, $countryRowIndex, $count);
                }
                $yearTotal += $count;
            }
            $total += $yearTotal;

            // Row below last country: sum for each year.
            $this->sheet->setCellValueByColumnAndRow($yearColumnIndex, $countryRowIndex + 1, $yearTotal);

            $yearColumnIndex++;
        }
        $this->setWidth(self::FIRST_YEAR_COLUMN, $yearColumnIndex - 1, 5);

        // Total.
        $countryTotalColumn = self::FIRST_YEAR_COLUMN + count($this->years);
        $this->setCellValue($countryTotalColumn, $countryRowIndex + 1, $total);

        $this->setBold(
            self::FIRST_YEAR_COLUMN,
            $countryRowIndex + 1,
            $countryTotalColumn,
            $countryRowIndex + 1,
        );

        // Column after last year: sum for the country.
        $index = $this->headerRow;
        foreach ($this->countries as $country) {
            $this->sheet->setCellValueByColumnAndRow(
                $yearColumnIndex,
                ++$index,
                $country->issueCount->getTotalCount()
            );
        }
        $this->setWidth($yearColumnIndex, $yearColumnIndex, 10);

        $this->setBold(
            $yearColumnIndex,
            $this->headerRow + 1,
            $yearColumnIndex,
            $this->headerRow + count($this->countries)
        );
    }
}
