<?php

namespace App\Generators\Catalog\Files;

abstract class CatalogFile extends LatexFile
{
    protected $locale;
    private $fileName;

    public function __construct(array $locale, string $fileName)
    {
        parent::__construct();

        $this->locale = $locale;
        $this->fileName = $fileName;
    }

    public function getFileName()
    {
        return $this->fileName;
    }
}
