<?php

namespace App\Generators\Catalog\Files\CoinsFiles;

use App\Generators\Catalog\CatalogGenerator;
use App\Models\Coin;
use App\Models\Country;
use Illuminate\Support\Facades\Storage;

class CommemorativeCoinsFile extends CoinsFile
{
    public function __construct(array $locale, Country $country)
    {
        parent::__construct($locale, 'commemorative-coins', $country);

        $this->section(__('2€ commemorative coins'));

        /* @var $coin Coin */
        foreach ($country->commemorativeCoins as $coin) {
            $designersList = [];
            $engraversList = [];
            foreach ($coin->designerDetails as $designerDetails) {
                if ($designerDetails->role === 'ENGRAVING') {
                    $engraversList[] = $designerDetails;
                } else {
                    $designersList[] = $designerDetails;
                }
            }

            $designers = CoinsFile::designersList(__('Design'), $designersList);
            $engravers = CoinsFile::designersList(__('Engraving'), $engraversList);

            $this->newLine()
                ->noindent()
                // image on the left
                ->beginMinipage(0.175)
                ->image(self::coinImage($coin), '25.75mm')
                ->endMinipage()
                // and text on the right
                ->beginMinipage(0.825)
                ->boldParagraph($coin->formatted_issue_date . ', ' . $coin->name)
                ->paragraphIf($coin->description)
                ->addLineIf($designers . ($designers && $engravers ? ', ' : '') . $engravers)
                ->endMinipage();

            // ... and the details below
            $this->coinDetailsTable($coin);
        }
    }

    private static function coinImage(Coin $coin): string
    {
        if (Storage::disk('public')->exists($coin->image)) {
            return CatalogGenerator::IMAGE_ROOT_PATH . $coin->image;
        }
        return CatalogGenerator::IMAGE_ROOT_PATH . 'images/2euro/missing';
    }
}
