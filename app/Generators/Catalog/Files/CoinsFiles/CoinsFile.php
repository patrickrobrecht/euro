<?php

namespace App\Generators\Catalog\Files\CoinsFiles;

use App\Generators\Catalog\CatalogGenerator;
use App\Generators\Catalog\Files\CatalogFile;
use App\Generators\Catalog\Files\LatexFile;
use App\Models\Coin;
use App\Models\CoinDetails;
use App\Models\Country;
use App\Models\MintDetails;

abstract class CoinsFile extends CatalogFile
{
    protected $country;

    public function __construct(array $locale, string $fileName, Country $country)
    {
        parent::__construct($locale, $country->slug . '/' . $fileName);

        $this->country = $country;
    }

    public static function designersList(string $role, array $designersList): string
    {
        if (count($designersList) === 0) {
            return '';
        }

        $designers = [];
        foreach ($designersList as $designersListItem) {
            $name = sprintf(
                '%s %s',
                $designersListItem->designer->first_name,
                $designersListItem->designer->last_name
            );
            $designers[] = $name;
        }

        return $role . ': ' . implode(', ', $designers);
    }

    public function coinDetailsTable(Coin $coin)
    {
        if (count($coin->details) === 0) {
            return '';
        }

        $this->begin('longtable', 'p{15mm}p{20mm}p{35mm}p{60mm}r')
            ->row(
                __('Year'),
                __('Mint'),
                __('Mint mark'),
                __('Mint master mark'),
                __('Mintage')
            )
            ->appendLine('\toprule')
            ->appendLine('\endhead');

        /* @var $coinDetails CoinDetails */
        foreach ($coin->details as $coinDetails) {
            /* @var $mintDetails MintDetails */
            $mintDetails = $coinDetails->mintDetails;
            $this->row(
                $coinDetails->year,
                $mintDetails->mint ? $mintDetails->mint->name : __('unknown'),
                self::mark($mintDetails->mintMark),
                self::mintMasterMark($mintDetails),
                $coinDetails->formatted_mintage
            );
        }

        return $this->appendLine('\bottomrule')
            ->end();
    }

    public static function mark($mark): LatexFile
    {
        if (!$mark) {
            return self::new()->text(__('none'));
        }

        if ($mark->type === 'IMAGE') {
            return self::new()->image(CatalogGenerator::IMAGE_ROOT_PATH . $mark->text, '5mm', true);
        }

        return self::new()->text($mark->text);
    }

    public static function mintMasterMark(MintDetails $mintDetails): LatexFile
    {
        $latex = self::mark($mintDetails->mintMasterMark);

        $mintMaster = $mintDetails->mintMaster;
        if ($mintMaster) {
            $latex->text(sprintf('~ %s %s %s', __('for'), $mintMaster->first_name, $mintMaster->last_name));
        }

        return $latex;
    }
}
