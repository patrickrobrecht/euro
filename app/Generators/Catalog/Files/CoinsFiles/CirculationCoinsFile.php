<?php

namespace App\Generators\Catalog\Files\CoinsFiles;

use App\Generators\Catalog\CatalogGenerator;
use App\Models\Coin;
use App\Models\CoinSeries;
use App\Models\Country;

class CirculationCoinsFile extends CoinsFile
{
    public function __construct(array $locale, Country $country)
    {
        parent::__construct($locale, 'circulation-coins', $country);

        $this->section(__('Circulation coins'));

        /* @var $series CoinSeries */
        foreach ($country->circulationCoinSeries as $series) {
            $this->newLine()
                ->subsection(sprintf('%s (%s)', $series->name, $series->period));

            /* @var $coin Coin */
            foreach ($series->coins as $coin) {
                $this->image(CatalogGenerator::IMAGE_ROOT_PATH . $coin->image, self::imageSize($coin) . 'mm');
            }
        }
    }

    private static function imageSize(Coin $coin): ?float
    {
        switch ($coin->denomination) {
            case 0.01:
                return 16.25;
            case 0.02:
                return 18.75;
            case 0.05:
                return 21.25;
            case 0.10:
                return 19.75;
            case 0.20:
                return 22.25;
            case 0.50:
                return 24.25;
            case 1.00:
                return 23.25;
            case 2.00:
                return 25.75;
        }
    }
}
