<?php

namespace App\Generators\Catalog\Files;

use App\Models\Country;

class IndexFile extends CatalogFile
{
    public function __construct(array $locale)
    {
        parent::__construct($locale, __('catalog_file_name'));

        $this->comment('!TeX encoding = UTF-8')
            ->comment(sprintf('!TeX spellcheck = %s', $locale['code']))
            ->newLine()
            ->def('Title', __('Euro Coin Catalog'))
            ->def('SubTitle', __('Circulation coins and 2€ commemorative coins'))
            ->def('Author', 'Patrick Robrecht')
            ->def('Date', date(__('date_format')))
            ->newLine()
            ->input('commands')
            ->usePackage('babel', $locale['latex'])
            ->newLine()
            ->meta(
                self::new()->appendUnescaped('\Title'),
                self::new()->appendUnescaped('\Author'),
                self::new()->appendUnescaped('\Date')
            );

        $this->begin('document');

        // titlepage
        $this->begin('titlepage')
            ->begin('center')
            ->paragraph('~', 150)
            ->fontSize(10)
            ->boldParagraph(LatexFile::new()->appendUnescaped('\Title'), 50)
            ->fontSize(7)
            ->paragraph(LatexFile::new()->appendUnescaped('\SubTitle'), 75)
            ->fontSize(8)
            ->paragraph(LatexFile::new()->appendUnescaped('\Date'))
            ->end()
            ->end();

        // second page
        $this->newPage()
            ->paragraph('~', 550)
            ->noindent()
            ->paragraph(sprintf('2011-%s Creative Commons 4.0 BY-NC,', date('Y')))
            ->url(__('license'))
            ->newLine();

        // table of contents
        $this->newPage()
            ->appendUnescapedLine(sprintf('\pdfbookmark[1]{%s}{toc}', __('Table of Contents')))
            ->tableOfContents()
            ->newLine();

        // introduction chapter
        $this->input('intro')
            ->newLine();

        // country chapters
        foreach (Country::withEuro() as $country) {
            $basePath = './' . strtolower($country->slug) . '/';
            $this->chapter($country->name)
                ->input($basePath . 'circulation-coins')
                ->input($basePath . 'commemorative-coins')
                ->newLine();
        }

        // appendix
        $this->appendix()
            ->input('eurozone')
            ->input('eurozone2')
            ->input('chronicle')
            ->input('mints')
            ->input('coin-designers');

        $this->end();
    }
}
