<?php

namespace App\Generators\Catalog\Files\AppendixFiles;

use App\Generators\Catalog\Files\CatalogFile;
use App\Generators\Catalog\Files\LatexFile;
use App\Models\Designer;
use App\Options\Sex;

class DesignersFile extends CatalogFile
{
    public function __construct(array $locale)
    {
        parent::__construct($locale, 'coin-designers');

        $this->chapter(__('Coin designers'))
            ->newLine()
            ->beginItemize();

        foreach (Designer::allSortedByName()->get() as $designer) {
            $this->item(self::designerItem($designer));
        }

        $this->endItemize();
    }

    private static function designerItem(Designer $designer): LatexFile
    {
        $title = $designer->sex === Sex::FEMALE ? __('Coin designer female') : __('Coin designer');

        if ($designer->country) {
            $title = __(':title from :country', ['title' => $title, 'country' => $designer->country->name]);
        }

        $latex = self::new()
            ->text($designer->first_name ? ($designer->first_name . ' ') : '')
            ->boldText($designer->last_name)
            ->text(', ' . $title);

        if ($designer->birthday) {
            $birthday = date_format(date_create($designer->birthday), __('date_format'));
            if ($designer->day_of_death) {
                $dayOfDeath = date_format(date_create($designer->day_of_death), __('date_format'));
                $latex->text(', ' . __('born :birthday, died :day_of_death', [
                        'birthday' => $birthday,
                        'day_of_death' => $dayOfDeath
                    ]));
            } else {
                $latex->text(', ' . __('born :birthday', ['birthday' => $birthday]));
            }
        }

        return $latex;
    }
}
