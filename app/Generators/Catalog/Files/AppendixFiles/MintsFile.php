<?php

namespace App\Generators\Catalog\Files\AppendixFiles;

use App\Generators\Catalog\CatalogGenerator;
use App\Generators\Catalog\Files\CatalogFile;
use App\Generators\Catalog\Files\LatexFile;
use App\Models\Mark;
use App\Models\Mint;
use App\Models\MintMaster;
use Illuminate\Support\Facades\Storage;

class MintsFile extends CatalogFile
{
    public function __construct(array $locale)
    {
        parent::__construct($locale, 'mints');

        $this->chapter(__('Mints'))
            ->newLine()
            ->beginDescription();

        /* @var $mint Mint */
        foreach (Mint::all() as $mint) {
            $itemContent =
                self::new()
                    ->label('mint:' . $mint->name)
                    ->appendUnescapedLine("~ \\\\")
                    ->paragraph($mint->local_name);

            if ($mint->website) {
                $itemContent->url($mint->website)
                    ->endParagraph();
            }

            if ($mint->mintMasters->count() === 0 && $mint->mintMarks()->count() === 0) {
                $itemContent->text(
                    __('Coins minted in :city have neither a mint mark nor a mint master mark.', [
                        'city' => $mint->name
                    ])
                );
            } else {
                $itemContent->begin('longtable', 'Rp{135mm}');

                /* @var $mark Mark */
                foreach ($mint->mintMarks() as $mark) {
                    $itemContent->row(
                        self::mark($mark),
                        $mark->description ?: $mark->name
                    );
                }

                /* @var $mintMaster MintMaster */
                foreach ($mint->mintMasters as $mintMaster) {
                    foreach ($mintMaster->marks() as $mark) {
                        $for = sprintf(
                            '%s %s, %s',
                            $mintMaster->first_name,
                            $mintMaster->last_name,
                            $mintMaster->title
                        );
                        $itemContent->row(
                            self::mark($mark),
                            __(':mark for :for', [
                                'mark' => $mark->description ?: $mark->name,
                                'for' => $for
                            ])
                        );
                    }
                }

                $itemContent->end('longtable');
            }

            $this->item(
                $itemContent,
                $mint->name . ', ' . $mint->country->name
            );
        }

        $this->endDescription();
    }

    private static function mark(Mark $mark): LatexFile
    {
        $latex = self::new();

        if ($mark->type === 'IMAGE') {
            if (Storage::disk('public')->exists($mark->text)) {
                return $latex->image(CatalogGenerator::IMAGE_ROOT_PATH . $mark->text, '5mm');
            }

            return $latex->text('?');
        }

        return $latex->boldText($mark->text);
    }
}
