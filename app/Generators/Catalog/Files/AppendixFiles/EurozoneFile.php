<?php

namespace App\Generators\Catalog\Files\AppendixFiles;

use App\Generators\Catalog\Files\CatalogFile;
use App\Models\Country;

class EurozoneFile extends CatalogFile
{
    public function __construct(array $locale)
    {
        parent::__construct($locale, 'eurozone');

        $this->chapter(__('Euro countries'))
            ->paragraph(
                // phpcs:ignore Generic.Files.LineLength.TooLong
                __(':euMembers member countries of the European Union (EU) use the euro (€) as their common currency.', [
                    'euMembers' => Country::euMemberCount()
                ])
                . ' ' .
                // phpcs:ignore Generic.Files.LineLength.TooLong
                __('In addition, Andorra, San Marino, Monaco, and Vatican City issue euro coins due to formal agreements with the EU.')
            )
            ->newLine()
            ->begin('longtable', 'llll')
            ->row(
                __('Country name'),
                __('Official country name'),
                __('Country code'),
                __('Euro since')
            )
            ->appendUnescapedLine('\toprule');

        $i = 0;
        foreach (Country::withEuro() as $country) {
            $this->row(
                $country->name,
                $country->full_name,
                $country->code,
                $country->euro_since
            );
            $i++;
            if ($i % 5 === 0) {
                $this->appendUnescapedLine('\midrule');
            }
        }
        $this->appendUnescapedLine('\bottomrule')
            ->caption(__('Euro countries'))
            ->label('tab:euro-countries')
            ->end('longtable');
    }
}
