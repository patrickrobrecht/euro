<?php

namespace App\Generators\Catalog\Files\AppendixFiles;

use App\Generators\Catalog\Files\CatalogFile;
use App\Models\ChronicleEntry;

class ChronicleFile extends CatalogFile
{
    public function __construct(array $locale)
    {
        parent::__construct($locale, 'chronicle');

        $this->chapter(__('Chronicle'))
            ->newLine()
            ->beginDescription();

        foreach (ChronicleEntry::all() as $entry) {
            $this->item($entry->text, $entry->formatted_date . ($entry->title ? ': ' . $entry->title : ''));
        }

        $this->endDescription();
    }
}
