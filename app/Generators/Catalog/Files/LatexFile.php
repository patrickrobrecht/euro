<?php

namespace App\Generators\Catalog\Files;

class LatexFile
{
    private $contents;
    private $tabs = 0;
    private $environments = [];

    public function __construct()
    {
        $this->contents = '';
    }

    public function __toString()
    {
        return $this->contents;
    }

    public static function new(): LatexFile
    {
        return new self();
    }

    private function generateTabs(): string
    {
        return str_repeat("\t", $this->tabs);
    }

    public function usePackage(string $package, $options = ''): LatexFile
    {
        return $this->appendLine('\usepackage[%s]{%s}', $options, $package);
    }

    public function meta($title, $author = '', $date = ''): LatexFile
    {
        return $this->appendLine('\title{%s}', $title)
            ->appendLine('\author{%s}', $author)
            ->appendLine('\date{%s}', $date)
            ->newLine();
    }

    public function def(string $var, string $value): LatexFile
    {
        return $this->appendLine('\def\%s{%s}', $var, $value);
    }

    // HEADLINES

    public function chapter(string $headline): LatexFile
    {
        return $this->appendLine('\chapter{%s}', $headline);
    }

    public function section(string $headline): LatexFile
    {
        return $this->appendLine('\section{%s}', $headline);
    }

    public function subsection(string $headline): LatexFile
    {
        return $this->appendLine('\subsection{%s}', $headline);
    }

    public function label(string $label): LatexFile
    {
        return $this->appendLine('\label{%s}', $label);
    }

    public function appendix(): LatexFile
    {
        return $this->appendLine('\appendix');
    }

    public function tableOfContents(): LatexFile
    {
        return $this->appendLine('\tableofcontents');
    }

    // TEXT and PARAGRAPHS

    public function text(string $text): LatexFile
    {
        return $this->append('%s', $text);
    }

    public function url(string $url): LatexFile
    {
        return $this->append('\url{%s}', $url);
    }

    public function boldText(string $text): LatexFile
    {
        return $this->append('\textbf{%s}', $text);
    }

    public function endParagraph($padding = 0): LatexFile
    {
        return $this->appendUnescapedLine(' \\\\' .
            ($padding === 0 ? '' : sprintf('[%spt]', $padding)));
    }

    public function paragraph(string $text, $padding = 0): LatexFile
    {
        return $this->text($text)->endParagraph($padding);
    }

    public function paragraphIf($text, $condition = null): LatexFile
    {
        if ($condition || ($condition === null && $text)) {
            return $this->paragraph($text);
        }
        return $this;
    }

    public function boldParagraph(string $text, $padding = 0): LatexFile
    {
        return $this->boldText($text)->endParagraph($padding);
    }

    public function noindent(): LatexFile
    {
        return $this->appendLine('\noindent');
    }

    public function fontSize(int $size): LatexFile
    {
        switch ($size) {
            case 1:
                return $this->appendLine('\tiny');
            case 2:
                return $this->appendLine('\scriptsize');
            case 3:
                return $this->appendLine('\footnotesize');
            case 4:
                return $this->appendLine('\small');
            case 5:
            default:
                return $this->appendLine('\normal');
            case 6:
                return $this->appendLine('\large');
            case 7:
                return $this->appendLine('\Large');
            case 8:
                return $this->appendLine('\LARGE');
            case 9:
                return $this->appendLine('\huge');
            case 10:
                return $this->appendLine('\Huge');
        }
    }

    // environments

    public function begin($environment, $arg = null): LatexFile
    {
        $this->appendLine('\begin{%s}' . ($arg === null ? '' : '{' .  $arg . '}'), $environment);
        $this->tabs++;
        $this->environments[] = $environment;
        return $this;
    }

    public function end($environment = null): LatexFile
    {
        if ($environment === null) {
            $environment = array_pop($this->environments);
        }
        $this->tabs--;
        return $this->appendLine('\end{%s}', $environment);
    }

    public function beginDescription(): LatexFile
    {
        return $this->begin('description');
    }

    public function endDescription(): LatexFile
    {
        return $this->end('description');
    }

    public function beginItemize(): LatexFile
    {
        return $this->begin('itemize');
    }

    public function endItemize(): LatexFile
    {
        return $this->end('itemize');
    }

    public function item($text, $index = null): LatexFile
    {
        if ($text instanceof self) {
            $this->appendLine(isset($index) ? '\item[%s]' : '\item', $index);
            $this->tabs++;
            $this->appendUnescapedLines($text);
            $this->tabs--;
            return $this;
        }

        $format = '\item' . (isset($index) ? ('[%s]' . PHP_EOL . "\t\t %s") : ' %2$s');
        return $this->appendLine($format, $index, $text);
    }

    public function beginMinipage(float $width): LatexFile
    {
        $this->appendLine('\begin{minipage}{%1f\textwidth}', $width);
        $this->tabs++;
        return $this;
    }

    public function endMinipage(): LatexFile
    {
        return $this->end('minipage');
    }

    // TABLES

    public function row(...$args): LatexFile
    {
        $format = str_repeat('%s & ', count($args) - 1);
        return $this->appendLine($format . "%s \\\\", ...$args);
    }

    public function caption(string $headline): LatexFile
    {
        return $this->appendLine('\caption{%s}', $headline);
    }

    // FILES

    public function input(string $path): LatexFile
    {
        return $this->appendLine('\input{%s}', $path);
    }

    public function newPage(): LatexFile
    {
        return $this->newLine()
            ->appendLine('\newpage');
    }

    public function image(string $image, string $height = null, $inline = false): LatexFile
    {
        $format = '\includegraphics' . ($height ? '[height=%2$s]' : '') . '{%1$s}';
        if ($inline) {
            return $this->append($format, $image, $height);
        }
        return $this->appendLine($format, $image, $height);
    }

    // OTHER

    public function comment(string $comment): LatexFile
    {
        return $this->appendUnescapedLine('% ' . $comment);
    }

    public function addLineIf($latex, $condition = null): LatexFile
    {
        if ($condition || ($condition === null && $latex)) {
            return $this->appendLine($latex . PHP_EOL);
        }
        return $this;
    }

    public function append(string $format, ...$args): LatexFile
    {
        $this->contents .= $this->generateTabs() . sprintf($format, ...self::escapeArgs($args));
        return $this;
    }

    public function appendLine(string $format, ...$args): LatexFile
    {
        return $this->append($format . PHP_EOL, ...$args);
    }

    public function appendUnescaped(string $text): LatexFile
    {
        $this->contents .= $this->generateTabs() . $text;
        return $this;
    }

    public function appendUnescapedLine(string $text): LatexFile
    {
        return $this->appendUnescaped($text . PHP_EOL);
    }

    public function appendUnescapedLines(string $text): LatexFile
    {
        foreach (explode(PHP_EOL, $text) as $line) {
            $this->appendUnescapedLine($line);
        }
        return $this;
    }

    private static function escape($text)
    {
        return str_replace(
            ['{', '}', '&', '#'],
            ["\\{", "\\}", "\\&", "\\#"],
            $text
        );
    }

    private static function escapeArgs($args): array
    {
        return array_map(
            static function ($i) {
                if ($i instanceof LatexFile) {
                    return $i;
                }
                return self::escape($i);
            },
            $args
        );
    }

    public function newLine($lines = 1): LatexFile
    {
        $this->contents .= str_repeat(PHP_EOL, $lines);
        return $this;
    }
}
