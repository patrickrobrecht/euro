<?php

namespace App\Generators\Catalog;

use App\Generators\Catalog\Files\CatalogFile;
use App\Generators\Catalog\Files\AppendixFiles\ChronicleFile;
use App\Generators\Catalog\Files\AppendixFiles\DesignersFile;
use App\Generators\Catalog\Files\AppendixFiles\EurozoneFile;
use App\Generators\Catalog\Files\AppendixFiles\MintsFile;
use App\Generators\Catalog\Files\CoinsFiles\CirculationCoinsFile;
use App\Generators\Catalog\Files\CoinsFiles\CommemorativeCoinsFile;
use App\Generators\Catalog\Files\IndexFile;
use App\Models\Country;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;

class CatalogGenerator
{
    /**
     * @var array the locale
     */
    private $locale;

    /**
     * @var string the path to the locale directory on the "temp" disk
     */
    private $localeDirectory;

    /**
     * The path to the "public/images" directory, relative from the locale directory.
     */
    public const IMAGE_ROOT_PATH = '../../../../public/';

    public function __construct(array $locale)
    {
        $this->locale = $locale;
        $this->localeDirectory = 'catalog/' . strtolower($locale['slug']) . '/';
    }

    public function generate(): void
    {
        App::setLocale($this->locale['slug']);

        // files from "latex" directory
        $fileNames = [
            'commands.tex',
            'eurozone2.LANG.tex',
            'intro.LANG.tex'
        ];

        foreach ($fileNames as $fileName) {
            $sourceFileName = str_replace('LANG', $this->locale['slug'], $fileName);
            try {
                $fileContents = Storage::disk('data')->get('latex/' . $sourceFileName);
                $fileContents = str_replace('IMAGE_ROOT_PATH/', self::IMAGE_ROOT_PATH, $fileContents);

                $targetFileName = str_replace('.LANG', '', $fileName);
                Storage::disk('temp')->put(sprintf('%s/%s', $this->localeDirectory, $targetFileName), $fileContents);
            } catch (FileNotFoundException $e) {
                echo sprintf('Missing file %s' . PHP_EOL, $sourceFileName);
            }
        }

        // main file
        $this->createFile(new IndexFile($this->locale));

        // catalog
        foreach (Country::withEuro() as $country) {
            $this->createFile(new CirculationCoinsFile($this->locale, $country));
            $this->createFile(new CommemorativeCoinsFile($this->locale, $country));
        }

        // appendix files
        $this->createFile(new EurozoneFile($this->locale));
        $this->createFile(new ChronicleFile($this->locale));
        $this->createFile(new MintsFile($this->locale));
        $this->createFile(new DesignersFile($this->locale));
    }

    private function createFile(CatalogFile $file): void
    {
        Storage::disk('temp')->put(sprintf('%s/%s.tex', $this->localeDirectory, $file->getFileName()), $file);
    }
}
