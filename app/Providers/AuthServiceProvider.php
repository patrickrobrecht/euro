<?php

namespace App\Providers;

use App\Models\ChronicleEntry;
use App\Models\Coin;
use App\Models\CoinSeries;
use App\Models\Country;
use App\Models\Designer;
use App\Models\Mint;
use App\Models\User;
use App\Policies\ChronicleEntryPolicy;
use App\Policies\CoinPolicy;
use App\Policies\CoinSeriesPolicy;
use App\Policies\CountryPolicy;
use App\Policies\DesignerPolicy;
use App\Policies\MintPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        ChronicleEntry::class => ChronicleEntryPolicy::class,
        Coin::class => CoinPolicy::class,
        CoinSeries::class => CoinSeriesPolicy::class,
        Country::class => CountryPolicy::class,
        Designer::class => DesignerPolicy::class,
        Mint::class => MintPolicy::class,
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();
    }
}
