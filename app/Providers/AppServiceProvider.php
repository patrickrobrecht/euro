<?php

namespace App\Providers;

use App\Models\Country;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        Paginator::useBootstrap();

        // Data shared by all views.
        View::composer('*', static function ($view) {
            $view->with('countries', Country::withEuro());
        });
    }
}
