<?php

namespace App\Providers;

use App\Models\ChronicleEntry;
use App\Models\Coin;
use App\Models\CoinSeries;
use App\Models\Country;
use App\Models\Designer;
use App\Models\Mint;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map(): void
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes(): void
    {
        Route::middleware('web')
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes(): void
    {
        Route::prefix('api')
            ->middleware('api')
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot(): void
    {
        Route::model('chronicleEntryId', ChronicleEntry::class);

        Route::bind('circulationCoinSeries', function ($value, $route) {
            $coinSeries = CoinSeries::query()->whereTranslation('slug', $value, App::getLocale());
            if (Request::route()->hasParameter('country')) {
                /** @var Country $country */
                $country = Request::route('country');
                $coinSeries->where('country_id', $country->id);
            }

            return $coinSeries->firstOrFail();
        });

        Route::model('coinId', Coin::class);
        Route::model('coinSeriesId', CoinSeries::class);

        Route::bind('country', function ($value) {
            return Country::findBySlug($value, App::getLocale());
        });
        Route::model('countryId', Country::class);

        Route::bind('designer', function ($value) {
            return Designer::findBySlug($value);
        });
        Route::model('designerId', Designer::class);

        Route::bind('mint', function ($value) {
            return Mint::findBySlug($value, App::getLocale());
        });
        Route::model('mintId', Mint::class);

        Route::model('userId', User::class);
    }
}
