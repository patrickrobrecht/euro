<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class MarkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->when($this->name, $this->name),
            'description' => $this->when($this->description, $this->description),
            'text' => $this->when($this->type === 'TEXT', $this->text),
            'image' => $this->when($this->type === 'IMAGE', Storage::disk('public')->url($this->text)),
        ];
    }
}
