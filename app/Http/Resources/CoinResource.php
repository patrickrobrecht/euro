<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Storage;

class CoinResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->name,
            'type' => $this->type,
            'denomination' => $this->denomination,
            'issue_date' => $this->issue_date,
            'image' => Storage::disk('public')->url($this->image),
            'designers' => new CoinDesignerResourceCollection($this->designerDetails),
            'country' => new CountryResource($this->whenLoaded('country')),
            'series' => new CoinSeriesResource($this->whenLoaded('series')),
        ];
    }
}
