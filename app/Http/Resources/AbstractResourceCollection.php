<?php

namespace App\Http\Resources;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\AbstractPaginator;

class AbstractResourceCollection extends ResourceCollection
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function toResponse($request): JsonResponse
    {
        return $this->resource instanceof AbstractPaginator
            ? (new PageSizeAwareResourceReponse($this))->toResponse($request)
            : parent::toResponse($request);
    }
}
