<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CoinSeriesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'code' => $this->code,
            'first_year' => $this->first_year,
            'last_year' => $this->last_year,
            'type' => $this->type,
            'coins' => new CoinResourceCollection($this->whenLoaded('coins')),
            'country' => new CountryResource($this->whenLoaded('country')),
        ];
    }
}
