<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MintResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'name' => $this->name,
            'slug' => $this->slug,
            'local_name' => $this->local_name,
            'website' => $this->website,
            'country' => new CountryResource($this->country),
            'mint_masters' => new MintMasterResourceCollection($this->mintMasters)
        ];
    }
}
