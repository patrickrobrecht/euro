<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DesignerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'slug' => $this->slug,
            'sex' => $this->sex,
            'birthday' => $this->when($this->birthday, $this->birthday),
            'day_of_death' => $this->when($this->day_of_death, $this->day_of_death),
            'website' => $this->when($this->website, $this->website),
            'country' => new CountryResource($this->whenLoaded('country')),
        ];
    }
}
