<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\PaginatedResourceResponse;

class PageSizeAwareResourceReponse extends PaginatedResourceResponse
{
    private const URL_KEYS = [
        'first_page_url',
        'last_page_url',
        'prev_page_url',
        'next_page_url',
    ];

    /**
     * Get the pagination links for the response.
     *
     * @param  array  $paginated
     * @return array
     */
    protected function paginationLinks($paginated): array
    {
        /**
         * 15 is the default page size defined in @see Model::$perPage
         */
        if ($paginated['per_page'] !== 15) {
            $suffix = sprintf('&page_size=%s', $paginated['per_page']);
            foreach (self::URL_KEYS as $key) {
                if ($paginated[$key]) {
                    $paginated[$key] .= $suffix;
                }
            }
        }

        return [
            'first' => $paginated['first_page_url'] ?? null,
            'last' => $paginated['last_page_url'] ?? null,
            'prev' => $paginated['prev_page_url'] ?? null,
            'next' => $paginated['next_page_url'] ?? null,
        ];
    }
}
