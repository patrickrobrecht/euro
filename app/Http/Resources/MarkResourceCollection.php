<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;

class MarkResourceCollection extends AbstractResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return parent::toArray($request);
    }
}
