<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CountryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'code' => $this->code,
            'euro_since' => $this->euro_since,
            'image' => Storage::disk('public')->url($this->image),
            'slug' => $this->slug,
            'name' => $this->name,
            'full_name' => $this->full_name,
        ];
    }
}
