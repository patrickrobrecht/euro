<?php

namespace App\Http\Requests;

use App\Http\Controllers\DesignerController;
use App\Http\Requests\Traits\AuthorizationViaPolicyInController;
use App\Models\Designer;
use App\Options\Sex;
use App\Policies\DesignerPolicy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class DesignerRequest
 * @package App\Http\Requests
 *
 * Validation for {@see Designer}
 */
class DesignerRequest extends FormRequest
{
    use AuthorizationViaPolicyInController; /** {@see DesignerPolicy} in {@see DesignerController} */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'required',
                'string',
                'max:255',
            ],
            'last_name' => [
                'required',
                'string',
                'max:255',
            ],
            'sex' => [
                'required',
                Sex::getRule(),
            ],
            'birthday' => [
                'nullable',
                'date_format:Y-m-d',
            ],
            'day_of_death' => [
                'nullable',
                'date_format:Y-m-d',
            ],
            'home_country_id' => [
                'nullable',
                Rule::exists('countries', 'id'),
            ],
            'website' => [
                'nullable',
                'string',
                'max:255',
            ],
        ];
    }
}
