<?php

namespace App\Http\Requests;

use App\Http\Controllers\UserController;
use App\Http\Requests\Traits\AuthorizationViaPolicyInController;
use App\Models\User;
use App\Options\UserRole;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class UserRequest
 * @package App\Http\Requests
 *
 * Validation for {@see User}
 */
class UserRequest extends FormRequest
{
    use AuthorizationViaPolicyInController; /** {@see UserPolicy} in {@see UserController} */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        /** @var User|null $user */
        $user = $this->userId;

        $uniqueRule = Rule::unique('users', 'email');
        if ($user) {
            $uniqueRule->ignoreModel($user);
        }

        return [
            'first_name' => [
                'required',
                'string',
                'max:255',
            ],
            'last_name' => [
                'required',
                'string',
                'max:255',
            ],
            'email' => [
                'required',
                'email:filter',
                'max:255',
                $uniqueRule,
            ],
            'password' => [
                $user ? 'nullable' : 'required',
                'min:10',
                'confirmed',
            ],
            'role' => [
                'nullable',
                UserRole::getRule(),
            ]
        ];
    }
}
