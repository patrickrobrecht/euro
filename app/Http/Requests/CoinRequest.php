<?php

namespace App\Http\Requests;

use App\Http\Controllers\CoinController;
use App\Http\Requests\Traits\AuthorizationViaPolicyInController;
use App\Http\Requests\Traits\RequestForTranslatable;
use App\Models\Coin;
use App\Options\CoinType;
use App\Options\Denomination;
use App\Policies\CoinPolicy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CountryRequest
 * @package App\Http\Requests
 *
 * Validation for {@see Coin}
 */
class CoinRequest extends FormRequest
{
    use AuthorizationViaPolicyInController; /** {@see CoinPolicy} in {@see CoinController} */
    use RequestForTranslatable;

    public function rulesForAttributes(): array
    {
        return [
            'country_id' => [
                'required',
                Rule::exists('countries', 'id'),
            ],
            'coin_series_id' => [
                'nullable',
                Rule::exists('coin_series', 'id')
                    ->where('country_id', $this->country_id ?? 0),
            ],
            'type' => [
                'required',
                CoinType::getRule(),
            ],
            'denomination' => [
                'required',
                Denomination::getRule(),
            ],
            'issue_date' => [
                'nullable',
                'date_format:Y-m-d',
            ],
            'image' => [
                'nullable',
                'string',
                'max:255',
            ],
        ];
    }

    public function rulesForTranslatedAttributes(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'description' => [
                'required',
                'string',
            ],
        ];
    }
}
