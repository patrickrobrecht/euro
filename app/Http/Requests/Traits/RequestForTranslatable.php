<?php

namespace App\Http\Requests\Traits;

trait RequestForTranslatable
{
    abstract public function rulesForAttributes(): array;

    abstract public function rulesForTranslatedAttributes(): array;

    public function rules(): array
    {
        $rules = $this->rulesForAttributes();

        foreach ($this->getLocales() as $locale) {
            foreach ($this->rulesForTranslatedAttributes() as $attribute => $rulesForAttribute) {
                $rules[$attribute . ':' . $locale] = $rulesForAttribute;
            }
        }

        return $rules;
    }

    public function attributes(): array
    {
        return $this->translatedAttributes();
    }

    protected function translatedAttributes(): array
    {
        $attributes = [];

        foreach ($this->getLocales() as $locale) {
            foreach (array_keys($this->rulesForTranslatedAttributes()) as $attribute) {
                $attributes[$attribute . ':' . $locale] = __('validation.attributes.' . $attribute);
            }
        }

        return $attributes;
    }

    public function getLocales(): array
    {
        return array_keys(config('app.locales'));
    }
}
