<?php

namespace App\Http\Requests\Traits;

/**
 * Trait AuthorizationViaPolicyInController
 * @package App\Http\Requests\Traits
 *
 * Disables the authorization check in the request in favor of handling the check in the responding controller.
 */
trait AuthorizationViaPolicyInController
{
    public function authorize(): bool
    {
        return true;
    }
}
