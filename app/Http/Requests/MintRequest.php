<?php

namespace App\Http\Requests;

use App\Http\Controllers\MintController;
use App\Http\Requests\Traits\AuthorizationViaPolicyInController;
use App\Http\Requests\Traits\RequestForTranslatable;
use App\Policies\MintPolicy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CountryRequest
 * @package App\Http\Requests
 *
 * Validation for {@see Mint}
 */
class MintRequest extends FormRequest
{
    use AuthorizationViaPolicyInController; /** {@see MintPolicy} in {@see MintController} */
    use RequestForTranslatable;

    public function rulesForAttributes(): array
    {
        return [
            'local_name' => [
                'required',
                'string',
                'max:255',
            ],
            'country_id' => [
                'required',
                Rule::exists('countries', 'id'),
            ],
            'website' => [
                'nullable',
                'string',
                'max:255',
            ],
        ];
    }

    public function rulesForTranslatedAttributes(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
            ],
        ];
    }
}
