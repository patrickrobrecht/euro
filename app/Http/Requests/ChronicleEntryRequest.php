<?php

namespace App\Http\Requests;

use App\Http\Controllers\ChronicleController;
use App\Http\Requests\Traits\AuthorizationViaPolicyInController;
use App\Http\Requests\Traits\RequestForTranslatable;
use App\Models\ChronicleEntry;
use App\Policies\ChronicleEntryPolicy;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ChronicleEntryRequest
 * @package App\Http\Requests
 *
 * Validation for {@see ChronicleEntry}
 */
class ChronicleEntryRequest extends FormRequest
{
    use AuthorizationViaPolicyInController; /** {@see ChronicleEntryPolicy} in {@see ChronicleController} */
    use RequestForTranslatable;

    public function rulesForAttributes(): array
    {
        return [
            'date' => [
                'nullable',
                'date_format:Y-m-d',
            ],
            'year' => [
                'required',
                'integer',
                'min:1900',
            ],
        ];
    }

    public function rulesForTranslatedAttributes(): array
    {
        return [
            'title' => [
                'nullable',
                'string',
                'max:255',
            ],
            'text' => [
                'required',
                'string',
            ]
        ];
    }
}
