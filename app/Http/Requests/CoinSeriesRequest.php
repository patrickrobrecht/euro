<?php

namespace App\Http\Requests;

use App\Http\Controllers\CoinSeriesController;
use App\Http\Requests\Traits\AuthorizationViaPolicyInController;
use App\Http\Requests\Traits\RequestForTranslatable;
use App\Models\CoinSeries;
use App\Options\CoinType;
use App\Policies\CoinSeriesPolicy;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class CountryRequest
 * @package App\Http\Requests
 *
 * Validation for {@see CoinSeries}
 */
class CoinSeriesRequest extends FormRequest
{
    use AuthorizationViaPolicyInController; /** {@see CoinSeriesPolicy} in {@see CoinSeriesController} */
    use RequestForTranslatable;

    public function rulesForAttributes(): array
    {
        return [
            'country_id' => [
                'required',
                Rule::exists('countries', 'id'),
            ],
            'type' => [
                'required',
                CoinType::getRule(),
            ],
            'first_year' => [
                'nullable',
                'integer',
                'min:1900',
            ],
            'last_year' => [
                'nullable',
                'integer',
                'min:1900',
                'gte:first_year',
            ],
            'code' => [
                'required',
                'string',
                'max:255',
            ],
        ];
    }

    public function rulesForTranslatedAttributes(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
            ],
        ];
    }
}
