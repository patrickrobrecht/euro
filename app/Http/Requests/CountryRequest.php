<?php

namespace App\Http\Requests;

use App\Http\Controllers\CountryController;
use App\Http\Requests\Traits\AuthorizationViaPolicyInController;
use App\Http\Requests\Traits\RequestForTranslatable;
use App\Models\Country;
use App\Policies\CountryPolicy;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CountryRequest
 * @package App\Http\Requests
 *
 * Validation for {@see Country}
 */
class CountryRequest extends FormRequest
{
    use AuthorizationViaPolicyInController; /** {@see CountryPolicy} in {@see CountryController} */
    use RequestForTranslatable;

    public function rulesForAttributes(): array
    {
        return [
            'code' => [
                'required',
                'string',
                'size:2',
            ],
            'eu_member_since' => [
                'nullable',
                'integer',
                'min:1900',
            ],
            'euro_since' => [
                'nullable',
                'integer',
                'min:1900',
            ],
            'image' => [
                'required',
                'string',
                'max:255',
            ],
        ];
    }

    public function rulesForTranslatedAttributes(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'full_name' => [
                'required',
                'string',
                'max:255',
            ],
        ];
    }
}
