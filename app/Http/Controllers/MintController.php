<?php

namespace App\Http\Controllers;

use App\Http\Requests\MintRequest;
use App\Models\Country;
use App\Models\Mint;
use Illuminate\Support\Facades\Session;

class MintController extends Controller
{
    public function __invoke()
    {
        return view('mints.mints_index', [
            'mints' => Mint::all()->load('country', 'mintMarks'),
        ]);
    }

    public function index()
    {
        $this->authorize('viewAny', Mint::class);

        $mints = Mint::query();

        return view('mints.mints_admin_index', [
            'mints' => $mints->paginate(),
        ]);
    }

    public function create()
    {
        $this->authorize('create', Mint::class);

        return view('mints.mint_form', [
            'allCountries' => Country::all(),
        ]);
    }

    public function store(MintRequest $request)
    {
        $this->authorize('create', Mint::class);

        $mint = new Mint();
        if ($this->saveMint($mint, $request->validated())) {
            Session::flash('success', __('Created successfully.'));
            return redirect()->route('country.edit', $mint->id);
        }

        return back();
    }

    private function saveMint(Mint $mint, array $data): bool
    {
        return $mint->fill($data)->save();
    }

    public function edit(Mint $mint)
    {
        $this->authorize('update', $mint);

        return view('mints.mint_form', [
            'allCountries' => Country::all(),
            'mint' => $mint,
        ]);
    }

    public function update(MintRequest $request, Mint $mint)
    {
        $this->authorize('update', $mint);

        if ($mint && $this->saveMint($mint, $request->validated())) {
            Session::flash('success', __('Saved successfully.'));
        }

        return back();
    }

    public function show(Mint $mint)
    {
        return view('mints.mint', [
            'mint' => $mint->load(
                'country',
                'mintDetails',
                'mintMasters',
            ),
        ]);
    }
}
