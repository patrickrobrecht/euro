<?php

namespace App\Http\Controllers;

use App\Models\CoinSeries;
use App\Models\Country;

class CountryCirculationCoinsController extends Controller
{
    public function __invoke(Country $country)
    {
        return view('countries.circulation-coins', [
            'country' => $country->load(
                'circulationCoinSeries',
                'circulationCoinSeries.coins',
                'circulationCoinSeries.coins.designerDetails.designer',
                'circulationCoinSeries.coins.designerDetails.mark',
            ),
        ]);
    }

    public function showSeries(Country $country, CoinSeries $coinSeries)
    {
        return view('countries.circulation-coin-series', [
            'country' => $country,
            'series' => $coinSeries->load(
                'coins.designerDetails',
                'coins.designerDetails.designer',
                'coins.designerDetails.mark',
                'coins.details.mintDetails.mint',
                'coins.details.mintDetails.mintMark',
                'coins.details.mintDetails.mintMaster',
                'coins.details.mintDetails.mintMasterMark',
            ),
        ]);
    }
}
