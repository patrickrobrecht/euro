<?php

namespace App\Http\Controllers\Api;

class FallbackApiController extends BasicApiController
{
    public function __invoke()
    {
        return $this->notFound();
    }
}
