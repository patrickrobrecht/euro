<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\MarkResource;
use App\Http\Resources\MarkResourceCollection;
use App\Models\Mark;

class MarkApiController extends BasicApiController
{
    public function index(): MarkResourceCollection
    {
        return new MarkResourceCollection(Mark::paginate());
    }

    public function show($id)
    {
        if ((int)$id > 0) {
            $mark = Mark::find((int) $id);
        } else {
            $mark = Mark::findByCode($id);
        }

        if ($mark) {
            return new MarkResource($mark);
        }

        return $this->notFound(__('Mark'), $id);
    }
}
