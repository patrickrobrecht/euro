<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\MintResource;
use App\Http\Resources\MintResourceCollection;
use App\Models\Mint;
use Illuminate\Support\Facades\App;

class MintApiController extends BasicApiController
{
    public function index(): MintResourceCollection
    {
        return new MintResourceCollection(Mint::with('mintMasters')->paginate());
    }

    public function show($id)
    {
        if ((int)$id > 0) {
            $mint = Mint::find((int) $id);
        } else {
            $mint = Mint::findBySlug($id, App::getLocale());
        }

        if ($mint) {
            return new MintResource($mint);
        }

        return $this->notFound(__('Mint'), $id);
    }
}
