<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class BasicApiController extends Controller
{
    protected function notFound(?string $type = null, ?string $query = null)
    {
        return self::error(404, $type, $query);
    }

    private static function error(int $statusCode, ?string $type = null, ?string $query = null)
    {
        return response()->json(
            [
                'error' => [
                    'message' => self::getMessage($type, $query),
                    'code' => $statusCode
                ],
            ],
            $statusCode
        );
    }

    private static function getMessage(?string $type = null, ?string $query = null)
    {
        if ($type) {
            if ($query) {
                return __('There is no API resource :type with ID :query.', ['type' => $type, 'query' => $query]);
            }

            return  __('There is no API resource :type.', ['type' => $type]);
        }

        return __('This API resource does not exist.');
    }
}
