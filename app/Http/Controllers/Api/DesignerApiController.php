<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\DesignerResource;
use App\Http\Resources\DesignerResourceCollection;
use App\Models\Designer;

class DesignerApiController extends BasicApiController
{
    public function index(): DesignerResourceCollection
    {
        return new DesignerResourceCollection(Designer::paginate());
    }

    public function show($id)
    {
        if ((int)$id > 0) {
            $designer = Designer::find((int) $id);
        } else {
            $designer = Designer::findBySlug($id);
        }

        if ($designer) {
            return new DesignerResource($designer->load('country'));
        }

        return $this->notFound(__('Coin designer'), $id);
    }
}
