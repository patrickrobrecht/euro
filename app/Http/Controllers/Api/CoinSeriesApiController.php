<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CoinSeriesResource;
use App\Http\Resources\CoinSeriesResourceCollection;
use App\Models\CoinSeries;
use Illuminate\Support\Facades\App;

class CoinSeriesApiController extends BasicApiController
{
    public function index(): CoinSeriesResourceCollection
    {
        return new CoinSeriesResourceCollection(CoinSeries::paginate());
    }

    public function show($id)
    {
        if ((int)$id > 0) {
            $coinSeries = CoinSeries::find((int) $id);
        } else {
            $coinSeries = CoinSeries::findByCode($id);
            if (!$coinSeries) {
                $coinSeries = CoinSeries::findBySlug($id, App::getLocale());
            }
        }

        if ($coinSeries) {
            return new CoinSeriesResource($coinSeries->load(['coins', 'country']));
        }

        return $this->notFound(__('Series'), $id);
    }
}
