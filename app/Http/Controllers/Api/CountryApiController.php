<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CountryResource;
use App\Http\Resources\CountryResourceCollection;
use App\Models\Country;
use Illuminate\Support\Facades\App;

class CountryApiController extends BasicApiController
{
    public function index(): CountryResourceCollection
    {
        return new CountryResourceCollection(Country::paginate());
    }

    public function show($id)
    {
        if ((int)$id > 0) {
            $country = Country::find((int) $id);
        } elseif (strlen($id) === 2) {
            $country = Country::findByCode($id);
        } else {
            $country =  Country::findBySlug($id, App::getLocale());
        }

        if ($country) {
            return new CountryResource($country);
        }

        return $this->notFound(__('Country'), $id);
    }
}
