<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CoinResource;
use App\Http\Resources\CoinResourceCollection;
use App\Models\Coin;

class CoinApiController extends BasicApiController
{
    public function index(): CoinResourceCollection
    {
        return new CoinResourceCollection(Coin::paginate());
    }

    public function show($id)
    {
        $coin = null;
        if ((int)$id > 0) {
            $coin = Coin::find((int) $id);
        }

        if ($coin) {
            return new CoinResource($coin->load(['country', 'series']));
        }

        return $this->notFound(__('Coin'), $id);
    }
}
