<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Designer;
use App\Models\Mint;
use Illuminate\Support\Facades\App;

class SitemapFileController extends Controller
{
    public function __invoke()
    {
        App::setLocale('de');
        return response()->view('files.sitemap', [
            'countries' => Country::withEuro(),
            'designers' => Designer::all(),
            'mints' => Mint::all()
        ])->header('Content-Type', 'text/xml');
    }
}
