<?php

namespace App\Http\Controllers;

use App\Models\Coin;
use App\Models\CoinSeries;
use App\Models\Country;
use App\Models\Designer;
use App\Models\Mint;
use App\Options\SearchScope;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;

class SearchController extends Controller
{
    /**
     * @var LengthAwarePaginator $searchResults
     */
    private $searchResults;

    /**
     * @var array
     */
    private $searchResultsCountPerScope = [];

    public function __invoke()
    {
        $term = Request::query('term');
        $selectedScope = Request::query('scope', SearchScope::COINS);

        if (!SearchScope::exists($selectedScope)) {
            abort(403, __('Invalid scope.'));
        }

        if ($term) {
            $this->loadResults($selectedScope, $term);
        }

        return view('search.results', [
            'searchResults' => $this->searchResults,
            'searchResultsCount' => $this->getTotal(),
            'searchResultsCountPerScope' => $this->searchResultsCountPerScope,
            'searchScope' => $selectedScope,
            'searchTerm' => $term,
        ]);
    }

    private function loadResults(string $selectedScope, string $searchTerm): void
    {
        foreach (SearchScope::getKeys() as $scope) {
            /** @var HasMany $query */
            $query = $this->getQueryForScope($scope, $searchTerm);
            if ($scope === $selectedScope) {
                $this->searchResults = $query->paginate(50);
                $this->searchResultsCountPerScope[$scope] = $this->searchResults->total();
            } else {
                // Only count for other scopes.
                $this->searchResultsCountPerScope[$scope] = $query->count();
            }
        }
    }

    private function getQueryForScope(string $scope, string $term)
    {
        switch ($scope) {
            case SearchScope::COINS:
                return Coin::query()
                    ->search($term); /** @see Coin::scopeSearch() */
            case SearchScope::COIN_SERIES:
                return CoinSeries::query()
                    ->search($term); /** @see CoinSeries::scopeSearch() */
            case SearchScope::COUNTRIES:
                return Country::query()
                    ->search($term); /** @see Country::scopeSearch() */
            case SearchScope::DESIGNERS:
                return Designer::query()
                    ->search($term); /** @see Designer::scopeSearch() */
            case SearchScope::MINTS:
                return Mint::query()
                    ->search($term); /** @see Mint::scopeSearch() */
        }

        return null;
    }

    private function getTotal()
    {
        return isset($this->searchResultsCountPerScope)
            ? array_sum($this->searchResultsCountPerScope)
            : 0;
    }
}
