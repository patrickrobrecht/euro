<?php

namespace App\Http\Controllers;

use App\Http\Requests\DesignerRequest;
use App\Models\Country;
use App\Models\Designer;
use Illuminate\Support\Facades\Session;

class DesignerController extends Controller
{
    public function __invoke()
    {
        if (request()->query(__('page')) === '1') {
            return redirect()->route('designers');
        }

        $designers = Designer::allSortedByName()
            ->with('country', 'country.translations');

        return view('designers.designers_index', [
            'designers' => $designers->paginate(100, ['*'], __('page'))
        ]);
    }

    public function index()
    {
        $this->authorize('viewAny', Designer::class);

        $designers = Designer::allSortedByName()
            ->with('country', 'country.translations');

        return view('designers.designers_admin_index', [
            'designers' => $designers->paginate(),
        ]);
    }

    public function create()
    {
        $this->authorize('create', Designer::class);

        return view('designers.designer_form', [
            'allCountries' => Country::all(),
        ]);
    }

    public function store(DesignerRequest $request)
    {
        $this->authorize('create', Designer::class);

        $designer = new Designer();
        if ($this->saveDesigner($designer, $request->validated())) {
            Session::flash('success', __('Created successfully.'));
            return redirect()->route('designer.edit', $designer->id);
        }

        return back();
    }

    private function saveDesigner(Designer $designer, array $data): bool
    {
        $designer->fill($data);
        if ($data['home_country_id']) {
            $designer->country()->associate($data['home_country_id']);
        } else {
            $designer->country()->disassociate();
        }

        return $designer->save();
    }

    public function edit(Designer $designer)
    {
        $this->authorize('update', $designer);

        return view('designers.designer_form', [
            'allCountries' => Country::all(),
            'designer' => $designer,
        ]);
    }

    public function update(DesignerRequest $request, Designer $designer)
    {
        $this->authorize('update', $designer);

        if ($designer && $this->saveDesigner($designer, $request->validated())) {
            Session::flash('success', __('Saved successfully.'));
        }

        return back();
    }

    public function show(Designer $designer)
    {
        return view('designers.designer', [
            'designer' => $designer,
        ]);
    }
}
