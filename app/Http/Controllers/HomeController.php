<?php

namespace App\Http\Controllers;

use App\Models\Country;

class HomeController extends Controller
{
    public function __invoke()
    {
        return view('pages.home', [
            'euMemberCount' => Country::euMemberCount()
        ]);
    }

    public function redirect()
    {
        return redirect()
            ->route(config('app.fallback_locale') . '.home')
            ->setStatusCode(302);
    }
}
