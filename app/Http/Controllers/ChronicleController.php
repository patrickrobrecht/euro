<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChronicleEntryRequest;
use App\Models\ChronicleEntry;
use Illuminate\Support\Facades\Session;

class ChronicleController extends Controller
{
    public function __invoke()
    {
        return view('chronicle.chronicle', [
            'entries' => ChronicleEntry::query()
                ->orderedChronologically() /** @see ChronicleEntry::scopeOrderedChronologically() */
                ->get(),
        ]);
    }

    public function index()
    {
        $this->authorize('viewAny', ChronicleEntry::class);

        $chronicleEntries = ChronicleEntry::query()
            ->orderedChronologically(); /** @see ChronicleEntry::scopeOrderedChronologically() */

        return view('chronicle.chronicle_entries_admin_index', [
            'chronicleEntries' => $chronicleEntries->paginate(),
        ]);
    }

    public function create()
    {
        $this->authorize('create', ChronicleEntry::class);

        return view('chronicle.chronicle_entry_form');
    }

    public function store(ChronicleEntryRequest $request)
    {
        $this->authorize('create', ChronicleEntry::class);

        $chronicleEntry = new ChronicleEntry();
        if ($this->saveChronicleEntry($chronicleEntry, $request->validated())) {
            Session::flash('success', __('Created successfully.'));
            return redirect()->route('chronicle_entry.edit', $chronicleEntry->id);
        }

        return back();
    }

    private function saveChronicleEntry(ChronicleEntry $chronicleEntry, array $data): bool
    {
        return $chronicleEntry->fill($data)->save();
    }

    public function edit(ChronicleEntry $chronicleEntry)
    {
        $this->authorize('update', $chronicleEntry);

        return view('chronicle.chronicle_entry_form', [
            'chronicleEntry' => $chronicleEntry,
        ]);
    }

    public function update(ChronicleEntryRequest $request, ChronicleEntry $chronicleEntry)
    {
        $this->authorize('update', $chronicleEntry);

        if ($chronicleEntry && $this->saveChronicleEntry($chronicleEntry, $request->validated())) {
            Session::flash('success', __('Saved successfully.'));
        }

        return back();
    }
}
