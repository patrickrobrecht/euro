<?php

namespace App\Http\Controllers;

use App\Http\Requests\CoinRequest;
use App\Models\CoinSeries;
use App\Models\Country;
use Illuminate\Support\Facades\Session;

class CoinSeriesController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', CoinSeries::class);

        $coinSeries = CoinSeries::query()
            ->with('country');

        return view('coin_series.coin_series_admin_index', [
            'coinSeries' => $coinSeries->paginate(),
        ]);
    }

    public function create()
    {
        $this->authorize('create', CoinSeries::class);

        return view('coin_series.coin_series_form');
    }

    public function store(CoinRequest $request)
    {
        $this->authorize('create', CoinSeries::class);

        $coinSeries = new CoinSeries();
        if ($this->saveCoinSeries($coinSeries, $request->validated())) {
            Session::flash('success', __('Created successfully.'));
            return redirect()->route('coin_series.edit', $coinSeries->id);
        }

        return back();
    }

    private function saveCoinSeries(CoinSeries $coinSeries, array $data): bool
    {
        return $coinSeries->fill($data)->save();
    }

    public function edit(CoinSeries $coinSeries)
    {
        $this->authorize('update', $coinSeries);

        return view('coin_series.coin_series_form', [
            'coinSeries' => $coinSeries,
        ]);
    }

    public function update(CoinRequest $request, CoinSeries $coinSeries)
    {
        $this->authorize('update', $coinSeries);

        if ($coinSeries && $this->saveCoinSeries($coinSeries, $request->validated())) {
            Session::flash('success', __('Saved successfully.'));
        }

        return back();
    }
}
