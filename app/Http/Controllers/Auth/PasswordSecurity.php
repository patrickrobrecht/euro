<?php

namespace App\Http\Controllers\Auth;

class PasswordSecurity
{
    public static function rule(): array
    {
        return [
            'required',
            'string',
            'min:10',
            'confirmed'
        ];
    }
}
