<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Where to redirect users  after resetting their password.
     *
     * @return string
     */
    public function redirectTo(): string
    {
        return route('home');
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules(): array
    {
        return [
            'token' => 'required',
            'email' => [
                'required',
                'email',
            ],
            'password' => PasswordSecurity::rule(),
        ];
    }
}
