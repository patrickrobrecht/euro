<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Where to redirect users after login.
     *
     * @return string
     */
    public function redirectTo(): string
    {
        return route('home');
    }

    /**
     * Where to redirect users after logout.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function loggedOut(Request $request)
    {
        return redirect(route('login'));
    }
}
