<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Country;

class PageController extends Controller
{
    public function circulationCoins()
    {
        return view('pages.circulation-coins', [
            'countriesForSeries' => Country::query()
                ->orderByTranslation('name') /** @see Translatable::scopeOrderByTranslation() */
                ->with('circulationCoinSeries')
                ->withCount('circulationCoins')
                ->withEuro() /** @see Country::scopeWithEuro() */
                ->get(),
        ]);
    }

    public function commemorativeCoins()
    {
        return view('pages.2-euro-commemorative-coins');
    }

    public function downloads()
    {
        return view('pages.downloads');
    }

    public function legalNotice()
    {
        return view('pages.default-page', [
            'page' => Block::findByCode('legal-notice'),
        ]);
    }

    public function privacy()
    {
        return view('pages.default-page', [
            'page' => Block::findByCode('privacy'),
        ]);
    }
}
