<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', User::class);

        $users = User::query();

        return view('users.users_admin_index', [
            'users' => $users->paginate(),
        ]);
    }

    public function create()
    {
        $this->authorize('create', User::class);

        return view('users.user_form');
    }

    public function store(UserRequest $request)
    {
        $this->authorize('create', User::class);

        $user = new User();
        if ($this->saveUser($user, $request->validated())) {
            Session::flash('success', __('Created successfully.'));
            return redirect()->route('user.edit', $user->id);
        }

        return back();
    }

    private function saveUser(User $user, array $data): bool
    {
        $user->fill($data);
        if (isset($data['password'])) {
            $user->password = Hash::make($data['password']);
        }
        return $user->save();
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);

        return view('users.user_form', [
            'user' => $user,
        ]);
    }

    public function update(UserRequest $request, User $user)
    {
        $this->authorize('update', $user);

        if ($user && $this->saveUser($user, $request->validated())) {
            Session::flash('success', __('Saved successfully.'));
        }

        return back();
    }
}
