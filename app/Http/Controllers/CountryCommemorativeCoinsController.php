<?php

namespace App\Http\Controllers;

use App\Models\Country;

class CountryCommemorativeCoinsController extends Controller
{
    public function __invoke(Country $country)
    {
        return view('countries.2-euro-commemorative-coins', [
            'country' => $country->load(
                'commemorativeCoins',
                'commemorativeCoins.details.mintDetails.mint',
                'commemorativeCoins.details.mintDetails.mintMark',
                'commemorativeCoins.details.mintDetails.mintMaster',
                'commemorativeCoins.details.mintDetails.mintMasterMark',
                'commemorativeCoins.designerDetails.designer',
                'commemorativeCoins.designerDetails.mark',
                'commemorativeCoinSeries',
            ),
        ]);
    }
}
