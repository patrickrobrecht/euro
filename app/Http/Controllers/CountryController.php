<?php

namespace App\Http\Controllers;

use App\Http\Requests\CountryRequest;
use App\Models\Country;
use Illuminate\Support\Facades\Session;

class CountryController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Country::class);

        $countries = Country::query();

        return view('countries.countries_admin_index', [
            'allCountries' => $countries->paginate(),
        ]);
    }

    public function create()
    {
        $this->authorize('create', Country::class);

        return view('countries.country_form');
    }

    public function store(CountryRequest $request)
    {
        $this->authorize('create', Country::class);

        $country = new Country();
        if ($this->saveCountry($country, $request->validated())) {
            Session::flash('success', __('Created successfully.'));
            return redirect()->route('country.edit', $country->id);
        }

        return back();
    }

    private function saveCountry(Country $country, array $data): bool
    {
        $country->fill($data);
        return $country->save();
    }

    public function edit(Country $country)
    {
        $this->authorize('update', $country);

        return view('countries.country_form', [
            'country' => $country,
        ]);
    }

    public function update(CountryRequest $request, Country $country)
    {
        $this->authorize('update', $country);

        if ($country && $this->saveCountry($country, $request->validated())) {
            Session::flash('success', __('Saved successfully.'));
        }

        return back();
    }

    public function show(Country $country)
    {
        return view('countries.country', [
            'country' => $country->load(
                'circulationCoinSeries',
                'commemorativeCoinSeries',
                'mints',
            )->loadCount(
                'circulationCoins',
                'commemorativeCoins',
            ),
        ]);
    }
}
