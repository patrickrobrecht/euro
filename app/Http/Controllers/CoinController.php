<?php

namespace App\Http\Controllers;

use App\Http\Requests\CoinRequest;
use App\Models\Coin;
use App\Models\CoinSeries;
use App\Models\Country;
use Illuminate\Support\Facades\Session;

class CoinController extends Controller
{
    public function index()
    {
        $this->authorize('viewAny', Coin::class);

        $coins = Coin::query()
            ->with('country', 'series');

        return view('coins.coins_admin_index', [
            'coins' => $coins->paginate(),
        ]);
    }

    public function create()
    {
        $this->authorize('create', Coin::class);

        return view('coins.coin_form', [
            'allCoinSeries' => CoinSeries::all(),
        ]);
    }

    public function store(CoinRequest $request)
    {
        $this->authorize('create', Coin::class);

        $coin = new Coin();
        if ($this->saveCoin($coin, $request->validated())) {
            Session::flash('success', __('Created successfully.'));
            return redirect()->route('chronicle_entry.edit', $coin->id);
        }

        return back();
    }

    private function saveCoin(Coin $coin, array $data): bool
    {
        return $coin->fill($data)->save();
    }

    public function edit(Coin $coin)
    {
        $this->authorize('update', $coin);

        return view('coins.coin_form', [
            'allCoinSeries' => CoinSeries::all(),
            'coin' => $coin,
        ]);
    }

    public function update(CoinRequest $request, Coin $coin)
    {
        $this->authorize('update', $coin);

        if ($coin && $this->saveCoin($coin, $request->validated())) {
            Session::flash('success', __('Saved successfully.'));
        }

        return back();
    }
}
