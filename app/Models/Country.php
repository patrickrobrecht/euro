<?php

namespace App\Models;

use App\Models\Traits\HasSluggableTranslation;
use App\Models\Traits\SearchableTranslations;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Collection;

/**
 * App\Models\Country
 *
 * @property-read integer $id
 *
 * @property string $code
 * @property int|null $eu_member_since
 * @property int|null $euro_since
 * @property string $image
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * via {@see CountryTranslation}
 * @property string $slug
 * @property string $name
 * @property string $full_name
 *
 * @property-read Collection|Coin[] $coins {@see Country::coins())}
 * @property-read int|null $coins_count
 * @property-read Collection|Coin[] $circulationCoins {@see Country::circulationCoinSeries())}
 * @property-read int|null $circulation_coins_count
 * @property-read Collection|Coin[] $commemorativeCoins {@see Country::commemorativeCoins())}
 * @property-read int|null $commemorative_coins_count
 * @property-read Collection|CoinSeries[] $circulationCoinSeries {@see Country::circulationCoinSeries()}
 * @property-read Collection|CoinSeries[] $commemorativeCoinSeries {@see Country::commemorativeCoinSeries()}
 * @property-read Collection|Mint[] $mints {@see Country::mints()}
 * @property-read int|null $mints_count
 * @property-read Collection|CoinSeries[] $series {@see Country::series()}
 * @property-read int|null $series_count
 */
class Country extends TranslatableModel
{
    use HasSluggableTranslation;
    use SearchableTranslations;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    public $translationModel = CountryTranslation::class;

    public $translatedAttributes = [
        'slug',
        'name',
        'full_name',
    ];

    protected $fillable = [
        'code',
        'euro_member_since',
        'euro_since',
        'image',
        'slug',
        'name',
        'full_name',
    ];

    public static function findByCode($code)
    {
        return self::query()
            ->where('code', $code)
            ->first();
    }

    public static function withEuro()
    {
        return self::query()
            ->orderByTranslation('name') /** @see Translatable::scopeOrderByTranslation() */
            ->withCount(
                'coins',
                'circulationCoins',
                'commemorativeCoins',
            )
            ->withEuro() /** @see Country::scopeWithEuro() */
            ->get();
    }

    public static function euMemberCount(): int
    {
        return self::query()
            ->whereNotNull('eu_member_since')
            ->count();
    }

    public function coins(): HasMany
    {
        return $this->hasMany(Coin::class, 'country_id');
    }

    public function circulationCoins(): HasMany
    {
        return $this->coins()
            ->where('type', 'CIRCULATION');
    }

    public function commemorativeCoins(): HasMany
    {
        return $this->coins()
            ->where('type', 'COMMEMORATIVE');
    }

    public function mints(): HasMany
    {
        return $this->hasMany(Mint::class, 'country_id');
    }

    public function series(): HasMany
    {
        return $this->hasMany(CoinSeries::class, 'country_id');
    }

    public function circulationCoinSeries(): HasMany
    {
        return $this->series()
            ->where('type', 'CIRCULATION');
    }

    public function commemorativeCoinSeries(): HasMany
    {
        return $this->series()
            ->where('type', 'COMMEMORATIVE');
    }

    public function scopeWithEuro(Builder $query)
    {
        return $query->whereNotNull('euro_since');
    }

    public function scopeSearch(Builder $query, string $term): Builder
    {
        return $this->scopeSearchTranslations($query, $term, ['name', 'full_name'])
            ->orWhere('code', $term);
    }
}
