<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait HasSluggableTranslation
{
    /**
     * Finds a model by the slug in the translations table.
     *
     * @param string $slug the slug
     * @param string $locale the locale
     * @return Builder|Model|object|null the model if such an object exists
     */
    public static function findBySlug(string $slug, string $locale)
    {
        return self::whereTranslation('slug', $slug, $locale)->first();
    }
}
