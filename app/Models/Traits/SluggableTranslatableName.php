<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait SluggableTranslatableName
{
    use SluggableName;

    /**
     * Defines that slugs must be only unique within their locale.
     *
     * @param Builder $query
     * @param Model $model the model
     * @param $attribute
     * @param $config
     * @param string $slug the slug
     * @return Builder
     */
    public function scopeWithUniqueSlugConstraints(Builder $query, Model $model, $attribute, $config, $slug): Builder
    {
        return $query->where('locale', $model->locale);
    }
}
