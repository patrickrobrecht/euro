<?php

namespace App\Models\Traits;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

trait SluggableName
{
    use Sluggable;

    /**
     * Finds a model by the slug.
     *
     * @param string $slug the slug
     * @return Builder|Model|object|null
     */
    public static function findBySlug(string $slug)
    {
        return self::query()
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
