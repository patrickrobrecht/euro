<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Searchable
{
    abstract public function scopeSearch(Builder $query, string $term): Builder;
}
