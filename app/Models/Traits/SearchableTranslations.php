<?php

namespace App\Models\Traits;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use InvalidArgumentException;

trait SearchableTranslations
{
    use Searchable;
    use Translatable;

    public function translateAttribute(string $attribute, ?string $locale = null): string
    {
        return $this->hasTranslation($locale) ? $this->translate($locale)->$attribute : '';
    }

    public function scopeSearchTranslation(Builder $query, string $term, string $field): Builder
    {
        return $this->scopeWhereTranslationLike($query, $field, '%' . $term . '%');
    }

    public function scopeSearchTranslations(Builder $query, string $term, array $fields): Builder
    {
        if (count($fields) === 0) {
            throw new InvalidArgumentException('Fields must not be empty');
        }

        $newQuery = $this->scopeSearchTranslation($query, $term, $fields[0]);
        foreach (array_splice($fields, 1) as $field) {
            $newQuery->orWhere(function (Builder $query) use ($term, $field) {
                return $this->scopeSearchTranslation($query, $term, $field);
            });
        }
        return $newQuery;
    }
}
