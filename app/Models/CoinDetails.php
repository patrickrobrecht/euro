<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\CoinDetails
 *
 * @property-read int $id
 * @property int $year
 * @property int|null $mintage
 * @property int|null $set_only
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read string $formatted_mintage {@see CoinDetails::getFormattedMintageAttribute()}
 *
 * @property-read Coin $coin {@see CoinDetails::coin()}
 * @property-read MintDetails $mintDetails {@see CoinDetails::mintDetails()}
 * @property-read Mint $mint {@see CoinDetails::mint()}
 */
class CoinDetails extends AbstractModel
{
    public function getFormattedMintageAttribute(): string
    {
        if ($this->mintage) {
            return number_format($this->mintage, 0, '.', __('thousand_separator'));
        }

        return __('unknown');
    }

    public function coin(): BelongsTo
    {
        return $this->belongsTo(Coin::class, 'coin_id');
    }

    public function mint(): Mint
    {
        return $this->mintDetails->mint;
    }

    public function mintDetails(): BelongsTo
    {
        return $this->belongsTo(MintDetails::class, 'mint_details_id');
    }
}
