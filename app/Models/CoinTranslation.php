<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CoinTranslation extends AbstractModel
{
    protected $fillable = [
        'name',
        'description',
    ];

    public function coin(): BelongsTo
    {
        return $this->belongsTo(Coin::class, 'coin_id', 'id');
    }
}
