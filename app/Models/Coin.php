<?php

namespace App\Models;

use App\Models\Traits\SearchableTranslations;
use App\Options\CoinType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Coin
 *
 * @property-read int $id
 * @property string $type
 * @property float $denomination
 * @property string|null $issue_date
 * @property string|null $image
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * via {@see CoinTranslation}
 * @property string $name
 * @property string $description
 *
 * @property-read string $formatted_issue_date {@see Coin::getFormattedIssueDateAttribute()}
 * @property-read int[] $years {@see Coin::getYearsAttribute()}
 * @property-read mixed $first_year {@see Coin::getFirstYearAttribute()}
 * @property-read string $formatted_denomination {@see Coin::getFormattedDenominationAttribute()}
 * @property-read int $denomination_cents {@see Coin::getDenominationCentsAttribute()}
 *
 * @property-read Country $country {@see Coin::country()}
 * @property-read Collection|CoinDesigner[] $designerDetails {@see Coin::designerDetails()}
 * @property-read int|null $designer_details_count
 * @property-read CoinSeries|null $series {@see Coin::series()}
 */
class Coin extends TranslatableModel
{
    use SearchableTranslations;

    public $translationModel = CoinTranslation::class;

    public $translatedAttributes = [
        'name',
        'description'
    ];

    protected $fillable = [
        'name',
        'description',
    ];

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function designerDetails(): HasMany
    {
        return $this->hasMany(CoinDesigner::class, 'coin_id');
    }

    public function details(): HasMany
    {
        return $this->hasMany(CoinDetails::class, 'coin_id');
    }

    public function series(): BelongsTo
    {
        return $this->belongsTo(CoinSeries::class, 'coin_series_id');
    }

    public function getFormattedIssueDateAttribute(): string
    {
        if ($this->issue_date) {
            return date_format(date_create($this->issue_date), __('date_format'));
        }

        return __('unknown issue date');
    }

    public function getYearsAttribute(): array
    {
        $years = [];
        foreach ($this->details as $detail) {
            $years[] = $detail->year;
        }
        return array_unique($years);
    }

    public function getFirstYearAttribute(): ?int
    {
        $years = $this->years;
        if (empty($years)) {
            return $this->issue_date ? date_format(date_create($this->issue_date), 'Y') : null;
        }
        return min($years);
    }

    public function getFormattedDenominationAttribute(): string
    {
        return $this->denomination < 1
            ? ($this->denomination * 100) . ' ' . __('cent')
            : number_format($this->denomination, 0) . ' ' . __('euro');
    }

    public function getDenominationCentsAttribute(): int
    {
        return (int) ($this->denomination * 100);
    }

    public function getURL(): string
    {
        if ($this->type === CoinType::CIRCULATION) {
            return route('circulation-coin-series', [
                'country' => $this->country->slug,
                'circulationCoinSeries' => $this->series->slug,
            ]);
        }

        if ($this->type === CoinType::COMMEMORATIVE) {
            return route('2-euro-commemorative-coins', $this->country->slug);
        }

        return route('country', $this->country->slug);
    }

    public function scopeSearch(Builder $query, string $term): Builder
    {
        return $this->scopeSearchTranslations($query, $term, $this->translatedAttributes);
    }
}
