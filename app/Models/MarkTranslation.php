<?php

namespace App\Models;

class MarkTranslation extends AbstractModel
{
    protected $fillable = [
        'name',
        'description',
    ];
}
