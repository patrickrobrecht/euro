<?php

namespace App\Models;

use App\Models\Traits\SluggableTranslatableName;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CountryTranslation extends AbstractModel
{
    use SluggableTranslatableName;

    protected $fillable = [
        'slug',
        'name',
        'full_name',
        'description',
    ];

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }
}
