<?php

namespace App\Models;

use App\Models\Traits\SluggableTranslatableName;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CoinSeriesTranslation extends AbstractModel
{
    use SluggableTranslatableName;

    protected $fillable = [
        'slug',
        'name',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => [
                    'name',
                ]
            ]
        ];
    }

    public function series(): BelongsTo
    {
        return $this->belongsTo(CoinSeries::class, 'coin_series_id', 'id');
    }

    public function getCountryNameAttribute(): string
    {
        return CoinSeries::query()
            ->find($this->coin_series_id)
            ->country
            ->translate($this->locale)
            ->name;
    }
}
