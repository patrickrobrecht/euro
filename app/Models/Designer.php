<?php

namespace App\Models;

use App\Models\Traits\Searchable;
use App\Models\Traits\SluggableName;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * App\Models\Designer
 *
 * @property-read int $id
 * @property string|null $first_name
 * @property string $last_name
 * @property string $sex
 * @property string|null $birthday
 * @property string|null $day_of_death
 * @property int|null $home_country
 * @property string|null $website
 * @property string $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read string full_name {@see Designer::getFullNameAttribute()}
 * @property-read string $name_with_comma {@see Designer::getNameWithCommaAttribute()}
 *
 * @property-read Country|null $country {@see Designer::country()}
 * @property-read Collection|CoinDesigner[] $designs {@see Designer::designs()}
 * @property-read int|null $designs_count
 */
class Designer extends AbstractModel
{
    use Searchable;
    use SluggableName;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'sex',
        'birthday',
        'day_of_death',
        'website',
    ];

    public function getFullNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getNameWithCommaAttribute(): string
    {
        return $this->last_name . ($this->first_name ? (', ' . $this->first_name) : '');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'home_country', 'id');
    }

    public function designs(): HasMany
    {
        return $this->hasMany(CoinDesigner::class, 'designer_id');
    }

    public static function findByName(string $name)
    {
        return self::query()
            ->where(DB::raw('concat(first_name," ",last_name)'), $name)
            ->orWhere('last_name', $name)
            ->first();
    }

    public static function allSortedByName()
    {
        return self::query()->orderBy('last_name');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => [
                    'first_name',
                    'last_name',
                ]
            ]
        ];
    }

    public function scopeSearch(Builder $query, string $term): Builder
    {
        return $query->where('first_name', 'LIKE', '%' . $term . '%')
            ->orWhere('last_name', 'LIKE', '%' . $term . '%');
    }
}
