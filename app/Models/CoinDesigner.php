<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * App\Models\CoinDesigner
 *
 * @property-read int $id
 * @property string $role
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read Coin $coin {@see CoinDesigner::coin()}
 * @property-read Designer $designer {@see CoinDesigner::designer()}
 * @property-read Mark $mark {@see CoinDesigner::mark()}
 */
class CoinDesigner extends AbstractModel
{
    public function coin(): HasOne
    {
        return $this->hasOne(Coin::class, 'id', 'coin_id');
    }

    public function designer(): HasOne
    {
        return $this->hasOne(Designer::class, 'id', 'designer_id');
    }

    public function mark(): HasOne
    {
        return $this->hasOne(Mark::class, 'id', 'designer_mark_id');
    }
}
