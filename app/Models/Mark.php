<?php

namespace App\Models;

use Eloquent;
use Illuminate\Support\Carbon;

/**
 * App\Models\Mark
 *
 * @property-read int $id
 * @property string $code
 * @property string $type
 * @property string $text
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * via {@see MarkTranslation}
 * @property string|null $name
 * @property string|null $description
 */
class Mark extends TranslatableModel
{
    public $translatedAttributes = [
        'name',
        'description',
    ];

    public static function findByCode(string $code)
    {
        return self::query()
            ->where('code', $code)
            ->first();
    }
}
