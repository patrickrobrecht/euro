<?php

namespace App\Models;

class BlockTranslation extends AbstractModel
{
    protected $fillable = [
        'title',
        'contents',
    ];
}
