<?php

namespace App\Models;

class ChronicleEntryTranslation extends AbstractModel
{
    protected $fillable = [
        'title',
        'text',
    ];
}
