<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

abstract class TranslatableModel extends AbstractModel implements TranslatableContract
{
    use Translatable;

    protected $with = ['translations'];
}
