<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 * App\Models\ChronicleEntry
 *
 * @property-read int $id
 * @property ?string $date
 * @property int $year
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * via {@see ChronicleEntryTranslation}
 * @property ?string $title
 * @property string $text
 *
 * @property-read string $formatted_date {@see ChronicleEntry::getFormattedDateAttribute()}
 */
class ChronicleEntry extends TranslatableModel
{
    public $translationModel = ChronicleEntryTranslation::class;

    public $translatedAttributes = [
        'title',
        'text',
    ];

    protected $fillable = [
        'date',
        'year',
        'title',
        'text',
    ];

    public function getFormattedDateAttribute()
    {
        if ($this->date) {
            return date_format(date_create($this->date), __('date_format'));
        }

        return $this->year;
    }

    public function scopeOrderedChronologically(Builder $query): Builder
    {
        return $query->orderBy('year')
            ->orderBy('date');
    }
}
