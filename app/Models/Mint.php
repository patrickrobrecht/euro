<?php

namespace App\Models;

use App\Models\Traits\HasSluggableTranslation;
use App\Models\Traits\SearchableTranslations;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * App\Models\Mint
 *
 * @property-read int $id
 * @property string $local_name
 * @property string $website
 * @property string $slug
 * @property string $name
 *
 * @property-read Country $country {@see Mint::country()}
 * @property-read Collection|MintDetails[] $mintDetails {@see Mint::mintDetails()}
 * @property-read Collection|MintMaster[] $mintMasters {@see Mint::mintMasters()}
 */
class Mint extends TranslatableModel
{
    use HasSluggableTranslation;
    use SearchableTranslations;

    public $translationModel = MintTranslation::class;

    public $translatedAttributes = [
        'slug',
        'name',
    ];

    protected $fillable = [
        'local_name',
        'website',
        'name',
    ];

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function mintDetails(): HasMany
    {
        return $this->hasMany(MintDetails::class, 'mint_id');
    }

    public function mintMarks(): HasManyThrough
    {
        return $this->hasManyThrough(
            Mark::class,
            MintDetails::class,
            'mint_id',
            'id',
            'id',
            'mint_mark_id'
        )->distinct();
    }

    public function mintMasters(): HasMany
    {
        return $this->hasMany(MintMaster::class, 'works_for');
    }

    public static function findByEnglishName(string $name)
    {
        return self::query()
            ->whereHas('translations', static function (Builder $query) use ($name) {
                $query->where('name', $name);
            })->first();
    }

    public function scopeSearch(Builder $query, string $term): Builder
    {
        return $this->scopeSearchTranslation($query, $term, 'name')
            ->orWhere('local_name', 'LIKE', '%' . $term . '%');
    }
}
