<?php

namespace App\Models;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

class AbstractModel extends Model
{
    use Cachable;

    private const PAGE_SIZE_LIMIT = 100;

    public function getPerPage(): int
    {
        $pageSize = (int)request('page_size', $this->perPage);

        if ($pageSize) {
            return min($pageSize, self::PAGE_SIZE_LIMIT);
        }

        return parent::getPerPage();
    }
}
