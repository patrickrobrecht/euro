<?php

namespace App\Models;

use App\Models\Traits\HasSluggableTranslation;
use App\Models\Traits\SearchableTranslations;
use App\Options\CoinType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\CoinSeries
 *
 * @property-read int $id
 * @property string $type
 * @property string|null $first_year
 * @property string|null $last_year
 * @property string $code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * via {@see CoinSeriesTranslation}
 * @property string $name
 * @property string $slug
 *
 * @property-read string $period {@see CoinSeries::getPeriodAttribute()}
 * @property-read string $country_name {@see CoinSeriesTranslation::getCountryNameAttribute()}
 *
 * @property-read Collection|Coin[] $coins {@see CoinSeries::coins()}
 * @property-read int|null $coins_count
 * @property-read Country $country {@see CoinSeries::country()}
 */
class CoinSeries extends TranslatableModel
{
    use HasSluggableTranslation;
    use SearchableTranslations;

    public $translationModel = CoinSeriesTranslation::class;

    public $translatedAttributes = [
        'slug',
        'name',
    ];

    protected $fillable = [
        'slug',
        'name',
    ];

    public function coins(): HasMany
    {
        return $this->hasMany(Coin::class, 'coin_series_id');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function getPeriodAttribute(): string
    {
        if ($this->first_year) {
            if ($this->last_year) {
                if ($this->first_year === $this->last_year) {
                    return $this->first_year;
                }

                return __(':firstYear until :lastYear', [
                    'firstYear' => $this->first_year,
                    'lastYear' => $this->last_year
                ]);
            }

            return __('since :firstYear', ['firstYear' => $this->first_year]);
        }

        return '';
    }

    public function getURL(): string
    {
        if ($this->type === CoinType::CIRCULATION) {
            return route('circulation-coin-series', [
                'country' => $this->country->slug,
                'circulationCoinSeries' => $this->slug,
            ]);
        }

        if ($this->type === CoinType::COMMEMORATIVE) {
            return route('2-euro-commemorative-coins', $this->country->slug);
        }

        return route('country', $this->country->slug);
    }

    /**
     * @param string $code the code
     * @return mixed
     */
    public static function findByCode(string $code)
    {
        return self::query()
            ->where('code', $code)
            ->first();
    }

    public function scopeSearch(Builder $query, string $term): Builder
    {
        return $this->scopeSearchTranslation($query, $term, 'name');
    }
}
