<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\MintDetails
 *
 * @property int $id
 * @property int|null $mint_id
 * @property int|null $mint_mark_id
 * @property int|null $mint_master_id
 * @property int|null $mint_master_mark_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read Mint $mint {@see MintDetails::mint()}
 * @property-read Mark $mintMark {@see MintDetails::mintMark()}
 * @property-read MintMaster $mintMaster{@see MintDetails::mintMaster()}
 * @property-read Mark $mintMasterMark {@see MintDetails::mintMasterMark()}
 */
class MintDetails extends AbstractModel
{
    public function mint(): BelongsTo
    {
        return $this->belongsTo(Mint::class, 'mint_id');
    }

    public function mintMark(): BelongsTo
    {
        return $this->belongsTo(Mark::class, 'mint_mark_id');
    }

    public function mintMaster(): BelongsTo
    {
        return $this->belongsTo(MintMaster::class, 'mint_master_id');
    }

    public function mintMasterMark(): BelongsTo
    {
        return $this->belongsTo(Mark::class, 'mint_master_mark_id');
    }

    public static function findOrCreate($mint, $mintMark, $mintMaster, $mintMasterMark)
    {
        return self::query()->firstOrCreate([
            'mint_id' => isset($mint) ? $mint->id : null,
            'mint_mark_id' => isset($mintMark) ? $mintMark->id : null,
            'mint_master_id' => isset($mintMaster) ? $mintMaster->id : null,
            'mint_master_mark_id' => isset($mintMasterMark) ? $mintMasterMark->id : null
        ]);
    }
}
