<?php

namespace App\Models;

use App\Models\Traits\SluggableTranslatableName;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MintTranslation extends AbstractModel
{
    use SluggableTranslatableName;

    protected $fillable = [
        'slug',
        'name',
        'full_name',
        'description',
    ];

    public function mint(): BelongsTo
    {
        return $this->belongsTo(Mint::class, 'mint_id', 'id');
    }

    public static function findSlug($locale, $slug)
    {
        return self::query()
            ->where('locale', $locale)
            ->where('slug', $slug)
            ->first();
    }
}
