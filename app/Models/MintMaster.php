<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Carbon;

/**
 * App\Models\MintMaster
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $sex
 * @property string|null $term_start
 * @property string|null $term_end
 * @property int $works_for
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @property-read string $title {@see MintMaster::getTitleAttribute()}
 *
 * @property-read Collection|Mark[] $marks {@see MintMaster::marks()}
 */
class MintMaster extends AbstractModel
{
    public function marks(): HasManyThrough
    {
        return $this->hasManyThrough(
            Mark::class,
            MintDetails::class,
            'mint_master_id',
            'id',
            'id',
            'mint_master_mark_id'
        )->distinct();
    }

    public function getTitleAttribute(): string
    {
        $title = $this->sex === 'FEMALE' ? __('mint master female') : __('mint master');

        if ($this->term_start) {
            if ($this->term_end) {
                if ($this->term_start === $this->term_end) {
                    return __(':title in :start', ['title' => $title, 'start' => $this->term_start]);
                }

                return __(':title from :start until :end', [
                    'title' => $title,
                    'start' => $this->term_start,
                    'end' => $this->term_end
                ]);
            }

            return __(':title since :start', ['title' => $title, 'start' => $this->term_start]);
        }

        if ($this->term_end) {
            return __(':title until :end', ['title' => $title, 'end' => $this->term_end]);
        }

        return $title;
    }

    public static function findByLastName(string $lastName)
    {
        return self::query()
            ->where('last_name', $lastName)
            ->first();
    }
}
