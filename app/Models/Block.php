<?php

namespace App\Models;

use Illuminate\Support\Carbon;

/**
 * App\Models\Block
 *
 * @property-read int $id
 * @property string $code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * via {@see BlockTranslation}
 * @property string $title
 * @property string $contents
 */
class Block extends TranslatableModel
{
    public $translatedAttributes = [
        'title',
        'contents',
    ];

    public static function findByCode($code)
    {
        return self::query()
            ->where('code', $code)
            ->first();
    }
}
