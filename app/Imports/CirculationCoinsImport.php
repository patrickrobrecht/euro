<?php

namespace App\Imports;

use App\Models\Coin;
use App\Models\CoinSeries;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CirculationCoinsImport implements ToModel, WithHeadingRow
{
    private $coin = null;
    private $series = null;

    /**
     * @param array $row
     *
     * @return Model|null
     */
    public function model(array $row)
    {
        if ($row['series']) {
            $this->series = CoinSeries::findByCode($row['series']);
            if (!$this->series) {
                echo sprintf("ERROR: Couldn't find series %s" . PHP_EOL, $row['series']);
                return null;
            }
        }

        if ($row['denomination']) {
            $this->coin = new Coin(
                ImportHelper::attributesAndTranslations(
                    [
                        'type' => 'CIRCULATION',
                        'country_id' => $this->series->country_id,
                        'coin_series_id' => $this->series->id,
                        'denomination' => $row['denomination'],
                        'image' => $row['image'],
                    ],
                    $row,
                    ['name', 'description']
                )
            );
            $this->coin->save();
        }

        ImportHelper::addDesigner($this->coin, $row['designer_name'], $row['designer_mark'], 'DESIGNING');
        ImportHelper::addDesigner($this->coin, $row['engraver_name'], $row['engraver_mark'], 'ENGRAVING');

        return null;
    }
}
