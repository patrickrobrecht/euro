<?php

namespace App\Imports;

use App\Models\Country;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CountriesImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return Model|null
     */
    public function model(array $row)
    {
        return new Country(
            ImportHelper::attributesAndTranslations(
                [
                    'code' => $row['code'],
                    'euro_since' => $row['euro_since'],
                    'eu_member_since' => $row['eu_member_since'] ? $row['eu_member_since'] : null,
                    'image' => $row['image']
                ],
                $row,
                ['name', 'full_name']
            )
        );
    }
}
