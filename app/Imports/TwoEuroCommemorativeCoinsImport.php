<?php

namespace App\Imports;

use App\Models\Coin;
use App\Models\CoinDetails;
use App\Models\CoinSeries;
use App\Models\Country;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class TwoEuroCommemorativeCoinsImport implements ToModel, WithHeadingRow
{
    private $coin = null;
    private $country = null;
    private $year = null;

    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        if ($row['country']) {
            $country = Country::findByCode($row['country']);
            if (!$country) {
                echo sprintf("ERROR: Couldn't find country %s" . PHP_EOL, $row['country']);
                return null;
            }
            $this->country = $country;
        }

        $series = null;
        if ($row['series']) {
            $series = CoinSeries::findByCode($row['series']);
            if ($series) {
                $series = $series->id;
            } else {
                echo sprintf("ERROR: Couldn't find series %s" . PHP_EOL, $row['series']);
            }
        }

        if ($row['name_de'] || $row['name_en']) {
            $this->coin = new Coin(
                ImportHelper::attributesAndTranslations(
                    [
                        'type' => 'COMMEMORATIVE',
                        'country_id' => $this->country->id,
                        'coin_series_id' => $series,
                        'denomination' => 2,
                        'issue_date' => DataImport::convert($row['issue_date']),
                        'image' => $row['image'],
                    ],
                    $row,
                    ['name', 'description']
                )
            );
            $this->coin->save();

            $this->year = $row['year'] ? $row['year'] : substr($this->coin->issue_date, 0, 4);
        }

        // Save details.
        if ($row['mint']) {
            $mintDetails = ImportHelper::findOrCreateMintDetails($row);
            if (!$mintDetails) {
                return null;
            }

            $details = new CoinDetails([
                'coin_id' => $this->coin->id,
                'year' => $this->year,
                'mint_details_id' => $mintDetails->id,
                'mintage' => $row['mintage']
            ]);
            $details->save();
        }

        ImportHelper::addDesigner($this->coin, $row['designer_name'], $row['designer_mark'], 'DESIGNING');

        return null;
    }
}
