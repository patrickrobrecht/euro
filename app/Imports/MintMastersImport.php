<?php

namespace App\Imports;

use App\Models\Mint;
use App\Models\MintMaster;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MintMastersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        $mint = Mint::whereTranslation('name', $row['works_for'])->first();

        if (!$mint) {
            echo sprintf("ERROR: Couldn't find mint %s" . PHP_EOL, $row['works_for']);
            return null;
        }

        return new MintMaster([
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'sex' => $row['sex'],
            'term_start' => $row['term_start'],
            'term_end' => $row['term_end'],
            'works_for' => $mint->id
        ]);
    }
}
