<?php

namespace App\Imports;

use App\Models\Country;
use App\Models\CoinSeries;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class TwoEuroCommemorativeCoinSeriesImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return Model|null
     */
    public function model(array $row)
    {
        if (!$row['code']) {
            // Skip empty row.
            return null;
        }

        $country = Country::findByCode($row['country']);
        if (!$country) {
            echo sprintf("ERROR: Couldn't find country %s" . PHP_EOL, $row['country']);
            return null;
        }

        return new CoinSeries(
            ImportHelper::attributesAndTranslations(
                [
                    'country_id' => $country->id,
                    'type' => 'COMMEMORATIVE',
                    'first_year' => $row['first_year'],
                    'last_year' => $row['last_year'],
                    'code' => $row['code']
                ],
                $row,
                ['name']
            )
        );
    }
}
