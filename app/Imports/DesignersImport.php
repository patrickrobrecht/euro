<?php

namespace App\Imports;

use App\Models\Country;
use App\Models\Designer;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class DesignersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        $country = Country::findByCode($row['home_country']);

        return new Designer([
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'sex' => strtolower($row['sex']),
            'birthday' => DataImport::convert($row['birthday']),
            'day_of_death' => DataImport::convert($row['day_of_death']),
            'home_country' => $country ? $country->id : null,
            'website' => $row['website'],
        ]);
    }
}
