<?php

namespace App\Imports;

use App\Models\Mark;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MarksImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        if (!$row['type']) {
            // Skip empty row.
            return null;
        }

        return new Mark(
            ImportHelper::attributesAndTranslations(
                [
                    'code' => $row['code'],
                    'type' => $row['type'],
                    'text' => $row['text']
                ],
                $row,
                ['name', 'description']
            )
        );
    }
}
