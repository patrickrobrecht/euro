<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class DataImport implements WithMultipleSheets
{
    public function sheets(): array
    {
        return [
            'countries' => new CountriesImport(),
            'designers' => new DesignersImport(),
            'mints' => new MintsImport(),
            'mint-masters' => new MintMastersImport(),
            'marks' => new MarksImport(),
            'circulation-coin-series' => new CirculationCoinSeriesImport(),
            'circulation-coins' => new CirculationCoinsImport(),
            'circulation-coins-details' => new CirculationCoinDetailsImport(),
            '2-euro-commemorative-coin-series' => new TwoEuroCommemorativeCoinSeriesImport(),
            '2-euro-commemorative-coins' => new TwoEuroCommemorativeCoinsImport()
        ];
    }

    public static function convert($date)
    {
        if (!$date) {
            return null;
        }
        return $date;
    }
}
