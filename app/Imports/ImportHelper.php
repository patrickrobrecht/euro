<?php

namespace App\Imports;

use App\Models\Designer;
use App\Models\CoinDesigner;
use App\Models\Mark;
use App\Models\Mint;
use App\Models\MintDetails;
use App\Models\MintMaster;

class ImportHelper
{
    public static function attributesAndTranslations(array $attributes, array $row, array $fields): array
    {
        return array_merge(
            $attributes,
            self::extractTranslations($row, $fields)
        );
    }

    public static function extractTranslations(array $row, array $fields): array
    {
        $translations = [];
        foreach (array_keys(config('app.locales')) as $locale) {
            foreach ($fields as $field) {
                $key = $field . '_' . $locale;
                if (isset($row[$key])) {
                    $translations[$field . ':' . $locale] = $row[$key];
                }
            }
        }
        return $translations;
    }

    public static function addDesigner($coin, $name, $mark, $role)
    {
        if ($name && !in_array($name, ['TODO', 'unknown'])) {
            $designer = Designer::findByName($name);
            if (!$designer) {
                echo sprintf("ERROR: Couldn't find designer %s" . PHP_EOL, $name);
                return null;
            }

            $designerMark = null;
            if ($mark) {
                $designerMark = self::findOrCreateDesignerMark($mark, $name);
            }

            $details = new CoinDesigner([
                'coin_id' => $coin->id,
                'designer_id' => $designer->id,
                'designer_mark_id' => $designerMark ? $designerMark->id : null,
                'role' => $role
            ]);
            $details->save();
        }
    }

    public static function findOrCreateDesignerMark($text, $name)
    {
        $code = 'designer-mark-' . $text;
        $mark = Mark::findByCode($code);

        if ($mark === null) {
            $mark = new Mark(
                [
                    'code' => $code,
                    'type' => 'TEXT',
                    'text' => $text,
                    'description:de' => 'Künstlerzeichen von ' . $name,
                    'description:en' => 'designer mark of ' . $name,
                ]
            );
            $mark->save();
        }

        return $mark;
    }

    public static function findOrCreateMintDetails($row): ?MintDetails
    {
        $mint = Mint::findByEnglishName($row['mint']);
        if (!$mint) {
            echo sprintf("ERROR: Couldn't find mint %s" . PHP_EOL, $row['mint']);
        }

        if ($row['mint_mark']) {
            $mintMark = Mark::findByCode($row['mint_mark']);
            if (!$mintMark) {
                echo sprintf("ERROR: Couldn't find mint mark %s" . PHP_EOL, $row['mint_mark']);
                return null;
            }
        } else {
            $mintMark = null;
        }

        if ($row['mint_master']) {
            $mintMaster = MintMaster::findByLastName($row['mint_master']);
            if (!$mintMaster) {
                echo sprintf("ERROR: Couldn't find mint mark %s" . PHP_EOL, $row['mint_master']);
                return null;
            }
        } else {
            $mintMaster = null;
        }

        if ($row['mint_master_mark']) {
            $mintMasterMark = Mark::findByCode($row['mint_master_mark']);
            if (!$mintMasterMark) {
                echo sprintf("ERROR: Couldn't find mint master mark %s" . PHP_EOL, $row['mint_master_mark']);
                return null;
            }
        } else {
            $mintMasterMark = null;
        }

        // Find or create mint details.
        return MintDetails::findOrCreate(
            $mint,
            $mintMark,
            $mintMaster,
            $mintMasterMark
        );
    }
}
