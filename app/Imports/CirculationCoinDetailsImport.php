<?php

namespace App\Imports;

use App\Models\CoinDetails;
use App\Models\CoinSeries;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CirculationCoinDetailsImport implements ToModel, WithHeadingRow
{
    private $series = null;

    public function model(array $row)
    {
        if ($row['series']) {
            $this->series = CoinSeries::findByCode($row['series']);
            if (!$this->series) {
                echo sprintf("ERROR: Couldn't find series %s" . PHP_EOL, $row['series']);
                return null;
            }
        }

        if ($row['year']) {
            $mintDetails = ImportHelper::findOrCreateMintDetails($row);
            $mintageOfSet = $row['mintage_set'];
            foreach ($this->series->coins as $coin) {
                $mintage = $row['mintage_' . number_format($coin->denomination * 100, 0)];

                if (is_numeric($mintage) || $mintage === 'TODO') {
                    $details = new CoinDetails([
                        'coin_id' => $coin->id,
                        'year' => $row['year'],
                        'mint_details_id' => $mintDetails->id,
                        'mintage' => $mintage === 'TODO' ? null : $mintage,
                        'set_only' => ($mintageOfSet === $mintage)
                    ]);
                    $details->save();
                }
            }
        }

        return null;
    }
}
