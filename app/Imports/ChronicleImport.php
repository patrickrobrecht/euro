<?php

namespace App\Imports;

use App\Models\ChronicleEntry;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ChronicleImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        if (!$row['date']) {
            return null;
        }

        return new ChronicleEntry(
            ImportHelper::attributesAndTranslations(
                [
                    'year' => (int)substr($row['date'], 0, 4),
                    'date' => strlen($row['date']) === 10 ? $row['date'] : null
                ],
                $row,
                ['title', 'text']
            )
        );
    }
}
