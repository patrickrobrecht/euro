<?php

namespace App\Imports;

use App\Models\Country;
use App\Models\Mint;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MintsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return Model|null
    */
    public function model(array $row)
    {
        $country = Country::findByCode($row['country']);

        if (!$country) {
            echo sprintf("ERROR: Couldn't find country %s" . PHP_EOL, $row['country']);
            return null;
        }

        return new Mint(
            ImportHelper::attributesAndTranslations(
                [
                    'local_name' => $row['local_name'],
                    'country_id' => $country->id,
                    'website' => $row['website']
                ],
                $row,
                ['name']
            )
        );
    }
}
