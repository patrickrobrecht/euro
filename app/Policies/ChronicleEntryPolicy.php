<?php

namespace App\Policies;

use App\Models\ChronicleEntry;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChronicleEntryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param ChronicleEntry $chronicleEntry
     * @return mixed
     */
    public function update(User $user, ChronicleEntry $chronicleEntry)
    {
        return $user->isAdmin();
    }
}
