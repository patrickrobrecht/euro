<?php

namespace App\Policies;

use App\Models\CoinSeries;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoinSeriesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param CoinSeries $coinSeries
     * @return mixed
     */
    public function view(User $user, CoinSeries $coinSeries)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param CoinSeries $coinSeries
     * @return mixed
     */
    public function update(User $user, CoinSeries $coinSeries)
    {
        return $user->isAdmin();
    }
}
