<?php

namespace App\Policies;

use App\Models\Mint;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MintPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Mint $model
     * @return mixed
     */
    public function view(User $user, Mint $model)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Mint $model
     * @return mixed
     */
    public function update(User $user, Mint $model)
    {
        return $user->isAdmin();
    }
}
