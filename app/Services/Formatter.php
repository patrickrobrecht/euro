<?php

namespace App\Services;

use Carbon\Carbon;

class Formatter
{
    private const TRANSLATIONS_FILE_NAME = 'formatting';

    public static function date(Carbon $date): string
    {
        return $date->format(__(self::TRANSLATIONS_FILE_NAME . '.date'));
    }

    public static function number(float $number, $decimals = 2): string
    {
        return number_format(
            $number,
            $decimals,
            __(self::TRANSLATIONS_FILE_NAME . '.decimal_separator'),
            __(self::TRANSLATIONS_FILE_NAME . '.thousands_separator')
        );
    }
}
