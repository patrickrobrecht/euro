#!/usr/bin/env bash

composer generate

# Generate issue overview PDF from *.xlsx
cd storage/app/public/downloads/de
soffice --headless --convert-to pdf *.xlsx && echo "PDF created"
cd ../en
soffice --headless --convert-to pdf *.xlsx && echo "PDF created"
