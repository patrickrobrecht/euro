<?php

return [

    /**
     * The locales you wish to support.
     */
    'supported-locales' => [
        'de',
        'en',
    ],

];
