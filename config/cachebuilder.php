<?php

return [

  'enable'  => env('DB_CACHE', true),

  'minutes' => 1,

];
