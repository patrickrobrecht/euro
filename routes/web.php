<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AccountController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\CoinController;
use App\Http\Controllers\CoinSeriesController;
use App\Http\Controllers\CountryCirculationCoinsController;
use App\Http\Controllers\CountryCommemorativeCoinsController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\ChronicleController;
use App\Http\Controllers\DesignerController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\MintController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SitemapFileController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;

Route::get('/', HomeController::class . '@redirect');

Route::get('/sitemap.xml', SitemapFileController::class)
    ->name('sitemap');

Route::localized(static function () {
    Route::get('', HomeController::class)
        ->name('home');

    /**
     * Authentication routes, similar to @see \Illuminate\Routing\Router::auth()
     */
    // Login
    Route::get(Lang::uri('login'), LoginController::class . '@showLoginForm')
        ->name('login');
    Route::post(Lang::uri('login'), LoginController::class . '@login');

    // Logout
    Route::post(Lang::uri('logout'), LoginController::class . '@logout')
        ->name('logout');

    // Registration
    Route::get(Lang::uri('register'), RegisterController::class . '@showRegistrationForm')
        ->name('register');
    Route::post(Lang::uri('register'), RegisterController::class . '@register');

    // Password reset
    Route::prefix(Lang::uri('password'))->group(static function () {
        Route::get(Lang::uri('reset'), ForgotPasswordController::class . '@showLinkRequestForm')
            ->name('password.request');
        Route::post(Lang::uri('email'), ForgotPasswordController::class . '@sendResetLinkEmail')
            ->name('password.email');
        Route::get(Lang::uri('reset') . '/{token}', ResetPasswordController::class . '@showResetForm')
            ->name('password.reset');
        Route::post(Lang::uri('reset'), ResetPasswordController::class . '@reset')
            ->name('password.update');
    });

    // E-Mail address verification
    Route::prefix(Lang::uri('email'))->group(static function () {
        Route::get(Lang::uri('verify'), VerificationController::class . '@show')
            ->name('verification.notice');
        Route::get(Lang::uri('verify') . '/{id}/{hash}', VerificationController::class . '@verify')
            ->name('verification.verify');
        Route::post(Lang::uri('resend'), VerificationController::class . '@resend')
            ->name('verification.resend');
    });

    Route::middleware('auth')->group(static function () {
        Route::get(Lang::uri('account'), AccountController::class)
            ->name('account');

        Route::prefix(Lang::uri('admin'))->group(static function () {
            Route::prefix(Lang::uri('countries'))->group(static function () {
                Route::get('', CountryController::class . '@index')
                    ->name('country.index');
                Route::get(Lang::uri('create'), CountryController::class . '@create')
                    ->name('country.create');
                Route::post(Lang::uri('create'), CountryController::class . '@store')
                    ->name('country.store');
                Route::get(Lang::uri('edit') . '/{countryId}', CountryController::class . '@edit')
                    ->name('country.edit');
                Route::post(Lang::uri('edit') . '/{countryId}', CountryController::class . '@update')
                    ->name('country.update');
            });

            Route::prefix(Lang::uri('coin-series'))->group(static function () {
                Route::get('', CoinSeriesController::class . '@index')
                    ->name('coin_series.index');
                Route::get(Lang::uri('create'), CoinSeriesController::class . '@create')
                    ->name('coin_series.create');
                Route::post(Lang::uri('create'), CoinSeriesController::class . '@store')
                    ->name('coin_series.store');
                Route::get(Lang::uri('edit') . '/{coinSeriesId}', CoinSeriesController::class . '@edit')
                    ->name('coin_series.edit');
                Route::post(Lang::uri('edit') . '/{coinSeriesId}', CoinSeriesController::class . '@update')
                    ->name('coin_series.update');
            });

            Route::prefix(Lang::uri('coins'))->group(static function () {
                Route::get('', CoinController::class . '@index')
                    ->name('coin.index');
                Route::get(Lang::uri('create'), CoinController::class . '@create')
                    ->name('coin.create');
                Route::post(Lang::uri('create'), CoinController::class . '@store')
                    ->name('coin.store');
                Route::get(Lang::uri('edit') . '/{coinId}', CoinController::class . '@edit')
                    ->name('coin.edit');
                Route::post(Lang::uri('edit') . '/{coinId}', CoinController::class . '@update')
                    ->name('coin.update');
            });

            Route::prefix(Lang::uri('mints'))->group(static function () {
                Route::get('', MintController::class . '@index')
                    ->name('mint.index');
                Route::get(Lang::uri('create'), MintController::class . '@create')
                    ->name('mint.create');
                Route::post(Lang::uri('create'), MintController::class . '@store')
                    ->name('mint.store');
                Route::get(Lang::uri('edit') . '/{mintId}', MintController::class . '@edit')
                    ->name('mint.edit');
                Route::post(Lang::uri('edit') . '/{mintId}', MintController::class . '@update')
                    ->name('mint.update');
            });

            Route::prefix(Lang::uri('coin-designers'))->group(static function () {
                Route::get('', DesignerController::class . '@index')
                    ->name('designer.index');
                Route::get(Lang::uri('create'), DesignerController::class . '@create')
                    ->name('designer.create');
                Route::post(Lang::uri('create'), DesignerController::class . '@store')
                    ->name('designer.store');
                Route::get(Lang::uri('edit') . '/{designerId}', DesignerController::class . '@edit')
                    ->name('designer.edit');
                Route::post(Lang::uri('edit') . '/{designerId}', DesignerController::class . '@update')
                    ->name('designer.update');
            });

            Route::prefix(Lang::uri('chronicle'))->group(static function () {
                Route::get('', ChronicleController::class . '@index')
                    ->name('chronicle_entry.index');
                Route::get(Lang::uri('create'), ChronicleController::class . '@create')
                    ->name('chronicle_entry.create');
                Route::post(Lang::uri('create'), ChronicleController::class . '@store')
                    ->name('chronicle_entry.store');
                Route::get(Lang::uri('edit') . '/{chronicleEntryId}', ChronicleController::class . '@edit')
                    ->name('chronicle_entry.edit');
                Route::post(Lang::uri('edit') . '/{chronicleEntryId}', ChronicleController::class . '@update')
                    ->name('chronicle_entry.update');
            });

            Route::prefix(Lang::uri('users'))->group(static function () {
                Route::get('', UserController::class . '@index')
                    ->name('user.index');
                Route::get(Lang::uri('create'), UserController::class . '@create')
                    ->name('user.create');
                Route::post(Lang::uri('create'), UserController::class . '@store')
                    ->name('user.store');
                Route::get(Lang::uri('edit') . '/{userId}', UserController::class . '@edit')
                    ->name('user.edit');
                Route::post(Lang::uri('edit') . '/{userId}', UserController::class . '@update')
                    ->name('user.update');
            });
        });
    });

    /**
     * Public content
     */
    Route::get(Lang::uri('circulation-coins'), PageController::class . '@circulationCoins')
        ->name('page-circulation-coins');
    Route::get(Lang::uri('2-euro-commemorative-coins'), PageController::class . '@commemorativeCoins')
        ->name('page-2-euro-commemorative-coins');
    Route::get(Lang::uri('downloads'), PageController::class . '@downloads')
        ->name('page-downloads');
    Route::get(Lang::uri('legal-notice'), PageController::class . '@legalNotice')
        ->name('page-legal-notice');
    Route::get(Lang::uri('privacy'), PageController::class . '@privacy')
        ->name('page-privacy');

    Route::prefix(Lang::uri('coin-designers'))->group(static function () {
        Route::get('', DesignerController::class)
            ->name('designers');
        Route::get('{designer}', DesignerController::class . '@show')
            ->name('designer.show');
    });

    Route::prefix(Lang::uri('mints'))->group(function () {
        Route::get('', MintController::class)
            ->name('mints');
        Route::get('{mint}', MintController::class . '@show')
            ->name('mint');
    });

    Route::get(Lang::uri('chronicle'), ChronicleController::class)
        ->name('chronicle');

    Route::get(Lang::uri('search'), SearchController::class)
        ->name('search');

    Route::prefix('{country}')->group(static function () {
        Route::get('', CountryController::class . '@show')
            ->name('country.show');
        Route::prefix(Lang::uri('circulation-coins'))->group(static function () {
            Route::get('', CountryCirculationCoinsController::class)
                ->name('circulation-coins');
            Route::get('{circulationCoinSeries}', CountryCirculationCoinsController::class . '@showSeries')
                ->name('circulation-coin-series');
        });

        Route::prefix(Lang::uri('2-euro-commemorative-coins'))->group(static function () {
            Route::get('', CountryCommemorativeCoinsController::class)
                ->name('2-euro-commemorative-coins');
        });
    });
});
