<?php

use App\Http\Controllers\Api\CoinApiController;
use App\Http\Controllers\Api\CoinSeriesApiController;
use App\Http\Controllers\Api\CountryApiController;
use App\Http\Controllers\Api\DesignerApiController;
use App\Http\Controllers\Api\FallbackApiController;
use App\Http\Controllers\Api\MarkApiController;
use App\Http\Controllers\Api\MintApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('coins')->group(static function () {
    Route::get('', CoinApiController::class . '@index')
        ->name('api-coins-index');
    Route::get('/{id}', CoinApiController::class . '@show')
        ->name('api-coins-show');
});

Route::prefix('series')->group(static function () {
    Route::get('', CoinSeriesApiController::class . '@index')
        ->name('api-series-index');
    Route::get('/{id}', CoinSeriesApiController::class . '@show')
        ->name('api-series-show');
});

Route::prefix('countries')->group(static function () {
    Route::get('', CountryApiController::class . '@index')
        ->name('api-countries-index');
    Route::get('/{id}', CountryApiController::class . '@show')
        ->name('api-countries-show');
});

Route::prefix('designers')->group(static function () {
    Route::get('', DesignerApiController::class . '@index')
        ->name('api-designers-index');
    Route::get('/{id}', DesignerApiController::class . '@show')
        ->name('api-designers-show');
});

Route::prefix('marks')->group(static function () {
    Route::get('', MarkApiController::class . '@index')
        ->name('api-marks-index');
    Route::get('/{id}', MarkApiController::class . '@show')
        ->name('api-marks-show');
});

Route::prefix('mints')->group(static function () {
    Route::get('', MintApiController::class . '@index')
        ->name('api-mints-index');
    Route::get('/{id}', MintApiController::class . '@show')
        ->name('api-mints-show');
});

Route::fallback(FallbackApiController::class);
