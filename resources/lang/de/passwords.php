<?php

return [

    'password' => 'Passwörter müssen mindestens 8 Zeichen lang sein und korrekt bestätigt werden.',
    'reset' => 'Das Passwort wurde zurückgesetzt!',
    'sent' => 'Wir haben dir einen Passwort-Zurücksetzen-Link geschickt!',
    'token' => 'Der Passwort-Wiederherstellungs-Schlüssel ist ungültig oder abgelaufen.',
    'user' => 'Es konnte leider kein Nutzer mit dieser E-Mail-Adresse gefunden werden.',

];
