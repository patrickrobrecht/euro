<?php

return [

    'failed' => 'Der Nutzername und das Passwort stimmen nicht mit unseren Unterlagen überein. Bitte überprüfe deine Angaben und versuche es erneut.',
    'throttle' => 'Zu viele Anmeldeversuche. Versuche es bitte in :seconds Sekunden nochmal.',

];
