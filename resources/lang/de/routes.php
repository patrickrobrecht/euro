<?php
// German routes

return [
    '2-euro-commemorative-coins' => '2-euro-gedenkmuenzen',
    'account' => 'konto',
    'admin' => 'admin',
    'chronicle' => 'chronik',
    'circulation-coins' => 'kursmuenzen',
    'coins' => 'muenzen',
    'coin-designers' => 'muenzgestalter',
    'coin-series' => 'muenzserien',
    'countries' => 'laender',
    'create' => 'erstellen',
    'edit' => 'bearbeiten',
    'legal-notice' => 'impressum',
    'login' => 'anmelden',
    'logout' => 'abmelden',
    'register' => 'registrieren',
    'resend' => 'erneut-senden',
    'reset' => 'zuruecksetzen',
    'mints' => 'praegestaetten',
    'password' => 'passwort',
    'privacy' => 'datenschutz',
    'search' => 'suche',
    'users' => 'benutzer',
    'verify' => 'verifizieren',
];
