<?php

return [

    'date' => 'd/m/Y',
    'decimal_separator' => '.',
    'thousands_separator' => ',',

];
