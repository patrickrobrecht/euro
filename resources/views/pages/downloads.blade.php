@extends('layouts.app')

@section('title')
    {{ __('Downloads') }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('Downloads') }}
@endsection

@section('content')
    <p>
        {{ __('Download the whole catalog as a PDF.') }}
    </p>
    @php
        $currentLocale = App::getLocale();
        $currentLocaleDetails = config('app.locales')[$currentLocale];
        $basePath = '/storage/downloads/' . $currentLocaleDetails['slug'] . '/';
    @endphp

    <h2>{{ __('Euro Coin Catalog') }}</h2>
    <ul>
        <li>
            <a href="{{ $basePath . __('catalog_file_name') . '.pdf' }}">
                {{ __('Euro Coin Catalog') }} ({{ __('PDF') }})
            </a>
        </li>
    </ul>

    <h2>{{ __('Issue overview') }}</h2>
    <ul>
        <li>
            <a href="{{ $basePath . __('issue-overview') . '.pdf' }}">
                {{ __('Issue overview') }} ({{ __('PDF') }})
            </a>
        </li>
        <li>
            <a href="{{ $basePath . __('issue-overview') . '.xlsx' }}">
                {{ __('Issue overview') }} ({{ __('Excel') }})
            </a>
        </li>
    </ul>
@endsection
