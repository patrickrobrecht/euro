@extends('layouts.app')

@section('title')
    {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('Euro Coin Catalog') }}
@endsection

@section('content')
    <p class="lead">
        {{ __(':euMembers member countries of the European Union (EU) use the euro (€) as their common currency.', [
            'euMembers' => $euMemberCount
        ]) }}
        {{ __('In addition, Andorra, San Marino, Monaco, and Vatican City issue euro coins due to formal agreements with the EU.') }}
    </p>

    <div class="row">
        @foreach($countries as $country)
            @php
                /** @var App\Models\Country $country */
            @endphp
            <div class="col-12 col-sm-6 col-lg-4 col-xl-3">
                <div class="card my-3">
                    <div class="card-header">
                        <h3 class="card-title">
                            <img src="{{ asset($country->image) }}" alt="" loading="lazy">
                            <a href="{{ route('country.show', $country->slug) }}">
                                {{ $country->name }}
                            </a>
                        </h3>
                        <p class="card-subtitle text-muted">
                            {{ __('Euro since :year', ['year' => $country->euro_since]) }}
                        </p>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="{{ route('circulation-coins', $country->slug) }}" class="card-link">
                                {{ __('Circulation coins') }}  <span class="badge badge-dark">{{ $country->circulation_coins_count }}</span>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('2-euro-commemorative-coins', $country->slug) }}" class="card-link">
                                {{ __('2€ commemorative coins') }} <span class="badge badge-dark">{{ $country->commemorative_coins_count }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
@endsection
