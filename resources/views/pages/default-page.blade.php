@extends('layouts.app')

@section('title')
    {{ $page->title }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ $page->title }}
@endsection

@section('content')
    @markdown($page->contents)
@endsection
