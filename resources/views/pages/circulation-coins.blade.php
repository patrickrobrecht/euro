@extends('layouts.full-width')

@php
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Country[] $countriesForSeries */
@endphp

@section('title')
    {{ __('Circulation coins') }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('Circulation coins') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb />
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-md-8 col-lg-9">

            <h2>{{ __('Common sides')  }}</h2>

            <h2>{{ __('National sides') }}</h2>
        </div>
        <nav class="col-12 col-md-4 col-lg-3">
            <ul class="navbar-nav pl-4">
                @foreach($countriesForSeries as $country)
                    <li class="nav-item">
                        <a class="nav-link pb-0" href="{{ route('circulation-coins', $country->slug) }}">
                            <img src="{{ asset($country->image) }}" alt="" loading="lazy"> {{ $country->name }}
                            <span class="badge badge-light">{{ $country->circulation_coins_count }}</span>
                        </a>
                        <ul class="navbar-nav ml-4 small">
                            @foreach($country->circulationCoinSeries as $series)
                                <li class="nav-item">
                                    <a class="nav-link p-1" href="{{ route('circulation-coin-series', [$country->slug, $series->slug]) }}">
                                        {{ $series->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>
@endsection
