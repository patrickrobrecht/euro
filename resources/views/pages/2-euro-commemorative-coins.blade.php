@extends('layouts.full-width')

@section('title')
    {{ __('2€ commemorative coins') }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('2€ commemorative coins') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb />
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-md-8 col-lg-9">
            <h2>{{ __('Joint issues') }}</h2>
        </div>
        <nav class="col-12 col-md-4 col-lg-3">
            <ul class="navbar-nav pl-4">
                @foreach($countries as $country)
                    @php
                        /** @var App\Models\Country $country */
                    @endphp
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('2-euro-commemorative-coins', $country->slug) }}">
                            <img src="{{ asset($country->image) }}" alt="" loading="lazy"> {{ $country->name }}
                            <span class="badge badge-light">{{ $country->commemorative_coins_count }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>
@endsection
