@extends('layouts.app')

@php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $searchResults */
    /** @var \App\Models\Coin $coin */
    /** @var \App\Models\CoinSeries $coinSeries */
    /** @var \App\Models\Country $country */
    /** @var \App\Models\Designer $designer */
    /** @var \App\Models\Mint $mint */
@endphp

@section('title')
    @isset($searchTerm)
        {{ __('Search results for ":term"', ['term' => $searchTerm]) }}
    @else
        {{ __('Search') }}
    @endisset
@endsection

@section('headline')
    @yield('title')
@endsection

@section('content')
    @isset($searchTerm, $searchResultsCount)
        <p class="lead">
            {{ trans_choice(':count results for ":term"', $searchResultsCount, ['count' => $searchResultsCount, 'term' => $searchTerm]) }}
        </p>
    @endisset

    @include('search.search_form', ['formClass' => 'my-3', 'scopes' => \App\Options\SearchScope::getAll()])

    @isset($searchTerm)
        <ul class="nav nav-pills mb-3">
            @foreach(\App\Options\SearchScope::getAll() as $scope => $name)
                <li class="nav-item">
                    <a class="nav-link {{ $scope === $searchScope ? 'active' : '' }}"
                       href="{{ route('search', ['term' => $searchTerm, 'scope' => $scope]) }}">
                        {{ $name }}
                        <span class="badge badge-light">{{ $searchResultsCountPerScope[$scope] ?? 0 }}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    @endisset

    @isset($searchResults)
        @switch($searchScope)
            @case(\App\Options\SearchScope::COINS)
                <ul>
                    @foreach($searchResults as $coin)
                        <li>
                            <a href="{{ $coin->getURL() }}">
                                {{ $coin->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @break
            @case(\App\Options\SearchScope::COIN_SERIES)
                <ul>
                    @foreach($searchResults as $coinSeries)
                        <li>
                            <a href="{{ $coinSeries->getURL() }}">
                                {{ $coinSeries->country->name }}, {{ $coinSeries->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @break
            @case(\App\Options\SearchScope::COUNTRIES)
                <ul>
                    @foreach($searchResults as $country)
                        <li>
                            <a href="{{ route('country.show', $country->slug) }}">
                                {{ $country->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @break
            @case(\App\Options\SearchScope::DESIGNERS)
                <ul>
                    @foreach($searchResults as $designer)
                        <li>
                            <a href="{{ route('designer.show', $designer->slug) }}">
                                {{ $designer->full_name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @break
            @case(\App\Options\SearchScope::MINTS)
                <ul>
                    @foreach($searchResults as $mint)
                        <li>
                            <a href="{{ route('mint', $mint->slug) }}">
                                {{ $mint->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @break
        @endswitch

        {{ $searchResults->withQueryString()->links() }}
    @endisset
@endsection
