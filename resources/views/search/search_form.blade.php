<form action="{{ route('search') }}" method="GET" @isset($formClass) class="{{ $formClass }}" @endisset>
    <div class="input-group">
        @isset($scopes, $searchScope)
            <div class="input-group-prepend">
                <select id="scope" name="scope" class="custom-select">
                    @foreach($scopes as $scope => $name)
                        <option value="{{ $scope }}" @if($scope === $searchScope) selected @endif>{{ $name }}</option>
                    @endforeach
                </select>
            </div>
        @endisset
        <label class="sr-only" for="term">{{ __('search term') }}</label>
        <input id="term" name="term" class="form-control" @isset($searchTerm)value="{{ $searchTerm }}"@endisset
               type="text" minlength="1" required>
        <div class="input-group-append">
            <input type="submit" class="btn btn-sm btn-primary" value="{{ __('Search') }}">
        </div>
    </div>
</form>
