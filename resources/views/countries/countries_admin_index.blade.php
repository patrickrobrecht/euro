@extends('layouts.app')

@php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $allCountries */
    /** @var \App\Models\Country $country */
@endphp

@section('title')
    {{ __('Countries') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @can('create', \App\Models\Country::class)
        <a class="btn btn-primary" href="{{ route('country.create') }}">
            {{ __('Create country') }}
        </a>
    @endcan

    @foreach($allCountries as $country)
        <div class="row my-3">
            <div class="col-12 col-md-4">
                <strong>{{ $country->name }}</strong>
                <div>{{ $country->full_name }}</div>
            </div>
            <div class="col-12 col-md-4">
                {{ $country->code }}
            </div>
            <div class="col-12 col-md-4">
                <div class="btn-group align-content-end">
                    @can('view', $country)
                        <a class="btn btn-secondary" href="{{ route('country.show', $country->slug) }}">
                            {{ __('Show') }}
                        </a>
                    @endcan
                    @can('update', $country)
                        <a class="btn btn-primary" href="{{ route('country.edit', $country->id) }}">
                            {{ __('Edit') }}
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    {{ $allCountries->withQueryString()->links() }}
@endsection
