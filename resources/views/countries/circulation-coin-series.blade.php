@php
    /** @var App\Models\Country $country */
    /** @var App\Models\CoinSeries $series */
@endphp

@extends('layouts.full-width')

@section('title')
    {{ __('Circulation coins') }} {{ $country->name }} {{ $series->name }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('Circulation coins') }} <img src="{{ asset($country->image) }}" alt="" loading="lazy"> {{ $country->full_name }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb href="{{ route('country.show', $country->slug) }}">{{ $country->name }}</x-nav.breadcrumb>
    <x-nav.breadcrumb href="{{ route('circulation-coins', $country->slug) }}">{{ __('Circulation coins') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb>{{ $series->name }}</x-nav.breadcrumb>
@endsection

@section('content')
    <h2>@include('common.coin-series-title')</h2>

    @include('common.circulation-coin-series')

    @foreach($series->coins as $coin)
        <h3 class="pb-2">{{ $coin->formatted_denomination }}</h3>
        @include('common.coin-details')
    @endforeach
@endsection
