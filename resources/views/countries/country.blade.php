@php
    /** @var App\Models\Country $country */
@endphp

@extends('layouts.app')

@section('title')
    {{ $country->name }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    <img src="{{ asset($country->image) }}" alt="" loading="lazy">{{ $country->full_name }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ $country->name }}</x-nav.breadcrumb>
@endsection

@section('edit')
    @can('update', $country)
        <a class="btn btn-primary" href="{{ route('country.edit', $country->id) }}">
            {{ __('Edit') }}
        </a>
    @endcan
@endsection

@section('content')
    <p class="lead text-center">
        @if($country->euro_since)
            {{ __('Euro since :year', ['year' => $country->euro_since]) . ',' }}
        @endif
        @if($country->eu_member_since)
            {{ __('EU member since :year', ['year' => $country->eu_member_since]) }}
        @else
            {{ __('no EU member') }}
        @endif
    </p>

    <p>
        @if($country->mints->count() === 0)
            {{ __('The country does not have an own mint, the coins are minted in foreign mints.') }}
        @else
            @if($country->mints->count() === 1)
                {{ __('The country has one mint,') }}
            @else
                {{ __('The following mints are in the country:') }}
            @endif
            @foreach($country->mints as $mint)
                <a href="{{ route('mint', $mint->slug) }}">{{ $mint->name }}</a>@if($loop->last). @elseif($loop->remaining === 1) {{ __('and') }} @else,@endif
            @endforeach
        @endif
    </p>

    <div class="row">
        <div class="col-12 col-sm-6">
            <div class="card my-3">
                <div class="card-header">
                    <h2 class="card-title">{{ __('Circulation coins') }}</h2>
                    @if($country->circulation_coins_count > 0)
                        <a href="{{ route('circulation-coins', $country->slug) }}" class="btn btn-primary">
                            {{ __('Show all') }} <span class="badge badge-light">{{ $country->circulation_coins_count }}</span>
                        </a>
                    @else
                        {{ __('No circulation coins issued yet.') }}
                    @endif
                </div>
                <ul class="list-group list-group-flush">
                    @foreach($country->circulationCoinSeries as $series)
                        <li class="list-group-item">
                            <a href="{{ route('circulation-coin-series', [$country->slug, $series->slug]) }}">
                                @include('common.coin-series-title')
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="card my-3">
                <div class="card-header">
                    <h2 class="card-title">{{ __('2€ commemorative coins') }}</h2>
                    @if($country->commemorative_coins_count > 0)
                        <a href="{{ route('2-euro-commemorative-coins', $country->slug) }}" class="btn btn-primary">
                            {{ __('Show all') }} <span class="badge badge-light">{{ $country->commemorative_coins_count }}</span>
                        </a>
                    @else
                        {{ __('No 2€ commemorative coins issued yet.') }}
                    @endif
                </div>
                <ul class="list-group list-group-flush">
                    @foreach($country->commemorativeCoinSeries as $series)
                        <li class="list-group-item">
                            <a href="{{ route('2-euro-commemorative-coins', $country->slug) }}#{{ $series->slug }}">
                                @include('common.coin-series-title')
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
