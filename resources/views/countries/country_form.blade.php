@extends('layouts.app')

@php
    /** @var \App\Models\Country $country */
@endphp

@section('title')
    @isset($country)
        {{ __('Edit :name', ['name' => $country->name]) }}
    @else
        {{ __('Create country') }}
    @endisset
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb href="{{ route('country.index') }}">{{ __('Countries') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb />
@endsection

@section('content')
    <x-alerts.success/>

    <form method="POST" action="{{ isset($country) ? route('country.update', $country->id) : route('country.store') }}">
        @csrf

        <div class="form-row">
            <label class="col-form-label col-md-2">{{ __('Name') }}</label>
            <div class="form-group col-md-10">
                <x-forms.translated-input field="name" :model="$country ?? null" required></x-forms.translated-input>
            </div>
        </div>
        <div class="form-row">
            <label class="col-form-label col-md-2">{{ __('Full name') }}</label>
            <div class="form-group col-md-10">
                <x-forms.translated-input field="full_name" :model="$country ?? null" required></x-forms.translated-input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12">
                <x-forms.label field="image">{{ __('Image') }}</x-forms.label>
                <x-forms.input field="image" required>{{ $country->image ?? '' }}</x-forms.input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <x-forms.label field="code">{{ __('Country code') }}</x-forms.label>
                <x-forms.input field="code" required>{{ $country->code ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-4">
                <x-forms.label field="euro_since">{{ __('Euro since') }}</x-forms.label>
                <x-forms.input type="number" field="euro_since">{{ $country->euro_since ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-4">
                <x-forms.label field="eu_member_since">{{ __('EU member since') }}</x-forms.label>
                <x-forms.input type="number" field="eu_member_since">{{ $country->eu_member_since ?? '' }}</x-forms.input>
            </div>
        </div>

        <div class="btn-group" role="group">
            <x-forms.button-save :save="isset($country)"></x-forms.button-save>
            <x-forms.link-cancel>{{ route('country.index') }}</x-forms.link-cancel>
        </div>
    </form>
@endsection
