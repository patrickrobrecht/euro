@php
    /** @var App\Models\Country $country */
@endphp

@extends('layouts.full-width')

@section('title')
    {{ __('Circulation coins') }} {{ $country->name }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('Circulation coins') }} <img src="{{ asset($country->image) }}" alt="" loading="lazy"> {{ $country->full_name }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb href="{{ route('country.show', $country->slug) }}">{{ $country->name }}</x-nav.breadcrumb>
    <x-nav.breadcrumb>{{ __('Circulation coins') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @if($country->circulationCoins->count() == 0)
        {{ __('No circulation coins issued yet.') }}
    @endif

    @foreach($country->circulationCoinSeries as $series)
        <h2 id="{{ $series->slug }}">
            <a href="{{ route('circulation-coin-series', [$country->slug, $series->slug]) }}">
                @include('common.coin-series-title')
            </a>
        </h2>

        @include('common.circulation-coin-series')
    @endforeach
@endsection
