@php
    /** @var App\Models\Country $country */
@endphp

@extends('layouts.full-width')

@section('title')
    {{ __('2€ commemorative coins') }} {{ $country->name }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('2€ commemorative coins') }} <img src="{{ asset($country->image) }}" alt="" loading="lazy"> {{ $country->full_name }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb href="{{ route('country.show', $country->slug) }}">{{ $country->name }}</x-nav.breadcrumb>
    <x-nav.breadcrumb>{{ __('2€ commemorative coins') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @if($country->commemorativeCoins->count() == 0)
        {{ __('No 2€ commemorative coins issued yet.') }}
    @endif

    @if($country->commemorativeCoinSeries->count() > 0)
        <div id="series-filter" class="text-center my-2" role="group">
            <button id="btn-coin-all" class="btn btn-primary active" onclick="filterCoins('all')" aria-pressed="true">
                {{ __('all') }}
                <span class="badge badge-light">{{ $country->commemorativeCoins->count() }}</span>
            </button>

            @foreach($country->commemorativeCoinSeries as $series)
                <button id="btn-coin-{{ $series->slug }}" class="btn btn-primary" onclick="filterCoins('{{ $series->slug }}')">
                    @include('common.coin-series-title')
                    <span class="badge badge-light">{{ $series->coins->count() }}</span>
                </button>
            @endforeach
        </div>
    @endif

    @php
        $years = [];
    @endphp
    @foreach($country->commemorativeCoins as $coin)
        @php
            /** @var App\Models\Coin $coin */
            $year = $coin->first_year;
            $firstInYear = !in_array($year, $years);
            if ($firstInYear) {
                $years[] = $year;
            }
        @endphp

        @if($firstInYear)
           <h2 id="{{ $year }}" class="my-4">{{ __('Year :year', ['year' => $year])}}</h2>
        @endif

        <div class="media commemorative-coin mt-2 coin-all @if($coin->series)coin-{{ $coin->series->slug }}@endif">
            @if($coin->image)
                <img src="{{ asset($coin->image) }}" class="mr-3 coin-{{ ($coin->denomination * 100) }}" loading="lazy">
            @endif
            <div class="media-body" id="{{ $coin->id }}">
                @if($coin->series)
                    <small>{{ $coin->formatted_issue_date }}</small>,
                    <small class="badge badge-primary">{{ __('part of the series ":series"', ['series' => $coin->series->name]) }}</small>
                @else
                    <small>{{ $coin->formatted_issue_date }}</small>
                @endif

                <h3>
                    {{ $coin->name }}
                </h3>
                <p>{{ __('Description') }}: {{ $coin->description }}</p>

                @php
                    $designersList = [];
                    $engraversList = [];
                    foreach ($coin->designerDetails as $designerDetails) {
                        if ($designerDetails->role === 'ENGRAVING') {
                            $engraversList[] = $designerDetails;
                        } else {
                            $designersList[] = $designerDetails;
                        }
                    }
                @endphp
                @include('common.designers-list', [
                    'designersList' => $designersList,
                    'role' => __('Design')
                ])
                @include('common.designers-list', [
                    'designersList' => $engraversList,
                    'role' => __('Engraving')
                ])

                @include('common.coin-details')
            </div>
        </div>
    @endforeach
@endsection

@section('scripts')
    @parent

    <script>
        var series = $(location).attr('hash');
        if (series) {
            filterCoins(series.replace('#', ''));
        }

        function filterCoins(series) {
            $('.commemorative-coin').hide();
            $('.coin-' + series).show();

            $('#series-filter .btn').removeClass('active');
            $('#btn-coin-' + series).addClass('active');
        }
    </script>
@endsection
