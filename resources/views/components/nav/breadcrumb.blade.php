
@isset($href)
    <li class="breadcrumb-item">
        <a href="{{ $href }}">{{ $slot }}</a>
    </li>
@else
    @php
        /** @var \Illuminate\Support\HtmlString $slot */
    @endphp
    <li class="breadcrumb-item active" aria-current="page">
        @if($slot->isEmpty())
            @hasSection('headline')
                @yield('headline')
            @else
                @yield('title')
            @endif
        @else
            {{ $slot }}
        @endif
    </li>
@endif
