<label for="{{ $field }}" @isset($class) class="{{ $class }}" @endisset>
    {{ $slot }}
</label>
