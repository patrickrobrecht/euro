@php
    $value = old($field) ?? $slot;
    if (isset($clearOld) && $clearOld) {
        $value = '';
    }
@endphp
<input id="{{ $field }}" name="{{ $field }}" type="{{ $type ?? 'text' }}"
       @if(isset($required) && $required) required @endif
       class="form-control {{ $class ?? '' }} @error($field) is-invalid @enderror"
       value="{{ $value }}">

@error($field)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
