@foreach(config('app.locales') as $locale => $localeDetails)
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">{{ strtoupper($locale) }}</span>
        </div>
        <x-forms.input field="{{ $field }}:{{ $locale }}" required="{{ $required ?? false }}">
            {{ $model ? $model->translate($locale)->{$field} : '' }}
        </x-forms.input>
    </div>
@endforeach
