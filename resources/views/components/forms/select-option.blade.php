@php
    $selectedValue = old() ? old($field) ?? '' : $selected;
@endphp
<option value="{{ $value }}" @if($value === $selectedValue) selected @endif>
    {{ $slot }}
</option>
