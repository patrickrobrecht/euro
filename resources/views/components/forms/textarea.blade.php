@php
    $value = old($field) ?? $slot;
    if (isset($clearOld) && $clearOld) {
        $value = '';
    }
@endphp
<textarea id="{{ $field }}" name="{{ $field }}"
       @if(isset($required) && $required) required @endif
       class="form-control {{ $class ?? '' }} @error($field) is-invalid @enderror">{{ $value }}</textarea>

@error($field)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
