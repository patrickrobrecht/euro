<button type="{{ $type ?? 'submit' }}" class="{{ $class ?? 'btn btn-primary' }}">
    @if($save)
        {{ __( 'Save' ) }}
    @else
        {{ __('Create') }}
    @endif
</button>
