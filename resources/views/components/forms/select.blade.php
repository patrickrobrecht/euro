<select id="{{ $field }}" name="{{ $field }}" class="form-control @error($field) is-invalid @enderror">
    {{ $slot }}
</select>

@error($field)
    <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
    </span>
@enderror
