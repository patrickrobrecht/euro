@extends('layouts.app')

@php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $coinSeries */
    /** @var \App\Models\CoinSeries $series */
@endphp

@section('title')
    {{ __('Coin series') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @can('create', \App\Models\CoinSeries::class)
        <a class="btn btn-primary" href="{{ route('coin_series.create') }}">
            {{ __('Create coin series') }}
        </a>
    @endcan

    @foreach($coinSeries as $series)
        <div class="row my-3">
            <div class="col-12 col-md-8">
                <strong>{{ $series->country->name }}, {{ $series->name }}</strong>
            </div>
            <div class="col-12 col-md-4">
                <div class="btn-group align-content-end">
                    @can('update', $series)
                        <a class="btn btn-primary" href="{{ route('coin_series.edit', $series->id) }}">
                            {{ __('Edit') }}
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    {{ $coinSeries->withQueryString()->links() }}
@endsection
