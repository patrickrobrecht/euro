@extends('layouts.app')

@php
    /** @var \App\Models\CoinSeries $coinSeries */
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Country[] $countries */
@endphp

@section('title')
    @isset($coinSeries)
        {{ __('Edit :name', ['name' => $coinSeries->country->name . ' ' . $coinSeries->name]) }}
    @else
        {{ __('Create coin series') }}
    @endisset
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb href="{{ route('coin_series.index') }}">{{ __('Coin series') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb />
@endsection

@section('content')
    <x-alerts.success/>

    <form method="POST" action="{{ isset($coinSeries) ? route('coin_series.edit', $coinSeries->id) : route('coin_series.store') }}">
        @csrf

        <div class="form-row">
            <label class="col-form-label col-md-2">{{ __('Name') }}</label>
            <div class="form-group col-md-10">
                <x-forms.translated-input field="name" :model="$coinSeries ?? null" required></x-forms.translated-input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <x-forms.label field="country_id">{{ __('Country') }}</x-forms.label>
                <x-forms.select field="country_id">
                    @foreach($countries as $country)
                        <x-forms.select-option field="type" value="{{ $country->id }}" selected="{{ $coinSeries->country->id ?? '' }}">
                            {{ $country->name }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
            <div class="form-group col-md-4">
                <x-forms.label field="type">{{ __('Type') }}</x-forms.label>
                <x-forms.select field="type">
                    @foreach(\App\Options\CoinType::getAll() as $type => $name)
                        <x-forms.select-option field="type" value="{{ $type }}" selected="{{ $coinSeries->type ?? '' }}">
                            {{ $name }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <x-forms.label field="first_year">{{ __('First year') }}</x-forms.label>
                <x-forms.input type="number" field="first_year" required>{{ $coinSeries->first_year ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-4">
                <x-forms.label field="last_year">{{ __('Last year') }}</x-forms.label>
                <x-forms.input type="number" field="last_year" required>{{ $coinSeries->last_year ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-4">
                <x-forms.label field="code">{{ __('Code') }}</x-forms.label>
                <x-forms.input field="code" required>{{ $coinSeries->code ?? '' }}</x-forms.input>
            </div>
        </div>

        <div class="btn-group" role="group">
            <x-forms.button-save :save="isset($coinSeries)"></x-forms.button-save>
            <x-forms.link-cancel>{{ route('coin_series.index') }}</x-forms.link-cancel>
        </div>
    </form>
@endsection
