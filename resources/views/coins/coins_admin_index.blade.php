@extends('layouts.app')

@php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $coins */
    /** @var \App\Models\Coin $coin */
@endphp

@section('title')
    {{ __('Coins') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @can('create', \App\Models\Coin::class)
        <a class="btn btn-primary" href="{{ route('coin.create') }}">
            {{ __('Create coin') }}
        </a>
    @endcan

    @foreach($coins as $coin)
        <div class="row my-3">
            <div class="col-12 col-md-8">
                <div><strong>{{ $coin->name }}</strong></div>
                <div>{{ $coin->country->name }}, {{ $coin->series->name ?? __('no series') }}</div>
            </div>
            <div class="col-12 col-md-4">
                <div class="btn-group align-content-end">
                    @can('update', $coin)
                        <a class="btn btn-primary" href="{{ route('coin.edit', $coin->id) }}">
                            {{ __('Edit') }}
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    {{ $coins->withQueryString()->links() }}
@endsection
