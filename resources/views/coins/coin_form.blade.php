@extends('layouts.app')

@php
    /** @var \App\Models\Coin $coin */
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Country[] $countries */
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\CoinSeries[] $allCoinSeries */
@endphp

@section('title')
    @isset($coin)
        {{ __('Edit :name', ['name' => $coin->country->name . ' ' . $coin->name]) }}
    @else
        {{ __('Create coin') }}
    @endisset
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb href="{{ route('coin.index') }}">{{ __('Coins') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb />
@endsection

@section('content')
    <x-alerts.success/>

    <form method="POST" action="{{ isset($coin) ? route('coin.update', $coin->id) : route('coin.store') }}">
        @csrf

        <div class="form-row">
            <label class="col-form-label col-md-2">{{ __('Name') }}</label>
            <div class="form-group col-md-10">
                <x-forms.translated-input field="name" :model="$coin ?? null" required></x-forms.translated-input>
            </div>
        </div>
        <div class="form-row">
            <label class="col-form-label col-md-2">{{ __('Description') }}</label>
            <div class="form-group col-md-10">
                <x-forms.translated-textarea field="description" :model="$coin ?? null" required></x-forms.translated-textarea>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <x-forms.label field="country_id">{{ __('Country') }}</x-forms.label>
                <x-forms.select field="country_id">
                    @foreach($countries as $country)
                        <x-forms.select-option field="type" value="{{ $country->id }}" selected="{{ $coinSeries->country->id ?? '' }}">
                            {{ $country->name }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
            <div class="form-group col-md-4">
                <x-forms.label field="type">{{ __('Type') }}</x-forms.label>
                <x-forms.select field="type">
                    @foreach(\App\Options\CoinType::getAll() as $type => $name)
                        <x-forms.select-option field="type" value="{{ $type }}" selected="{{ $coin->type ?? '' }}">
                            {{ $name }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
            <div class="form-group col-md-4">
                <x-forms.label field="coin_series_id">{{ __('Coin series') }}</x-forms.label>
                <x-forms.select field="coin_series_id">
                    <x-forms.select-option field="coin_series_id" value="" selected="{{ $coin->coinSeries->id ?? '' }}">
                        {{ __('no series') }}
                    </x-forms.select-option>
                    @foreach($allCoinSeries as $series)
                        <x-forms.select-option field="type" value="{{ $series->id }}" selected="{{ $coin->coinSeries->id ?? '' }}">
                            {{ $series->country->name }}, {{ $series->name }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <x-forms.label field="denomination">{{ __('Denomination') }}</x-forms.label>
                <x-forms.select field="denomination">
                    @foreach(\App\Options\Denomination::getAll() as $denomination)
                        <x-forms.select-option field="denomination" value="{{ $denomination }}" selected="{{ $coin->denomination ?? '' }}">
                            {{ \App\Options\Denomination::getName($denomination) }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
            <div class="form-group col-md-4">
                <x-forms.label field="issue_date">{{ __('Issue date') }}</x-forms.label>
                <x-forms.input type="date" field="issue_date">{{ $coin->code ?? '' }}</x-forms.input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12">
                <x-forms.label field="image">{{ __('Image') }}</x-forms.label>
                <x-forms.input field="image">{{ $coin->image ?? '' }}</x-forms.input>
            </div>
        </div>

        <div class="btn-group" role="group">
            <x-forms.button-save :save="isset($coin)"></x-forms.button-save>
            <x-forms.link-cancel>{{ route('coin.index') }}</x-forms.link-cancel>
        </div>
    </form>
@endsection
