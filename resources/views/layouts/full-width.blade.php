@extends('layouts.app')

@section('main')
    <main class="container-fluid">
        <h1 class="text-center">@yield('headline')</h1>
        @yield('content')
    </main>
@endsection
