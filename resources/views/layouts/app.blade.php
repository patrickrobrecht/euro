<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light d-flex flex-row">
                <a class="navbar-brand align-self-start" href="{{ route('home') }}">
                    {{ __('Euro Coin Catalog') }}
                </a>

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"
                        aria-controls="header-nav" aria-expanded="false" aria-label="{{ __('Toggle menu') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar-collapse collapse d-lg-flex flex-row flex-wrap" id="header-nav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="coinsDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('Euro coins') }}
                            </a>

                            <div class="dropdown-menu" aria-labelledby="coinsDropdown">
                                <a class="dropdown-item" href="{{ route('page-circulation-coins') }}">
                                    {{ __('Circulation coins') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('page-2-euro-commemorative-coins') }}">
                                    {{ __('2€ commemorative coins') }}
                                </a>
                            </div>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="countriesDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('Euro countries') }}
                            </a>

                            <div class="dropdown-menu countries-dropdown" aria-labelledby="countriesDropdown">
                                @foreach ($countries as $c)
                                    @php
                                        /** @var App\Models\Country $c */
                                    @endphp
                                    <a class="dropdown-item" href="{{ route('country.show', $c->slug) }}">
                                        <img src="{{ asset($c->image) }}" alt="" loading="lazy"> {{ $c->name }}
                                        <span class="badge badge-light">{{ $c->coins_count }}</span>
                                    </a>
                                @endforeach
                            </div>
                        </li>

                        {{-- other menu ithems --}}
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('mints') }}">
                                {{ __('Mints') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('designers') }}">
                                {{ __('Coin designers') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('chronicle') }}">
                                {{ __('Chronicle') }}
                            </a>
                        </li>
                    </ul>

                    @if(!isset($searchScope))
                        @include('search.search_form', ['formClass' => 'form-inline order-lg-2 order-xl-1'])
                    @endisset

                    <ul class="nav navbar-nav ml-auto mr-4 justify-content-end order-lg-1 order-xl-2">
                        <li class="nav-item dropdown">
                            {{-- current language --}}
                            @php
                                $currentLocale = App::getLocale();
                                $currentLocaleDetails = config('app.locales')[$currentLocale];
                            @endphp
                            <a class="nav-link dropdown-toggle" href="#" id="languageDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $currentLocaleDetails['native'] }}
                            </a>

                            {{-- language toggle --}}
                            <div class="dropdown-menu" aria-labelledby="languageDropdown">
                                @foreach(config('app.locales') as $locale => $localeDetails)
                                    @if($locale !== App::getLocale())
                                        @php
	                                        $route = substr(Route::currentRouteName(), 3);
	                                        if (in_array($route, ['home', 'designers', 'mints', 'chronicle']) || strpos($route, 'page-') === 0) {
	                                            $localizedRoute = route( $locale . '.' . $route );
	                                        } else if (isset($country) && in_array($route, ['country', 'circulation-coins', '2e-coins'])) {
                                                 $localizedRoute = route( $locale . '.' . $route, $country->translate($locale)->slug );
                                            } else if (isset($mint) && $route === 'mint') {
                                                 $localizedRoute = route( $locale . '.' . $route, $mint->translate($locale)->slug );
                                            } else if (isset($designer) && $route === 'designer') {
                                                 $localizedRoute = route( $locale . '.' . $route, $designer->slug );
                                            } else {
                                                $localizedRoute = route( $locale . '.home' );
                                            }
	                                    @endphp
                                        <a class="dropdown-item" href="{{ $localizedRoute }}" hreflang="{{ $locale }}">
                                            {{ $localeDetails['native'] }}
                                        </a>
                                    @endIf
                                @endforeach
                            </div>
                        </li>
                        @auth
                            @php
                                $currentUser = \Illuminate\Support\Facades\Auth::user();
                                $adminCountries = $currentUser->can('viewAny', \App\Models\Country::class);
                                $adminCoinSeries = $currentUser->can('viewAny', \App\Models\CoinSeries::class);
                                $adminCoins = $currentUser->can('viewAny', \App\Models\CoinSeries::class);
                                $adminDesigners = $currentUser->can('viewAny', \App\Models\Designer::class);
                                $adminChronicle = $currentUser->can('viewAny', \App\Models\ChronicleEntry::class);
                                $adminUsers = Auth::user()->can('viewAny', \App\Models\User::class);
                                $admin = $adminCountries || $adminCoinSeries|| $adminCoins
                                    || $adminDesigners || $adminChronicle || $adminUsers;
                            @endphp
                            @if($admin)
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="adminDropdown" role="button"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{ __('Administration') }}
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="adminDropdown">
                                        @if($adminCountries)
                                            <a class="dropdown-item" href="{{ route('country.index') }}">
                                                {{ __('Countries') }}
                                            </a>
                                        @endif
                                        @if($adminCoinSeries)
                                            <a class="dropdown-item" href="{{ route('coin_series.index') }}">
                                                {{ __('Coin series') }}
                                            </a>
                                        @endif
                                        @if($adminCoins)
                                            <a class="dropdown-item" href="{{ route('coin.index') }}">
                                                {{ __('Coins') }}
                                            </a>
                                        @endif
                                        @if($adminDesigners)
                                            <a class="dropdown-item" href="{{ route('designer.index') }}">
                                                {{ __('Coin designers') }}
                                            </a>
                                        @endif
                                        @if($adminChronicle)
                                            <a class="dropdown-item" href="{{ route('chronicle_entry.index') }}">
                                                {{ __('Chronicle') }}
                                            </a>
                                        @endif
                                        @if($adminUsers)
                                            <a class="dropdown-item" href="{{ route('user.index') }}">
                                                {{ __('Users') }}
                                            </a>
                                        @endif
                                    </div>
                                </li>
                            @endif
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="accountDropdown" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ __('My account') }}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="accountDropdown">
                                    <a class="dropdown-item" href="{{ route('account') }}">
                                        {{ __('Profile') }}
                                    </a>
                                    <form method="post" action="{{ route('logout') }}">
                                        @csrf
                                        <button class="dropdown-item" type="submit">{{ __('Logout') }}</button>
                                    </form>
                                </div>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">
                                    {{ __('Login') }}
                                </a>
                            </li>
                        @endauth
                    </ul>
                </div>
            </nav>
        </header>

        @hasSection('breadcrumbs')
            <nav class="container my-3" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <x-nav.breadcrumb href="{{ route('home') }}">{{ __('Home') }}</x-nav.breadcrumb>
                    @yield('breadcrumbs')
                </ol>
            </nav>
        @endif

        @section('main')
            <main class="container">
                <h1 class="text-center">
                    @section('headline')
                        @yield('title')
                    @show
                </h1>
                @hasSection('breadcrumbs')
                    <div class="text-right">
                        @yield('edit')
                    </div>
                @endif
                @yield('content')
            </main>
        @show

        <footer class="container-fluid bg-light mt-4">
            <nav class="p-2">
                <ul class="nav justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('page-legal-notice') }}">
                            {{ __('Legal notice') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('page-privacy') }}">
                            {{ __('Privacy statement') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('page-downloads') }}">
                            {{ __('Downloads') }}
                        </a>
                    </li>
                </ul>
            </nav>
        </footer>

        @section('scripts')
            <script src="{{ mix('/lib/jquery.min.js') }}"></script>
            <script src="{{ mix('/lib/bootstrap.min.js') }}"></script>
        @show
    </body>
</html>
