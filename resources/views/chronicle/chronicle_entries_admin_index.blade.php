@extends('layouts.app')

@php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $chronicleEntries */
    /** @var \App\Models\ChronicleEntry $chronicleEntry */
@endphp

@section('title')
    {{ __('Chronicle') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @can('create', \App\Models\ChronicleEntry::class)
        <a class="btn btn-primary" href="{{ route('chronicle_entry.create') }}">
            {{ __('Create chronicle entry') }}
        </a>
    @endcan

    @foreach($chronicleEntries as $chronicleEntry)
        <div class="row my-3">
            <div class="col-12 col-md-8">
                <div>
                    <span class="text-muted">{{ $chronicleEntry->formatted_date }}</span>
                    <strong>{{ $chronicleEntry->title }}</strong>
                </div>
                {{ substr($chronicleEntry->text, 0, 250) . ' ...' }}
            </div>
            <div class="col-12 col-md-4">
                <div class="btn-group align-content-end">
                    @can('update', $chronicleEntry)
                        <a class="btn btn-primary" href="{{ route('chronicle_entry.edit', $chronicleEntry->id) }}">
                            {{ __('Edit') }}
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    {{ $chronicleEntries->withQueryString()->links() }}
@endsection
