@extends('layouts.app')

@section('title')
    {{ __('Chronicle of European integration') }}
@endsection

@section('headline')
    {{ __('Chronicle of European integration') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb />
@endsection

@section('edit')
    @can('viewAny', \App\Models\ChronicleEntry::class)
        <a class="btn btn-primary" href="{{ route('chronicle_entry.index') }}">
            {{ __('Edit') }}
        </a>
    @endcan
@endsection

@section('content')
    @foreach ($entries as $entry)
        @php
            /** @var App\Models\ChronicleEntry $entry */
        @endphp
        <div class="row no-gutters">
            {{-- odd iterations: text, spacer otherwise --}}
            @if($loop->iteration % 2 === 1)
                <div class="col-sm"><!--spacer--></div>
            @else
                <div class="col-sm my-sm-n5">
                    <div class="card">
                        <div class="card-body">
                            <div class="float-right text-muted small">{{ $entry->formatted_date }}</div>
                            @isset($entry->title)
                                <h2 class="card-title text-muted">{{ $entry->title }}</h2>
                            @endisset
                            <p class="card-text">{{ $entry->text }}</p>
                        </div>
                    </div>
                </div>
            @endif

            {{-- timeline dot --}}
            <div class="col-sm-1 text-center flex-column d-none d-sm-flex">
                <div class="row h-50">
                    <div class="col{{ $loop->first ? '' : ' border-right' }}">&nbsp;</div>
                    <div class="col">&nbsp;</div>
                </div>
                <h5 class="m-2">
                    <span class="badge badge-pill bg-light border">&nbsp;</span>
                </h5>
                <div class="row h-50">
                    <div class="col{{ $loop->last ? '' : ' border-right' }}">&nbsp;</div>
                    <div class="col">&nbsp;</div>
                </div>
            </div>

            {{-- even iterations: text, spacer otherwise --}}
            @if($loop->iteration % 2 === 1)
                <div class="col-sm py-2">
                    <div class="card">
                        <div class="card-body">
                            <div class="float-right text-muted small">{{ $entry->formatted_date }}</div>
                            @isset($entry->title)
                                <h2 class="card-title text-muted">{{ $entry->title }}</h2>
                            @endisset
                            <p class="card-text">{{ $entry->text }}</p>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-sm"><!--spacer--></div>
            @endif
        </div>
    @endforeach
@endsection
