@extends('layouts.app')

@php
    /** @var \App\Models\ChronicleEntry $chronicleEntry */
@endphp

@section('title')
    @isset($chronicleEntry)
        {{ __('Edit :name', ['name' => $chronicleEntry->formatted_date]) }}
    @else
        {{ __('Create chronicle entry') }}
    @endisset
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb href="{{ route('chronicle_entry.index') }}">{{ __('Chronicle') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb />
@endsection

@section('content')
    <x-alerts.success/>

    <form method="POST" action="{{ isset($chronicleEntry) ? route('chronicle_entry.update', $chronicleEntry->id) : route('chronicle_entry.store') }}">
        @csrf

        <div class="form-row">
            <label class="col-form-label col-md-2">{{ __('Title') }}</label>
            <div class="form-group col-md-10">
                <x-forms.translated-input field="title" :model="$chronicleEntry ?? null"></x-forms.translated-input>
            </div>
        </div>
        <div class="form-row">
            <label class="col-form-label col-md-2">{{ __('Text') }}</label>
            <div class="form-group col-md-10">
                <x-forms.translated-textarea field="text" :model="$chronicleEntry ?? null"></x-forms.translated-textarea>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <x-forms.label field="date">{{ __('Date') }}</x-forms.label>
                <x-forms.input type="date" field="date">{{ $chronicleEntry->date ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-6">
                <x-forms.label field="year">{{ __('Year') }}</x-forms.label>
                <x-forms.input type="number" field="year">{{ $chronicleEntry->year ?? '' }}</x-forms.input>
            </div>
        </div>

        <div class="btn-group" role="group">
            <x-forms.button-save :save="isset($chronicleEntry)"></x-forms.button-save>
            <x-forms.link-cancel>{{ route('chronicle_entry.index') }}</x-forms.link-cancel>
        </div>
    </form>
@endsection
