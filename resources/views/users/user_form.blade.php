@extends('layouts.app')

@section('title')
    @isset($user)
        {{ __('Edit :name', ['name' => $user->full_name]) }}
    @else
        {{ __('Create user') }}
    @endisset
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb href="{{ route('user.index') }}">{{ __('Users') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb />
@endsection

@section('content')
    <x-alerts.success/>

    <form method="POST" action="{{ isset($user) ? route('user.update', $user->id) : route('user.store') }}">
        @csrf

        <div class="form-row">
            <div class="form-group col-md-6">
                <x-forms.label field="first_name">{{ __('First name') }}</x-forms.label>
                <x-forms.input field="first_name" required>{{ $user->first_name ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-6">
                <x-forms.label field="last_name">{{ __('Last name') }}</x-forms.label>
                <x-forms.input field="last_name" required>{{ $user->last_name ?? '' }}</x-forms.input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <x-forms.label field="email">{{ __('E-mail address') }}</x-forms.label>
                <x-forms.input field="email" required>{{ $user->email ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-6">
                <x-forms.label field="role">{{ __('User role') }}</x-forms.label>
                <x-forms.select field="role">
                    @foreach(\App\Options\UserRole::getAll() as $role => $name)
                        <x-forms.select-option field="role" value="{{ $role }}" selected="{{ $user->role ?? '' }}">{{ $name }}</x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <x-forms.label field="password">{{ __('Password') }}</x-forms.label>
                <x-forms.input field="password" type="password" required="{{ !isset($user) }}" clearOld></x-forms.input>
            </div>
            <div class="form-group col-md-6">
                <x-forms.label field="password_confirmation">{{ __('Confirm password') }}</x-forms.label>
                <x-forms.input field="password_confirmation" type="password" required="{{ !isset($user) }}" clearOld></x-forms.input>
            </div>
        </div>

        <div class="btn-group" role="group">
            <x-forms.button-save :save="isset($user)"></x-forms.button-save>
            <x-forms.link-cancel>{{ route('user.index') }}</x-forms.link-cancel>
        </div>
    </form>
@endsection
