@extends('layouts.app')

@php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $users */
    /** @var \App\Models\User $user */
@endphp

@section('title')
    {{ __('Users') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @can('create', \App\Models\User::class)
        <a class="btn btn-primary" href="{{ route('user.create') }}">
            {{ __('Create user') }}
        </a>
    @endcan

    @foreach($users as $user)
        <div class="row my-3">
            <div class="col-12 col-md-4">
                <strong>{{ $user->last_name }}, {{ $user->first_name }}</strong>
            </div>
            <div class="col-12 col-md-4">
                {{ \App\Options\UserRole::getName($user->role) }}
            </div>
            <div class="col-12 col-md-4">
                <div class="btn-group align-content-end">
                    @can('update', $user)
                        <a class="btn btn-primary" href="{{ route('user.edit', $user->id) }}">
                            {{ __('Edit') }}
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    {{ $users->withQueryString()->links() }}
@endsection
