@php
    /** @var App\Models\Designer $designer */
@endphp

@extends('layouts.app')

@section('title')
    {{ __('Coin designer') }} {{ $designer->full_name }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ $designer->full_name }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb href="{{ route('designers') }}">{{ __('Coin designers') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb />
@endsection

@section('edit')
    @can('update', $designer)
        <a class="btn btn-primary" href="{{ route('designer.edit', $designer->id) }}">
            {{ __('Edit') }}
        </a>
    @endcan
@endsection

@section('content')
    <div class="text-center">
        <p class="lead">
            @php
                $title = $designer->sex === 'FEMALE' ? __('Coin designer female'): __('Coin designer');
                if ($designer->birthday) {
                    $birthday = date_format(date_create($designer->birthday), __('date_format'));
                }
                if ($designer->day_of_death) {
                    $dayOfDeath = date_format(date_create($designer->day_of_death), __('date_format'));
                }
            @endphp
            @if($designer->country)
                {{ __(':title from :country', ['title' => $title, 'country' => $designer->country->name]) }}
            @else
                {{ $title }}
            @endif
            @if($designer->birthday)
                @if($designer->day_of_death)
                    ({{ __('born :birthday, died :day_of_death', ['birthday' => $birthday, 'day_of_death' => $dayOfDeath]) }})
                @else()
                    ({{ __('born :birthday', ['birthday' => $birthday]) }})
                @endif
            @endif
        </p>
        @if($designer->website)
            <a href="{{ $designer->website }}" rel="nofollow" target="_blank" class="btn btn-secondary">
                {{ __('More information') }}
            </a>
        @endif
    </div>

    @if(count($designer->designs) > 0)
        <p>{{ __(':name designed the following coins:', ['name' => $designer->full_name]) }}</p>
        <ul>
            @foreach($designer->designs as $design)
                <li>
                    <a href="{{ $design->coin->getURL() }}">{{ $design->coin->name }}</a>

                    @isset($design->mark)
                        @include('common.designer-mark', [
                            'mark' => $design->mark
                        ])
                    @endisset
                </li>
            @endforeach
        </ul>
    @endif
@endsection
