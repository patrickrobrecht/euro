@extends('layouts.app')

@php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $designers */
    /** @var \App\Models\Designer $designer */
@endphp

@section('title')
    {{ __('Coin designers') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @can('create', \App\Models\Designer::class)
        <a class="btn btn-primary" href="{{ route('designer.create') }}">
            {{ __('Create coin designer') }}
        </a>
    @endcan

    @foreach($designers as $designer)
        <div class="row my-3">
            <div class="col-12 col-md-4">
                <strong>{{ $designer->last_name }}, {{ $designer->first_name }}</strong>
            </div>
            <div class="col-12 col-md-4">
                {{ $designer->country->name ?? '' }}
            </div>
            <div class="col-12 col-md-4">
                <div class="btn-group align-content-end">
                    @can('view', $designer)
                        <a class="btn btn-secondary" href="{{ route('designer.show', $designer->slug) }}">
                            {{ __('Show') }}
                        </a>
                    @endcan
                    @can('update', $designer)
                        <a class="btn btn-primary" href="{{ route('designer.edit', $designer->id) }}">
                            {{ __('Edit') }}
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    {{ $designers->withQueryString()->links() }}
@endsection
