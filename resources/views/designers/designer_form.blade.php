@extends('layouts.app')

@php
    /** @var \Illuminate\Database\Eloquent\Collection|\App\Models\Country[] $allCountries */
    /** @var \App\Models\Designer $designer */
@endphp

@section('title')
    @isset($designer)
        {{ __('Edit :name', ['name' => $designer->full_name]) }}
    @else
        {{ __('Create coin designer') }}
    @endisset
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb href="{{ route('designer.index') }}">{{ __('Coin designers') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb />
@endsection

@section('content')
    <x-alerts.success/>

    <form method="POST" action="{{ isset($designer) ? route('designer.update', $designer->id) : route('designer.store') }}">
        @csrf

        <div class="form-row">
            <div class="form-group col-md-6">
                <x-forms.label field="first_name">{{ __('First name') }}</x-forms.label>
                <x-forms.input field="first_name" required>{{ $designer->first_name ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-6">
                <x-forms.label field="last_name">{{ __('Last name') }}</x-forms.label>
                <x-forms.input field="last_name" required>{{ $designer->last_name ?? '' }}</x-forms.input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <x-forms.label field="sex">{{ __('Sex') }}</x-forms.label>
                <x-forms.select field="sex">
                    @foreach(\App\Options\Sex::getAll() as $sex => $name)
                        <x-forms.select-option field="sex" value="{{ $sex }}" selected="{{ $designer->sex ?? '' }}">
                            {{ $name }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
            <div class="form-group col-md-6">
                <x-forms.label field="home_country_id">{{ __('Home country') }}</x-forms.label>
                <x-forms.select field="home_country_id">
                    <x-forms.select-option field="home_country_id" value="" selected="{{ $designer->home_country ?? '' }}">
                        {{ __('unknown') }}
                    </x-forms.select-option>
                    @foreach($allCountries as $country)
                        <x-forms.select-option field="home_country_id" value="{{ $country->id }}" selected="{{ $designer->home_country ?? '' }}">
                            {{ $country->name }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <x-forms.label field="birthday">{{ __('Birthday') }}</x-forms.label>
                <x-forms.input type="date" field="birthday">{{ $designer->birthday ?? '' }}</x-forms.input>
            </div>
            <div class="form-group col-md-6">
                <x-forms.label field="day_of_death">{{ __('Day of death') }}</x-forms.label>
                <x-forms.input type="date" field="day_of_death">{{ $designer->day_of_death ?? '' }}</x-forms.input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12">
                <x-forms.label field="website">{{ __('Website') }}</x-forms.label>
                <x-forms.input field="website">{{ $designer->website ?? '' }}</x-forms.input>
            </div>
        </div>

        <div class="btn-group" role="group">
            <x-forms.button-save :save="isset($designer)"></x-forms.button-save>
            <x-forms.link-cancel>{{ route('designer.index') }}</x-forms.link-cancel>
        </div>
    </form>
@endsection
