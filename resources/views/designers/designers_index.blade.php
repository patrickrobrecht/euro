@extends('layouts.app')

@section('title')
    {{ __('Coin designers') }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('Coin designers') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb />
@endsection

@section('content')
    @php
        /** @var Illuminate\Contracts\Pagination\LengthAwarePaginator $designers */
    @endphp
    @if($designers->count() !== $designers->total())
        <p>{{ __('Showing :count of :total coin designers.', ['count' => $designers->count(), 'total' => $designers->total()]) }}</p>
    @endif

    @php
        // ignore
        $currentFirstLetter = '';
    @endphp
    @foreach($designers as $designer)
        @php
            /** @var App\Models\Designer $designer */
            $firstLetter = substr($designer->last_name, 0, 1);
        @endphp
        @if($firstLetter !== $currentFirstLetter && $firstLetter === \Illuminate\Support\Str::ascii($firstLetter))
            @if($currentFirstLetter !== '')
                {!! '</ul>' !!}
            @endif
            @php
                $currentFirstLetter = $firstLetter;
            @endphp

            <h2>{{ $currentFirstLetter }}</h2>
            <ul class="row">
        @endif
        <li class="col-12 col-sm-6 col-md-4 col-xl-3">
            <a href="{{ route('designer.show', $designer->slug) }}">{{ $designer->name_with_comma }}</a>
        </li>
    @endforeach
    </ul>

    <nav aria-label="{{ __('Pagination') }}">
        {{ $designers->onEachSide(5)->links() }}
    </nav>
@endsection
