@if(count($coins) === count($allCoins))
    @isset($allText)
        {{ $allText }}
    @endisset
@else
    @foreach($coins as $coin)
        @php
            /** @var App\Models\Coin $coin */
        @endphp
        @if($loop->first){{ $before }}@endif{{ $coin->formatted_denomination }}@if($loop->last){{ $after }}@else,@endif
    @endforeach
@endif
