{{-- Mark with optional name --}}
@php
    /** @var App\Models\Mark $mark */
@endphp
@if($mark->type === 'IMAGE')
    ({{ __('Mark') }}: <img src="{{ asset($mark->text) }}" alt="{{ $mark->description }}" loading="lazy">)@isset($afterMark){{ $afterMark }}@endisset
@else
    ({{ __('Mark') }}: {{ $mark->text }})@isset($afterMark){{ $afterMark }}@endisset
@endif
