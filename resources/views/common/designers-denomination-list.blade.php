@if(count($designersList) > 0)
    <br>{{ $role }}:
    @foreach($designersList as $designersListItem)
        @if($loop->last && count($designersList) > 1)
            {{ __('and') }}
        @endif
        <a href="{{ route('designer.show', $designersListItem['designer']->slug) }}">{{ $designersListItem['designer']->first_name }}
            {{ $designersListItem['designer']->last_name }}</a>
        @isset($designersListItem['mark'])
            @include('common.designer-mark', [
                'mark' => $designersListItem['mark']
            ])
        @endisset
        @php
            $before = '(';
            $after = ($loop->iteration >= $loop->count - 1) ? ')' : '),';
        @endphp
        @include('common.denominations-list', [
            'coins' => $designersListItem['coins'],
            'allCoins' => $coinsWithIdenticalDescription,
            'before' => $before,
            'after' => $after
        ])
    @endforeach
@endif
