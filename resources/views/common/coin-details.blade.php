{{-- Coin Details table --}}
@php
    /** @var App\Models\Coin $coin */
@endphp
@if($coin->details->count() > 0)
    <div class="table-responsive">
        <table class="table table-striped coin-details-table">
            <thead>
                <tr>
                    <th scope="col">{{ __('Year') }}</th>
                    <th scope="col">{{ __('Mint') }}</th>
                    <th scope="col">{{ __('Mint mark') }}</th>
                    <th scope="col">{{ __('Mint master mark') }}</th>
                    <th scope="col">{{ __('Mintage') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($coin->details as $detail)
                    @php
                        /** @var App\Models\CoinDetails $detail */
                        $mint = $detail->mintDetails->mint;
                        $mintMark = $detail->mintDetails->mintMark;
                        $mintMasterMark = $detail->mintDetails->mintMasterMark;
                    @endphp
                    <tr>
                        <td>{{ $detail->year }}</td>
                        <td>
                            @if($mint)
                                <a href="{{ route('mint', $mint->slug) }}">
                                    {{ $mint->name }}
                                </a>
                            @else
                                {{ __('unknown') }}
                            @endif
                        </td>
                        <td>
                            @if($mintMark)
                                @include('common.mark', ['mark' => $mintMark])
                            @else
                                <span class="d-lg-none">{{ __('none') }}</span>
                                <span class="d-none d-lg-inline">{{ __('no mint mark') }}</span>
                            @endif
                        </td>
                        <td>
                            @if($mintMasterMark)
                                @include('common.mark', ['mark' => $mintMasterMark])
                                @if($detail->mintDetails->mintMaster)
                                    {{ __('for') }} {{ $detail->mintDetails->mintMaster->first_name }} {{ $detail->mintDetails->mintMaster->last_name }}
                                @endif
                            @else
                                <span class="d-lg-none">{{ __('none') }}</span>
                                <span class="d-none d-lg-inline">{{ __('no mint master mark') }}</span>
                            @endif
                        </td>
                        <td class="text-right">
                            @if($detail->set_only)
                                <span class="text-muted small" title="{{ __('set only') }}">{{ number_format($detail->mintage, 0, '.', __('thousand_separator')) }}</span>
                            @else
                                {{ $detail->formatted_mintage }}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <p>{{ __('No details available.') }}</p>
@endif
