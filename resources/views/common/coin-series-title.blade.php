{{-- Coin series title --}}
@php
    /** @var App\Models\CoinSeries $series */
@endphp

{{ $series->name }} <small>({{ $series->period }})</small>
