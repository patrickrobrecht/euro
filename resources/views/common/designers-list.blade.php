@if(count($designersList) > 0)
    <p>
        {{ $role }}:
        @foreach($designersList as $designersListItem)
            @if($loop->last && count($designersList) > 1)
                {{ __('and') }}
            @endif
            <a href="{{ route('designer.show', $designersListItem->designer->slug) }}">{{ $designersListItem->designer->first_name }}
                {{ $designersListItem->designer->last_name }}</a>@if(!isset($designersListItem->mark) && $loop->iteration < $loop->count - 1),@endif
            @php
                $afterMark = ($loop->iteration >= $loop->count - 1) ? '' : ',';
            @endphp
            @isset($designersListItem['mark'])
                @include('common.designer-mark', [
                    'mark' => $designersListItem['mark'],
                    'after' => $afterMark
                ])
            @endisset
        @endforeach
    </p>
@endif
