{{-- Mark with description --}}
@php
    /** @var App\Models\Mark $mark */
@endphp
<div class="row">
    <div class="col-2 col-lg-3 my-2">
        @if($mark->type === 'IMAGE')
            <img style="max-height: 1cm" src="{{ asset($mark->text) }}" alt="{{ $mark->description }}" loading="lazy">
        @else
            <strong>{{ $mark->text }}</strong>
        @endif
    </div>
    <div class="col-9">
        @php
            $desc = $mark->description ? $mark->description : $mark->name;
        @endphp
        @isset($for)
            {{ __(':mark for :for', ['mark' => $desc, 'for' => $for]) }}
        @else
            {{ $desc }}
        @endisset
    </div>
</div>
