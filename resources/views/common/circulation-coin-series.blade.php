@php
    /** @var App\Models\CoinSeries $series */
@endphp

<div class="circulation-coins-images">
    @foreach($series->coins as $coin)
        <img src="{{ asset($coin->image) }}" class="coin-{{ ($coin->denomination_cents) }}"
             alt="{{ $coin->name }}" loading="lazy">
    @endforeach
</div>

<div class="circulation-coins-descriptions">
    @php
        // Combine identical descriptions.
        $descriptions = [];
        foreach ($series->coins as $coin) {
            if (array_key_exists($coin->description, $descriptions)) {
                $descriptions[$coin->description][] = $coin;
            } else {
                $descriptions[$coin->description] = [ $coin ];
            }
        }
    @endphp
    <dl>
        @foreach($descriptions as $description => $coinsWithIdenticalDescription)
            <dt>
                @include('common.denominations-list', [
                    'coins' => $coinsWithIdenticalDescription,
                    'allCoins' => $series->coins,
                    'allText' => __('all'),
                    'before' => '',
                    'after' => ''
                ])
            </dt>
            <dd>
                {{ $description }}

                @php
                    // Collect all designers.
                    $designersList = [];
                    $engraversList = [];
                    foreach ($coinsWithIdenticalDescription as $c) {
                        foreach ($c->designerDetails as $designer) {
                            if ($designer->role === 'ENGRAVING') {
                                if (array_key_exists($designer->designer->id, $engraversList)) {
                                    $engraversList[$designer->designer->id]['coins'][] = $c;
                                } else {
                                    $engraversList[$designer->designer->id] = [
                                        'coins' => [ $c ],
                                        'designer' => $designer->designer,
                                        'mark' => $designer->mark
                                    ];
                                }
                            } else {
                                if (array_key_exists($designer->designer->id, $designersList)) {
                                    $designersList[$designer->designer->id]['coins'][] = $c;
                                } else {
                                    $designersList[$designer->designer->id] = [
                                        'coins' => [ $c ],
                                        'designer' => $designer->designer,
                                        'mark' => $designer->mark
                                    ];
                                }
                            }
                        }
                    }
                @endphp
                @include('common.designers-denomination-list', [
                    'designersList' => $designersList,
                    'role' => __('Design')
                ])
                @include('common.designers-denomination-list', [
                    'designersList' => $engraversList,
                    'role' => __('Engraving')
                ])
            </dd>
        @endforeach
    </dl>
</div>
