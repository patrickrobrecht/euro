{{-- Mark with optional name --}}
@php
    /** @var App\Models\Mark $mark */
@endphp
@if($mark->type === 'IMAGE')
    <img src="{{ asset($mark->text) }}" alt="{{ $mark->name }}" title="{{ $mark->description }}" loading="lazy">
    @if($mark->name)
        ({{ $mark->name }})
    @endif
@else
    <strong>{{ $mark->text }}</strong>
@endif
