@extends('layouts.app')

@php
    /** @var \App\Models\Mint $mint */
@endphp

@section('title')
    @isset($mint)
        {{ __('Edit :name', ['name' => $mint->name]) }}
    @else
        {{ __('Create mint') }}
    @endisset
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb href="{{ route('mint.index') }}">{{ __('Mints') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb />
@endsection

@section('content')
    <x-alerts.success/>

    <form method="POST" action="{{ isset($mint) ? route('mint.update', $mint->id) : route('mint.store') }}">
        @csrf

        <div class="form-row">
            <label class="col-form-label col-md-2">{{ __('Name') }}</label>
            <div class="form-group col-md-10">
                <x-forms.translated-input field="name" :model="$mint ?? null" required></x-forms.translated-input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12">
                <x-forms.label field="local_name">{{ __('Local name') }}</x-forms.label>
                <x-forms.input field="local_name" required>{{ $mint->local_name ?? '' }}</x-forms.input>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <x-forms.label field="country_id">{{ __('Country') }}</x-forms.label>
                <x-forms.select field="country_id">
                    @foreach($allCountries as $country)
                        <x-forms.select-option field="country_id" value="{{ $country->id }}" selected="{{ $mint->country->id ?? '' }}">
                            {{ $country->name }}
                        </x-forms.select-option>
                    @endforeach
                </x-forms.select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-12">
                <x-forms.label field="website">{{ __('Website') }}</x-forms.label>
                <x-forms.input field="website" required>{{ $mint->website ?? '' }}</x-forms.input>
            </div>
        </div>

        <div class="btn-group" role="group">
            <x-forms.button-save :save="isset($mint)"></x-forms.button-save>
            <x-forms.link-cancel>{{ route('mint.index') }}</x-forms.link-cancel>
        </div>
    </form>
@endsection
