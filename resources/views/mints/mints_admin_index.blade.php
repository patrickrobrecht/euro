@extends('layouts.app')

@php
    /** @var \Illuminate\Pagination\LengthAwarePaginator $mints */
    /** @var \App\Models\Mint $mint */
@endphp

@section('title')
    {{ __('Mints') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb>{{ __('Administration') }}</x-nav.breadcrumb>
@endsection

@section('content')
    @can('create', \App\Models\Mint::class)
        <a class="btn btn-primary" href="{{ route('mint.create') }}">
            {{ __('Create mint') }}
        </a>
    @endcan

    @foreach($mints as $mint)
        <div class="row my-3">
            <div class="col-12 col-md-8">
                <div><strong>{{ $mint->name }}</strong>, {{ $mint->country->name }}</div>
                <div>{{ $mint->local_name }}</div>
            </div>
            <div class="col-12 col-md-4">
                <div class="btn-group align-content-end">
                    @can('view', $mint)
                        <a class="btn btn-secondary" href="{{ route('mint', $mint->slug) }}">
                            {{ __('Show') }}
                        </a>
                    @endcan
                    @can('update', $mint)
                        <a class="btn btn-primary" href="{{ route('mint.edit', $mint->id) }}">
                            {{ __('Edit') }}
                        </a>
                    @endcan
                </div>
            </div>
        </div>
    @endforeach

    {{ $mints->withQueryString()->links() }}
@endsection
