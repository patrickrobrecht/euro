@php
    /** @var App\Models\Mint $mint */
@endphp

@extends('layouts.app')

@section('title')
    {{ __('Mint') }} {{ $mint->name }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ $mint->local_name }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb href="{{ route('mints') }}">{{ __('Mints') }}</x-nav.breadcrumb>
    <x-nav.breadcrumb>{{ $mint->name }}</x-nav.breadcrumb>
@endsection

@section('edit')
    @can('update', $mint)
        <a class="btn btn-primary" href="{{ route('mint.edit', $mint->id) }}">
            {{ __('Edit') }}
        </a>
    @endcan
@endsection

@section('content')
    <div class="text-center">
        <p class="lead">
            {{ __('Mint in :city, :country', ['city' => $mint->name, 'country' => $mint->country->name]) }}
        </p>
        @if($mint->website)
            <a href="{{ $mint->website }}" rel="nofollow" target="_blank" class="btn btn-secondary">
                {{ __('Website') }}
            </a>
        @endif
        <p class="mb-3">
            @if($mint->description)
                {{ $mint->description }}
            @endif
        </p>
    </div>

    @if($mint->mintMarks->count() === 0 && $mint->mintMasters->count() === 0)
        <p>{{ __('Coins minted in :city have neither a mint mark nor a mint master mark.', ['city' => $mint->name]) }}</p>
    @else
        <div class="row">
            <div class="col-12 col-md-6">
                <h2>{{ __('Mint marks') }}</h2>
                @if($mint->mintMarks->count() === 0)
                    <p>{{ __('Coins minted in :city do not have a mint mark.', ['city' => $mint->name]) }}</p>
                @else
                    @each('common.mark-list-item', $mint->mintMarks, 'mark')
                @endif
            </div>
            <div class="col-12 col-md-6">
                <h2>{{ __('Mint master marks') }}</h2>
                @if($mint->mintMasters->count() === 0)
                    <p>{{ __('Coins minted in :city do not have a mint master mark.', ['city' => $mint->name]) }}</p>
                @else
                    @foreach($mint->mintMasters as $mintMaster)
                        @foreach ($mintMaster->marks as $mark)
                            @include('common.mark-list-item', [
                                'mark' => $mark,
                                'for' => sprintf('%s %s, %s', $mintMaster->first_name, $mintMaster->last_name, $mintMaster->title)
                            ])
                        @endforeach
                    @endforeach
                @endif
            </div>
        </div>
    @endif
@endsection
