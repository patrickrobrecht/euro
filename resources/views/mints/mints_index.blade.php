@extends('layouts.app')

@section('title')
    {{ __('Mints') }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('Mints') }}
@endsection

@section('breadcrumbs')
    <x-nav.breadcrumb />
@endsection

@section('content')
    <div class="row">
        @foreach($mints as $mint)
            @php
                /** @var App\Models\Mint $mint */
            @endphp
            <div class="col-12 col-sm-6 col-lg-4">
                <div class="card my-3">
                    <div class="card-header">
                        <h5 class="card-title">
                            <a href="{{ route('mint', $mint->slug) }}">
                                {{ $mint->name }}
                            </a>
                        </h5>
                        <p class="card-subtitle text-muted">
                            {{ $mint->country->name }}
                        </p>
                    </div>
                    <div class="card-body">
                        @if($mint->mintMarks->count() > 0)
                            <p>
                                {{ __('Mint mark') }}:
                                @foreach($mint->mintMarks as $mark)
                                    @if($mark->type === 'IMAGE')
                                        <img src="{{ asset($mark->text) }}" alt="{{ $mark->name }}" title="{{ $mark->description }}" loading="lazy">
                                    @else
                                        <strong>{{ $mark->text }}</strong>@if(!$loop->last),@endif
                                    @endif
                                @endforeach
                            </p>
                        @endif
                        <p class="card-text">
                            {{ $mint->local_name }}
                        </p>
                        @if($mint->website)
                            <a href="{{ $mint->website }}" rel="nofollow" target="_blank" class="btn btn-secondary">
                                {{ __('Website') }}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
    @endforeach
    </div>
@endsection
