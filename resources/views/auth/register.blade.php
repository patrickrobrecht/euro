@extends('layouts.app')

@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-4">
        <form method="post" action="{{ route('register') }}">
            @csrf
            <div class="text-center">
                <h1 class="my-3">{{ __('Register') }}</h1>
            </div>
            <div class="form-group">
                <label for="first_name">{{ __('First name') }}</label>
                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror"
                       name="first_name" value="{{ old('first_name') }}" required autocomplete autofocus>
                @error('first_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="last_name">{{ __('Last name') }}</label>
                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror"
                       name="last_name" value="{{ old('last_name') }}" required autocomplete autofocus>
                @error('last_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">{{ __('E-mail address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                       name="email" value="{{ old('email') }}" required autocomplete="email">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                       name="password" required autocomplete="new-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password-confirm">{{ __('Confirm password') }}</label>
                <input id="password-confirm" type="password" class="form-control"
                       name="password_confirmation" required autocomplete="new-password">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Register now') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
