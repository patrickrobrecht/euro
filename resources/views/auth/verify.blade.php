@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <form method="post" action="{{ route('verification.resend') }}">
                @csrf
                <div class="text-center">
                    <h1 class="my-3">{{ __('Verify your e-mail address') }}</h1>
                </div>
                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('A fresh verification link has been sent to your e-mail address.') }}
                    </div>
                @endif
                <p>
                    {{ __('Before proceeding, please check your e-mail for a verification link.') }}
                </p>
                <button type="submit" class="btn btn-secondary">
                    {{ __('Send verification link') }}
                </button>
            </form>
        </div>
    </div>
@endsection
