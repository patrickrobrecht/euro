@extends('layouts.app')

@section('title')
    {{ __('Login') }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-4">
        <form method="post" action="{{ route('login') }}">
            @csrf
            <div class="text-center">
                <h1 class="my-3">{{ __('Login') }}</h1>
            </div>
            <div class="my-3 alert alert-info" role="alert">
                {{ __('Not registered yet?') }}
                <a href="{{ route('register') }}" class="alert-link">
                    {{ __('Register now') }}
                </a>
            </div>
            <div class="form-group">
                <label for="email">{{ __('E-mail address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                       name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="password">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                       name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Login') }}
                </button>
            </div>
        </form>
        <div class="text-center">
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot your password?') }}
            </a>
        </div>
    </div>
</div>
@endsection
