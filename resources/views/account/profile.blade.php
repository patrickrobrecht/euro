@extends('layouts.app')

@section('title')
    {{ __('Profile') }} | {{ __('Euro Coin Catalog') }}
@endsection

@section('headline')
    {{ __('Profile') }}
@endsection

@section('content')
    @php
        /** @var $user App\Models\User */
        $user = Auth::getUser();
    @endphp
    @auth
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h3 class="card-title">{{ Auth::getUser()->first_name }} {{ Auth::getUser()->last_name }}</h3>
                <p class="card-subtitle text-muted">{{ __('Joined :date', ['date' => Auth::getUser()->formatted_created_at]) }}</p>
                <p class="card-text">{{ Auth::getUser()->email }}</p>
                @if(!Auth::getUser()->email_verified_at)
                    <a href="{{ route('verification.notice') }}" class="card-link">
                        {{ __('Verify your e-mail address') }}
                    </a>
                @endif
            </div>
        </div>
    @endauth
@endsection
