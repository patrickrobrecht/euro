@extends('layouts.full-width')

@section('title')
    {{ __('Page Not Found') }}
@endsection

@section('headline')
    {{ __('Page Not Found') }}
@endsection

@section('content')
    <a class="btn btn-primary" href="{{ route('home') }}">
        {{ __('Home') }}
    </a>
@endsection
