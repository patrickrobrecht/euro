@php
    echo '<?xml version="1.0" encoding="UTF-8"?>';
    $routes = [
        'home',
        'page-circulation-coins',
        'page-2-euro-commemorative-coins',
        'designers',
        'mints'
    ];
@endphp
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach(config('app.locales') as $locale => $localeName)
        @foreach($routes as $route)
            <url>
                <loc>{{ route($locale . '.' . $route) }}</loc>
            </url>
        @endforeach

        @foreach($countries as $country)
            @php
                /** @var App\Models\Country $country */
            @endphp
            @foreach(['country.show', 'circulation-coins', '2-euro-commemorative-coins'] as $route)
                <url>
                    <loc>{{ route($locale . '.' . $route, $country->translate($locale)->slug) }}</loc>
                </url>
                @foreach($country->circulationCoinSeries as $series)
                    <url>
                        <loc>{{ route($locale . '.circulation-coin-series', [$country->translate($locale)->slug, $series->translate($locale)->slug]) }}</loc>
                    </url>
                @endforeach
            @endforeach
        @endforeach

        @foreach($mints as $mint)
            @php
                /** @var App\Models\Mint $mint */
            @endphp
            <url>
                <loc>{{ route($locale . '.mint', $mint->translate($locale)->slug) }}</loc>
            </url>
        @endforeach

        @foreach($designers as $designer)
            @php
                /** @var App\Models\Designer $designer */
            @endphp
            <url>
                <loc>{{ route($locale . '.designer.show', $designer->slug) }}</loc>
            </url>
        @endforeach
    @endforeach
</urlset>
