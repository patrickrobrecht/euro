<?php

namespace Database\Seeders;

use App\Imports\ChronicleImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class ChronicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Excel::import(new ChronicleImport(), 'chronicle.ods', 'data');
    }
}
