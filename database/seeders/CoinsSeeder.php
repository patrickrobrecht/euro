<?php

namespace Database\Seeders;

use App\Imports\DataImport;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;

class CoinsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Excel::import(new DataImport(), 'data.ods', 'data');
    }
}
