<?php

namespace Database\Seeders;

use App\Models\Block;
use Illuminate\Database\Seeder;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Spatie\YamlFrontMatter\YamlFrontMatter;

class BlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $disk = Storage::disk('data');
        foreach ($disk->files('blocks') as $path) {
            [$slug, $language] = explode('.', File::name($path));

            $block = Block::findByCode($slug);
            if (!$block) {
                $block = new Block(['code' => $slug]);
            }

            try {
                $file = YamlFrontMatter::parse($disk->get($path));

                $translation = $block->getNewTranslation($language);
                $translation->title = $file->title;
                $translation->contents = $file->body();
                $block->save();
            } catch (FileNotFoundException $e) {
            }
        }
    }
}
