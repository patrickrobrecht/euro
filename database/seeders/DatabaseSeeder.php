<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(UsersTableSeeder::class);

        // Disable query cache during imports.
        config(['cachebuilder.enable' => false]);

        $this->call([
            BlockSeeder::class,
            CoinsSeeder::class,
            ChronicleSeeder::class
        ]);

        // Re-enable caching.
        config(['cachebuilder.enable' => true]);
    }
}
