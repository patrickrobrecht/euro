<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('coins', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id')->references('id')->on('countries');
            $table->foreignId('coin_series_id')->nullable()->references('id')->on('coin_series');
            $table->enum('type', ['CIRCULATION', 'COMMEMORATIVE']);
            $table->decimal('denomination');
            $table->date('issue_date')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
        Schema::create('coin_translations', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('coin_id')->references('id')->on('coins');
            $table->string('locale')->index();
            $table->string('name');
            $table->text('description')->nullable();
            $table->unique(['coin_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('coins');
    }
}
