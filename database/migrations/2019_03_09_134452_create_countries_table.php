<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('countries', static function (Blueprint $table) {
            $table->id();
            $table->char('code', 2)->unique();
            $table->unsignedInteger('eu_member_since')->nullable();
            $table->unsignedInteger('euro_since')->nullable();
            $table->string('image');
            $table->timestamps();
        });
        Schema::create('country_translations', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id')->references('id')->on('countries');
            $table->string('locale')->index();
            $table->string('slug')->index();
            $table->string('name');
            $table->string('full_name');
            $table->unique(['country_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('countries_translations');
    }
}
