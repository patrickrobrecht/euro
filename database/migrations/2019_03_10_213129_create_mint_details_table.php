<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMintDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('mint_details', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('mint_id')->nullable()->references('id')->on('mints');
            $table->foreignId('mint_mark_id')->nullable()->references('id')->on('marks');
            $table->foreignId('mint_master_id')->nullable()->references('id')->on('mint_masters');
            $table->foreignId('mint_master_mark_id')->nullable()->references('id')->on('marks');
            $table->unique(['mint_id', 'mint_mark_id', 'mint_master_id', 'mint_master_mark_id'], 'combination');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('mint_details');
    }
}
