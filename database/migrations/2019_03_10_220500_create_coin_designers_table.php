<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinDesignersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('coin_designers', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('coin_id')->references('id')->on('coins');
            $table->foreignId('designer_id')->references('id')->on('designers');
            $table->foreignId('designer_mark_id')->nullable()->references('id')->on('marks');
            $table->enum('role', ['DESIGNING', 'ENGRAVING']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('coin_designers');
    }
}
