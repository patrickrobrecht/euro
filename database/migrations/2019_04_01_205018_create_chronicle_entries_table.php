<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChronicleEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('chronicle_entries', static function (Blueprint $table) {
            $table->id();
            $table->date('date')->nullable();
            $table->year('year');
            $table->timestamps();
        });
        Schema::create('chronicle_entry_translations', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('chronicle_entry_id')->references('id')->on('chronicle_entries');
            $table->string('locale')->index();
            $table->string('title')->nullable();
            $table->text('text');
            $table->unique(['chronicle_entry_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('chronicle_entries');
        Schema::dropIfExists('chronicle_entries_translations');
    }
}
