<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('coin_series', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id')->references('id')->on('countries');
            $table->enum('type', ['CIRCULATION', 'COMMEMORATIVE']);
            $table->year('first_year')->nullable();
            $table->year('last_year')->nullable();
            $table->string('code')->unique();
            $table->timestamps();
        });
        Schema::create('coin_series_translations', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('coin_series_id')->references('id')->on('coin_series');
            $table->string('locale')->index();
            $table->string('name');
            $table->string('slug')->index();
            $table->unique(['coin_series_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('coin_series');
    }
}
