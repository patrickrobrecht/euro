<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('blocks', static function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->timestamps();
        });
        Schema::create('block_translations', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('block_id')->references('id')->on('blocks');
            $table->string('locale')->index();
            $table->string('title');
            $table->text('contents');
            $table->timestamps();
            $table->unique(['block_id', 'locale']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('blocks');
        Schema::dropIfExists('blocks_translations');
    }
}
