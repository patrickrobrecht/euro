<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('mints', static function (Blueprint $table) {
            $table->id();
            $table->string('local_name');
            $table->foreignId('country_id')->references('id')->on('countries');
            $table->string('website')->nullable();
            $table->timestamps();
        });
        Schema::create('mint_translations', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('mint_id')->references('id')->on('mints');
            $table->string('locale')->index();
            $table->string('slug')->index();
            $table->string('name');
            $table->unique(['mint_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('mints');
        Schema::dropIfExists('mint_translations');
    }
}
