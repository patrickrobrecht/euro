<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('marks', static function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique();
            $table->enum('type', ['TEXT', 'IMAGE']);
            $table->string('text');
            $table->timestamps();
        });
        Schema::create('mark_translations', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('mark_id')->references('id')->on('marks');
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->unique(['mark_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('marks');
    }
}
