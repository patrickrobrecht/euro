<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('coin_details', static function (Blueprint $table) {
            $table->id();
            $table->foreignId('coin_id')->references('id')->on('coins');
            $table->year('year');
            $table->foreignId('mint_details_id')->nullable()->references('id')->on('mint_details');
            $table->integer('mintage')->nullable();
            $table->boolean('set_only')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('coin_details');
    }
}
