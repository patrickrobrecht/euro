const mix = require('laravel-mix');
const convertToFileHash = require('laravel-mix-make-file-hash');

mix.sourceMaps(true, 'source-map');

mix.sass('resources/sass/app.scss', 'public/css');

const javaScriptLibraries = [
    'node_modules/bootstrap/dist/js/bootstrap.min.js',
    'node_modules/jquery/dist/jquery.min.js'
];
javaScriptLibraries.forEach(
    file => mix.copy(file, 'public/lib')
);

if (mix.inProduction()) {
    mix.disableNotifications();

    // Setup hash-based file names for the production environment.
    mix.version();
    mix.then(() => {
        convertToFileHash({
            publicPath: 'public',
            manifestFilePath: 'public/mix-manifest.json'
        });
    });
}
