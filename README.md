# Euro Catalog

## Used Technologies

The web frontend and the backend are developed with:

- [PHP](https://secure.php.net/) and the following PHP libraries
    - [Laravel](https://laravel.com/docs/),
        web application framework
    - [Laravel Excel](https://laravel-excel.com/)
        for imports from `.ods` files
    - [laravel Localized Routes](https://github.com/codezero-be/laravel-localized-routes)
        for translations of the routes
    - [Laravel Translatable](https://github.com/Astrotomic/laravel-translatable)
        for translations of the models
    - [Model Caching for Laravel](https://github.com/GeneaLabs/laravel-model-caching)
        to cache models
    - [Eloquent-Sluggable](https://github.com/cviebrock/eloquent-sluggable)
        to generate unique slugs for models
    - [PHPSpreadsheet](https://github.com/PHPOffice/PhpSpreadsheet)
        for exports into Excel files (`.xlsx`)
- [Laravel Mix](https://laravel.com/docs/7.x/mix) and [Laravel Mix make file hash](https://github.com/ben-rogerson/laravel-mix-make-file-hash)
    for build and hashed-based file names for assets
- [Bootstrap 4](https://getbootstrap.com/docs/)

At runtime, a [MySQL](https://dev.mysql.com/downloads/) database
    and an [Apache](https://httpd.apache.org/) webserver are required.


## Data
The separate repository `euro-data` has to be mapped to `storage/data`
    (cp. disk `data` in `config/filesystems.php`).

The command `composer db` recreates the whole database,
    importing the data `.ods` or `.md` files located on the `data` disk.

### Add another locale
To add another locale with language key LANG
- add locale in the configuration files
    - `config/app.php` (in `'locale'`),
    - `config/localized-routes.php` (in `'supported-locales''`),
    - and `config/translatable.php` (in `'locales'`);
- add translations to the data
    - additional columns for the translated data in `*.ods` (suffix for the language),
    - `latex/eurozone2.LANG.tex`,
    - and `latex/intro.LANG.tex`;
- and adjust scripts in `composer.json`:
    - `latex-compile`,
    - `latex-copy`,
    - and `translate`.


## Development

### How to develop
Required: [Composer](https://getcomposer.org/)
    and [npm](https://www.npmjs.com/)

- Run `composer dev` to install dependencies,
    clear the cache and compile the CSS.
- Run `composer dev:watch` start the file watcher,
    which compiles `.scss` to `.css` upon every change.
- Create a database and define the credentials in `.env`
    (have a look at `.env.example` for the format).
- Copy (or symlink) the `euro-data` repository to `storage/data`.
- Run `composer db` to create the database tables
    and import the content from the Excel files. 

### Code style

- CSS code follows the recommended [stylelint](https://stylelint.io/) configuration.
- PHP code follows the [PSR-12 code style](https://www.php-fig.org/psr/psr-12/) (see [phpcs.xml](./phpcs.xml)).

Use `composer cs` (`composer fix`) to check for violations (fix errors) in CSS and PHP code.

### Localization

Translations can be found in `resources/lang`:
- `{locale}.json` contains the translations for the strings in the views.
    `composer translate` will update the JSON files
    using [translatable string exporter](https://github.com/kkomelin/laravel-translatable-string-exporter)
    (remove unused translations, add new translations).
- `{locale}/routes.php` handles the translations for segments of the URL.

The translations of the model are handled in the database (tables ending with `_translations`).

### Testing

`composer test` runs all the tests.


## Deployment

### Installation
- Create a `.env` for the public environment.
    - `APP_ENV=production`
    - `APP_DEBUG=false`
    - `APP_URL` must be set to the root URL of the application
    - `DB_CONNECTION`, `DB_HOST`, `DB_PORT`, `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD` 
        must be set to valid credentials for the database (MySQL/MariaDB recommended)
    - `DB_CACHE=true`
- Do a update as described below.

### Updates
- Run `./generate.sh` which 
    - runs `composer generate` to
        - generate the LaTeX files for the catalog (in the `storage/temp`),
        - compile the LaTeX files to a PDF (requires xelatex)
            and copies the PDF to `app/public/downloads/{locale}`,
        - and generate the Excel sheets for the issue overview.
    - and creates the issue overview PDFs from Excel
        (requires soffice - LibreOffice CLI).
- Run `composer production` to remove development dependencies
    and generate assets for production.
- Update the production environment
    - Adjust `.env` on the production server
    - Upload files to the server.
        - directories `app`, `bootstrap`, `config`,  `public`, `resources`, `routes`, `storage`, `vendor`
        - `artisan`, `composer.json`
    - Run `php artisan config:cache` to cache the configuration.
    - Run `php artisan route:cache` to cache the routes.
    - Run `php artisan view:cache` to cache the  views.
    - Update the database with `composer db` (run the migrations, and the data import, clear cache).
